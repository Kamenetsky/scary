using UnityEngine;
using System.Collections;

namespace Tiinoo.DeviceConsole
{
	public class DeviceConsoleLoader : MonoBehaviour
	{
		void Start()
		{
			#if FINAL_BUILD
			// disable device console
			#else
			Load();
			#endif
			
			Destroy(gameObject);
		}
		
		public static void Load()
		{
			string pluginName = DCConst.PLUGIN_NAME;
			string prefabPath = DCConst.DC_PREFAB_PATH_IN_RESOURCES;
			
			GameObject prefab = Resources.Load<GameObject>(prefabPath);
			
			if (prefab == null)
			{
				Debug.LogError(string.Format("[{0}] error: {1} doesn't exist!", pluginName, prefabPath));
				return;
			}
			
			GameObject go = Object.Instantiate(prefab) as GameObject;
			go.name = pluginName;
		}
	}
}

