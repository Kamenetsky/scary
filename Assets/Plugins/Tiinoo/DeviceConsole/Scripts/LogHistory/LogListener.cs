using UnityEngine;
using System.Collections;

namespace Tiinoo.DeviceConsole
{
	public class LogListener : MonoBehaviour
	{
		void Start()
		{
			RegisterLogCallback();
		}
		
		void OnDestroy()
		{
			UnregisterLogCallback();
		}
		
		private void RegisterLogCallback()
		{
			#if UNITY_5
			Application.logMessageReceived += LogHandler.LogCallback;
			#else
			Application.RegisterLogCallback(LogHandler.LogCallback);
			#endif
		}
		
		private void UnregisterLogCallback()
		{
			#if UNITY_5
			Application.logMessageReceived -= LogHandler.LogCallback;
			#else
			Application.RegisterLogCallback(null);
			#endif
		}
	}
}
