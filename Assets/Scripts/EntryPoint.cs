﻿using UnityEngine;
using System.Collections;

using Scary.GUI;

namespace Scary
{
    public class EntryPoint : MonoBehaviour
    {
        void Start()
        {
            //TODO remove later
            Scary.Models.Defs.GenerateGameDefs();
            
            WindowManager.Instance.Show<GUICharacterSelectWindow>();
        }
    }
}
