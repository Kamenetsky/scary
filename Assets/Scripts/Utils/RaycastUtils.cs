﻿using UnityEngine;
using System.Collections;

namespace Scary.Utils
{
    public class RaycastUtils
    {
        public static T FindCollidedControl<T>(Touch touch) where T : Component
        {
            Ray ray = Camera.main.ScreenPointToRay(touch.position);
            RaycastHit focusHit;
            return FindCollidedControl<T>(Physics.RaycastAll(ray, Mathf.Infinity), touch, out focusHit);
        }

        public static T FindCollidedControl<T>(RaycastHit[] hits, Touch touch, out RaycastHit focusHit) where T : Component
        {
            focusHit = new RaycastHit();
            T focus = null;
            float dst = Mathf.Infinity;
            for (int rh = 0; rh != hits.Length; ++rh)
            {
                RaycastHit hit = hits[rh];
                T control = hit.collider.transform.GetComponent<T>();
                if (control != null)
                {
                    if (dst > hit.distance)
                    {
                        focus = control;
                        dst = hit.distance;
                        focusHit = hit;
                    }
                }
            }
            return focus;
        }
    }

}
