﻿using System.Collections.Generic;
using System;
using System.Linq;

using UnityEngine;

namespace Scary.Utils
{
    public class DAS
    {

        public DekstraPoint[] DekstraPoints { get; private set; }
        public DekstraEdge[] Edges { get; private set; }
        public DekstraPoint BeginDekstraPoint { get; private set; }

        public DAS(Dictionary<int, List<int>> neightborsById)
        {
            List<DekstraPoint> points = new List<DekstraPoint>();
            List<DekstraEdge> erges = new List<DekstraEdge>();
            int index = 0;
            foreach (var kvp in neightborsById)
            {
                points.Add(new DekstraPoint(Int32.MaxValue, false, kvp.Key));
                ++index;
            }

            foreach (var kvp in neightborsById)
            {
                points.Add(new DekstraPoint(index == 0 ? 0 : Int32.MaxValue, false, kvp.Key));
                foreach (var it in kvp.Value)
                {
                    if (!erges.Exists(x => (x.FirstDekstraPoint.Id == kvp.Key && x.SecondDekstraPoint.Id == it) ||
                                           (x.FirstDekstraPoint.Id == it && x.SecondDekstraPoint.Id == kvp.Key)))
                    {
                        var eg = new DekstraEdge(points.Find(x => x.Id == kvp.Key), points.Find(x => x.Id == it), 1);
                        erges.Add(eg);
                    }
                }
                ++index;
            }

            DekstraPoints = points.ToArray();
            Edges = erges.ToArray();
        }
        
        public DAS(DekstraPoint[] dekstraPointsOfgrath, DekstraEdge[] edgesOfgrath)
        {
            DekstraPoints = dekstraPointsOfgrath;
            Edges = edgesOfgrath;
        }

        public void AlgoritmRun(int id)
        {
            foreach (var it in DekstraPoints)
            {
                if (it.Id == id)
                {
                    it.ValueMetka = 0;
                }
                else
                {
                    it.ValueMetka = Int32.MaxValue;
                }
                it.IsChecked = false;
                it.PredDekstraPoint = new DekstraPoint();
            }
            AlgoritmRun(DekstraPoints.First(x => x.Id == id));
        }

        public LinkedList<int> DijkstraSearch(int start, int goal)
        {
            LinkedList<int> result = new LinkedList<int>();
            AlgoritmRun(start);
            List<DekstraPoint> path = GetMinPath(goal);
            foreach (var it in path)
            {
                result.AddFirst(it.Id);
            }
            result.AddFirst(start);
            return result;
        }

        public void AlgoritmRun(DekstraPoint beginp)
        {
            if (this.DekstraPoints.Count() == 0 || this.Edges.Count() == 0)
            {
                throw new Exception("Wrong Graph!");
            }
            else
            {
                BeginDekstraPoint = beginp;
                OneStep(beginp);
                foreach (DekstraPoint point in DekstraPoints)
                {
                    DekstraPoint anotherP = GetAnotherUncheckedPoint();
                    if (anotherP != null)
                    {
                        OneStep(anotherP);
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        public void OneStep(DekstraPoint beginpoint)
        {
            foreach (DekstraPoint nextp in Pred(beginpoint))
            {
                if (nextp.IsChecked == false)
                {
                    float newmetka = beginpoint.ValueMetka + GetEdge(nextp, beginpoint).Weight;
                    if (nextp.ValueMetka > newmetka)
                    {
                        nextp.ValueMetka = newmetka;
                        nextp.PredDekstraPoint = beginpoint;
                    }
                }
            }
            beginpoint.IsChecked = true;
        }

        private IEnumerable<DekstraPoint> Pred(DekstraPoint currpoint)
        {
            IEnumerable<DekstraPoint> firstpoints = from ff in Edges
                where ff.FirstDekstraPoint == currpoint
                select ff.SecondDekstraPoint;
            IEnumerable<DekstraPoint> secondpoints = from sp in Edges
                where sp.SecondDekstraPoint == currpoint
                select sp.FirstDekstraPoint;
            IEnumerable<DekstraPoint> totalpoints = firstpoints.Concat<DekstraPoint>(secondpoints);
            return totalpoints;
        }

        private DekstraEdge GetEdge(DekstraPoint a, DekstraPoint b)
        {
            IEnumerable<DekstraEdge> myr = from reb in Edges
                where
                    (reb.FirstDekstraPoint == a & reb.SecondDekstraPoint == b) ||
                    (reb.SecondDekstraPoint == a & reb.FirstDekstraPoint == b)
                select reb;
            if (myr.Count() > 1 || myr.Count() == 0)
            {
                throw new Exception("Can not find erge!");
            }
            else
            {
                return myr.First();
            }
        }

        private DekstraPoint GetAnotherUncheckedPoint()
        {
            IEnumerable<DekstraPoint> pointsuncheck = from p in DekstraPoints where p.IsChecked == false select p;
            if (pointsuncheck.Count() != 0)
            {
                float minVal = pointsuncheck.First().ValueMetka;
                DekstraPoint minDekstraPoint = pointsuncheck.First();
                foreach (DekstraPoint p in pointsuncheck)
                {
                    if (p.ValueMetka < minVal)
                    {
                        minVal = p.ValueMetka;
                        minDekstraPoint = p;
                    }
                }
                return minDekstraPoint;
            }
            else
            {
                return null;
            }
        }

        public List<DekstraPoint> GetMinPath(int endId)
        {
            DekstraPoint end = DekstraPoints.First(x => x.Id == endId);
            List<DekstraPoint> listOfpoints = new List<DekstraPoint>();
            DekstraPoint tempp = new DekstraPoint();
            tempp = end;
            while (tempp != this.BeginDekstraPoint)
            {
                listOfpoints.Add(tempp);
                tempp = tempp.PredDekstraPoint;
            }

            return listOfpoints;
        }
    }

    public class DekstraEdge
    {
        public DekstraPoint FirstDekstraPoint { get; private set; }
        public DekstraPoint SecondDekstraPoint { get; private set; }
        public float Weight { get; private set; }

        public DekstraEdge(DekstraPoint firstDekstra, DekstraPoint secondDekstra, float valueOfWeight)
        {
            FirstDekstraPoint = firstDekstra;
            SecondDekstraPoint = secondDekstra;
            Weight = valueOfWeight;
        }

    }

    public class DekstraPoint
    {
        public float ValueMetka { get; set; }
        public int Id { get; private set; }
        public bool IsChecked { get; set; }
        public DekstraPoint PredDekstraPoint { get; set; }

        public DekstraPoint(int value, bool ischecked)
        {
            ValueMetka = value;
            IsChecked = ischecked;
            PredDekstraPoint = new DekstraPoint();
        }

        public DekstraPoint(int value, bool ischecked, int id)
        {
            ValueMetka = value;
            IsChecked = ischecked;
            Id = id;
            PredDekstraPoint = new DekstraPoint();
        }

        public DekstraPoint()
        {
        }
    }

    internal static class PrintGrath
    {
        public static List<string> PrintAllMinPaths(DAS da)
        {
            List<string> retListOfPointsAndPaths = new List<string>();
            foreach (DekstraPoint p in da.DekstraPoints)
            {

                if (p != da.BeginDekstraPoint)
                {
                    string s = string.Empty;
                    foreach (DekstraPoint p1 in da.GetMinPath(p.Id))
                    {
                        s += string.Format("{0} ", p1.Id);
                    }
                    retListOfPointsAndPaths.Add(string.Format("DekstraPoint = {0}, MinPath from {1} = {2}", p.Id,
                        da.BeginDekstraPoint.Id, s));
                }

            }
            return retListOfPointsAndPaths;
        }
    }
}