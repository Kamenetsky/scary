﻿using UnityEngine;

using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;

using Scary.GUI;

namespace Scary.Utils
{
    public class CameraController : MonoBehaviour 
    {
        [SerializeField] private float perspectiveZoomSpeed = 0.5f;
        [SerializeField] private float orthoZoomSpeed = 0.5f;
        [SerializeField] private float orthoWheelZoomSpeed = 3f;
        [SerializeField] private float perspectiveWheelZoomSpeed = 3f;

        private Vector3 dragOrigin;

        Vector2?[] oldTouchPositions = {
            null,
            null
        };

        public static bool IsPointerOverUIObject(Canvas canvas, Vector2 screenPosition)
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = screenPosition;
            GraphicRaycaster uiRaycaster = canvas.gameObject.GetComponent<GraphicRaycaster>();
            List<RaycastResult> results = new List<RaycastResult>();
            uiRaycaster.Raycast(eventDataCurrentPosition, results);
            return results.Count > 0;
        }

        void Update()
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }

            foreach (var touch in Input.touches)
            {
                if (IsPointerOverUIObject(WindowManager.Instance.GetBaseLayer(), touch.position))
                {
                    return;
                }
            }
            
            if (Input.touchCount == 0) 
            {
                oldTouchPositions[0] = null;
                oldTouchPositions[1] = null;
            }
            else if (Input.touchCount == 1) 
            {
                if (oldTouchPositions[0] == null || oldTouchPositions[1] != null) 
                {
                    oldTouchPositions[0] = Input.GetTouch(0).position;
                    oldTouchPositions[1] = null;
                }
                else 
                {
                    Vector2 newTouchPosition = Input.GetTouch(0).position;
                    transform.Translate((new Vector3((oldTouchPositions[0] - newTouchPosition).Value.x, 0, (oldTouchPositions[0] - newTouchPosition).Value.y)
                        * GetComponent<Camera>().orthographicSize / GetComponent<Camera>().pixelHeight * 2f), Space.World);
                    oldTouchPositions[0] = newTouchPosition;
                }
            }
            else if (Input.touchCount == 2)
            {
                Touch touchZero = Input.GetTouch(0);
                Touch touchOne = Input.GetTouch(1);

                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

                if (Camera.main.orthographic)
                {
                    Camera.main.orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;
                    Camera.main.orthographicSize = Mathf.Max(Camera.main.orthographicSize, 0.1f);
                }
                else
                {
                    Camera.main.fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed;
                    Camera.main.fieldOfView = Mathf.Clamp(Camera.main.fieldOfView, 30f, 60f);
                }
            }

            if (Mathf.Abs(UnityEngine.Input.GetAxis("wheel")) > 0f)
            {
                if (Camera.main.orthographic)
                {
                    Camera.main.orthographicSize += UnityEngine.Input.GetAxis("wheel") * orthoWheelZoomSpeed;
                    Camera.main.orthographicSize = Mathf.Max(Camera.main.orthographicSize, 0.1f);
                }
                else
                {
                    Camera.main.fieldOfView -= UnityEngine.Input.GetAxis("wheel") * perspectiveWheelZoomSpeed;
                    Camera.main.fieldOfView = Mathf.Clamp(Camera.main.fieldOfView, 30f, 60f);
                }
            }
        }
    }
}