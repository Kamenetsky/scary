﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Scary.Controllers;

namespace Scary.Utils
{
    public class DFS
    {
        private HashSet<int> visited;
        private LinkedList<int> path;
        private int goal;

        Dictionary<int, List<int>> neightborsById;

        public DFS(Dictionary<int, List<int>> neightborsById)
        {
            this.neightborsById = neightborsById;
        }

        public LinkedList<int> DepthFirstSearch(int start, int goal)
        {
            visited = new HashSet<int>();
            path = new LinkedList<int>();
            this.goal = goal;
            DepthFirstSearch(start);
            if (path.Count > 0)
            {
                path.AddFirst(start);
            }
            return path;
        }

        bool DepthFirstSearch(int node)
        {
            if (node == goal)
            {
                return true;
            }
            visited.Add(node);
            Dictionary<int, List<int>> source = neightborsById;

            foreach (var child in source[node].Where(x => !visited.Contains(x)))
            {
                if (DepthFirstSearch(child))
                {
                    path.AddFirst(child);
                    return true;
                }
            }
            return false;
        }

        public List<int> FindReachable(int start, int limit, List<int> result = null, bool isFree = false)
        {
            if (result == null)
            {
                result = new List<int>();
                visited = new HashSet<int>();
                result.Add(start);
                visited.Add(start);
            }

            if (limit == 0)
                return result;

            Dictionary<int, List<int>> source = neightborsById;
            foreach (var child in source[start].Where(x => !visited.Contains(x)))
            {
                result.Add(child);
                visited.Add(child);
                FindReachable(child, limit - 1, result, isFree);
            }
            return result;
        }

        public static List<Relation> GetRelations(LinkedList<int> source)
        {
            List<Relation> result = new List<Relation>();
            List<int> conv = new List<int>(source);
            for (int i = 1; i < conv.Count; ++i)
            {
                result.Add(new Relation{roomId1 = conv[i - 1], roomId2 = conv[i]});
            }
            return result;
        }
    }
}