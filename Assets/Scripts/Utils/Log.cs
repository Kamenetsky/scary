﻿using System;
using System.IO;

namespace Scary.Utils
{
    class Log
    {
        public enum MessageType
        {
            INFO,
            WARNING,
            ERROR,
            DEBUG,
            GAMEMESSAGE
        }
        
        public delegate void onMessage(string message, MessageType messType);
        public static onMessage OnMessage = delegate { };

        static Log()
        {
            if (File.Exists("log.txt"))
                File.Delete("log.txt");
        }
        
        public static void Info(string message)
        {
            using (StreamWriter w = File.AppendText("log.txt"))
            {
                w.Write("\r\nLog Info : ");
                w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                w.WriteLine("  :{0}", message);
                w.WriteLine("-------------------------------");
            }
            OnMessage(message, MessageType.INFO);
        }

        public static void Debug(string message)
        {
            using (StreamWriter w = File.AppendText("log.txt"))
            {
                w.Write("\r\nLog Debug : ");
                w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                w.WriteLine("  :{0}", message);
                w.WriteLine("-------------------------------");
            }
            OnMessage(message, MessageType.DEBUG);
        }

        public static void Error(string error)
        {
            using (StreamWriter w = File.AppendText("log.txt"))
            {
                w.Write("\r\nLog Error : ");
                w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                w.WriteLine("  :{0}", error);
                w.WriteLine("-------------------------------");
            }
            OnMessage(error, MessageType.ERROR);
        }

        public static void Warning(string message)
        {
            using (StreamWriter w = File.AppendText("log.txt"))
            {
                w.Write("\r\nLog Warning : ");
                w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                w.WriteLine("  :{0}", message);
                w.WriteLine("-------------------------------");
            }
            OnMessage(message, MessageType.WARNING);
        }

        public static void GameMessage(string message)
        {
            UnityEngine.Debug.Log(message);
            OnMessage(message, MessageType.GAMEMESSAGE);
        }
    }
}
