﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Scary.Views;
using Scary.Utils;
using Scary.GUI;
using Scary.Models;

using DG.Tweening;

namespace Scary.Controllers
{
    public class Relation
    {
        public int roomId1;
        public int roomId2;
    }

    [System.Serializable]
    public class Transition : Relation
    {
        public Transform point1 = null;
        public Transform point2 = null;

        public GameObject door = null;

        public Transform GetTransformById(int id)
        {
            if (id == roomId1)
                return point1;
            return point2;
        }
    }

    public class MansionController : MonoBehaviour 
    {
        public static MansionController instance = null;
     
        List<RoomView> rooms = new List<RoomView>();
        [SerializeField] List<Transition> transitions = new List<Transition>();

        [SerializeField] float characterSpeed = 3f;
        [SerializeField] RoomView hallView = null;

        [SerializeField] float tapDuration = 0.2f;
        float currentTapTime = 0f;
        bool isTaped = false;

        bool isCharacterMoving = false;
        List<Relation> currentPath;

        List<PersonView> characters = new List<PersonView>();
        private PersonView currentCharacter = null;

        List<MonsterView> monsters = new List<MonsterView>();

        bool mustSelectRoom = false;

        [SerializeField] Vector3 cameraOffset = new Vector3(0f, 0f, -1f);

        public List<MonsterView> GetMonsterViews()
        {
            return monsters;
        }

        void Awake()
        {
            if (instance != null)
                Debug.LogError("Instance of MansionController already exists!");
            instance = this;

            GameField.Instance.OnMonsterSpawn += OnMonsterSpawnCallback;
            GameField.Instance.OnMonsterMove += OnMonsterMoveCallback;
            GameField.Instance.OnNextMonster += OnNextMonsterCallback;
            GameField.Instance.OnMonsterDestroy += OnMonsterDestroy;

            GameField.Instance.OnPlayerMove += OnPlayerMoveCallback;

            GameField.Instance.OnMustSelectRoom += OnMustSelectRoomCallback;
        }

        void OnDestroy()
        {
            GameField.Instance.OnMonsterSpawn -= OnMonsterSpawnCallback;
            GameField.Instance.OnMonsterMove -= OnMonsterMoveCallback;
            GameField.Instance.OnNextMonster -= OnNextMonsterCallback;
            GameField.Instance.OnMonsterDestroy -= OnMonsterDestroy;

            GameField.Instance.OnPlayerMove -= OnPlayerMoveCallback;

            GameField.Instance.OnMustSelectRoom -= OnMustSelectRoomCallback;
        }

        void Start()
        {
            rooms = GetComponentsInChildren<RoomView>().ToList();
            foreach (var it in GameField.Instance.Players)
            {
                var go = Instantiate(Resources.Load<GameObject>("Prefabs/Characters/CharacterView" + it.def.type.ToString())) as GameObject;
                go.transform.SetParent(transform);
                go.transform.position = GetRandomShift(hallView.transform.position, 0.65f);
                PersonView view = go.GetComponent<PersonView>();
                view.person = it;
                view.Select(false);
                characters.Add(view);
            }

            currentCharacter = characters[0];
            currentCharacter.Select(true);

            foreach(var room in rooms)
            {
                //room.gameObject.GetComponent<SpriteRenderer>().enabled = false;
                room.Init(GameField.Instance.rooms.Find(x => x.id == room.id));
            }

            foreach(var it in transitions)
            {
                if (it.point1 == null)
                {
                    var room = rooms.Find(a => a.id == it.roomId1);
                    if (room != null)
                        it.point1 = room.transform;
                }
                if (it.point2 == null)
                {
                    var room = rooms.Find(a => a.id == it.roomId2);
                    if (room != null)
                        it.point2 = room.transform;
                }
            }

            GameField.Instance.StartPlayerTurn();
        }

        public void SetCurrentCharacter(PersonInfo info)
        {
            currentCharacter = characters.Find(x => x.person == info);
            characters.ForEach(x => x.Select(false));
            currentCharacter.Select(true);
            MoveCameraToObject(currentCharacter.transform);
        }

        public void MoveCameraToObject(Transform view)
        {
            if (view == null)
                return;
            Camera.main.transform.DOMove(cameraOffset + 
                new Vector3(view.transform.position.x, Camera.main.transform.position.y, view.transform.position.z),
                0.5f);
        }

        public void ShowAvailablePathes()
        {
            //PersonInfo info = currentCharacter.GetComponent<PersonView>().person;
            //var reachable = GameField.search.FindReachable(info.mover.CurrentRoomId, info.movesCount);

            //foreach (var room in rooms)
                //room.gameObject.GetComponent<SpriteRenderer>().enabled = reachable.Contains(room.id);
        }

        public void ClearAvailablePathes()
        {
            //foreach (var room in rooms)
                //room.gameObject.GetComponent<SpriteRenderer>().enabled = false;
        }

        public void SwitchLight(bool value, int roomId)
        {
            rooms.Find(x => x.id == roomId).SetLight(value);
        }

        void Update ()
        {
            if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                return;
            
            if (isTaped)
                currentTapTime += Time.deltaTime;
            
            if (Input.touchCount == 1 && !isTaped)
            {
                isTaped = true;
            }
            else if (Input.touchCount == 1 && isTaped && Input.GetTouch(0).phase == TouchPhase.Ended && currentTapTime < tapDuration)
            {
                if (isCharacterMoving)
                    return;
                
                isTaped = false;
                currentTapTime = 0f;
                var room = RaycastUtils.FindCollidedControl<RoomView>(Input.GetTouch(0));
                if (room != null)
                {
                    if (mustSelectRoom)
                    {
                        if (roomSelectionFilter != null && !roomSelectionFilter.Exists(x => x.id == room.id))
                            return;
                        mustSelectRoom = false;
                        GameField.Instance.SetSelectedRoom(room.GetInfo());
                        return;
                    }
                    
                    if (room.id == currentCharacter.person.mover.CurrentRoomId)
                        return;

                    //TODO make for dijkstra algorithm
                    var reachable = GameField.searchChecker.FindReachable(currentCharacter.person.mover.CurrentRoomId, 
                        currentCharacter.person.movesCount);
                    if (!reachable.Contains(room.id))
                        return;

                    GameField.Instance.BuildPathAndMove(room.id);
                }
            }
            else if (Input.touchCount != 1)
            {
                isTaped = false;
                currentTapTime = 0f;
            }
    	}

        private List<RoomInfo> roomSelectionFilter = null;
        void OnMustSelectRoomCallback(List<RoomInfo> filter = null)
        {
            roomSelectionFilter = filter;
            mustSelectRoom = true;
        }

        #region Player move
        void OnPlayerMoveCallback(PersonInfo personInfo, bool force)
        {
            PersonView view = characters.Find(x => x.person == personInfo);

            int currId = personInfo.mover.PrevRoomId;
            int nextId = personInfo.mover.CurrentRoomId;

            var currTransition = transitions.Find(a => (a.roomId1 == currId && a.roomId2 == nextId) ||
                                                           (a.roomId1 == nextId && a.roomId2 == currId));

            if (currTransition == null)
            {
                RoomView rv = rooms.Find(x => x.id == nextId);
                float dur = (view.transform.position - rv.center.position).magnitude / characterSpeed;
                //view.transform.LookAt(rv.center.position);
                view.transform.DOMove(rv.center.position, dur).SetEase(Ease.Linear).OnComplete(() =>
                {
                    GameField.Instance.PlayerCompletePath(personInfo);
                });
                return;
            }

            Vector3[] path = new Vector3[2];
            path[0] = currTransition.GetTransformById(currId).position;
            path[1] = currTransition.GetTransformById(nextId).position;
            float duration = (view.transform.position - path[0]).magnitude / characterSpeed;
            duration += (path[1] - path[0]).magnitude / characterSpeed;
            //view.transform.LookAt(path[1]);
            currTransition.door.GetComponentInChildren<Animator>().Play("door_open");
            view.transform.DOPath(path, duration).SetEase(Ease.Linear).OnComplete(() =>
            {
                GameField.Instance.PlayerMoveComplete(personInfo);
                currTransition.door.GetComponentInChildren<Animator>().Play("door_close");
                if (nextId == personInfo.mover.GetEndPathPoint())
                {
                    view.transform.DOMove(GetRandomShift(view.transform.position, 0.3f), 0.3f).SetEase(Ease.Linear);
                    GameField.Instance.PlayerCompletePath(personInfo);
                }
            });
        }
        #endregion

        #region Monsters move
        void OnMonsterSpawnCallback(MonsterInfo monsterInfo)
        {
            RoomView rw = rooms.Find(x => x.id == monsterInfo.mover.CurrentRoomId);
            GameObject prefab = Resources.Load<GameObject>("Prefabs/Monsters/" + monsterInfo.name);
            if (prefab == null)
                prefab = Resources.Load<GameObject>("Prefabs/Monsters/default");
            var go = Instantiate(prefab);
            go.name = monsterInfo.name;
            go.transform.SetParent(transform);
            go.transform.position = GetRandomShift(rw.transform.position, 0.3f);
            MonsterView mv = go.GetComponent<MonsterView>();
            mv.Init(monsterInfo);
            monsters.Add(mv);
        }

        void OnMonsterMoveCallback(MonsterInfo monsterInfo, bool force)
        {
            MonsterView view = monsters.Find(x => x.monster == monsterInfo);
            
            int currId = monsterInfo.mover.PrevRoomId;
            int nextId = monsterInfo.mover.CurrentRoomId;

            var currTransition = transitions.Find(a => (a.roomId1 == currId && a.roomId2 == nextId) ||
                                                           (a.roomId1 == nextId && a.roomId2 == currId));

            if (currTransition == null)
            {
                RoomView rv = rooms.Find(x => x.id == nextId);
                float dur = (view.transform.position - rv.center.position).magnitude / characterSpeed;
                view.transform.DOMove(rv.center.position, dur).SetEase(Ease.Linear).OnComplete(() =>
                {
                    if (!force)
                        GameField.Instance.MonsterMoveComplete(monsterInfo);
                    else
                        view.transform.DOMove(GetRandomShift(view.transform.position, 0.3f), 0.3f).SetEase(Ease.Linear);
                });
                return;
            }

            Vector3[] path = new Vector3[2];
            path[0] = currTransition.GetTransformById(currId).position;
            path[1] = currTransition.GetTransformById(nextId).position;
            float duration = (view.transform.position - path[0]).magnitude / characterSpeed;
            duration += (path[1] - path[0]).magnitude / characterSpeed;
            currTransition.door.GetComponentInChildren<Animator>().Play("door_open");
            view.transform.LookAt(path[1]);
            view.transform.DOPath(path, duration).SetEase(Ease.Linear).OnComplete(() =>
            {
                currTransition.door.GetComponentInChildren<Animator>().Play("door_close");
                if (!force)
                {
                    GameField.Instance.MonsterMoveComplete(monsterInfo);
                    if (nextId == monsterInfo.mover.GetEndPathPoint())
                    {
                        GameField.Instance.MonsterCompletePath(monsterInfo);
                    }
                }
                else
                {
                    view.transform.DOMove(GetRandomShift(view.transform.position, 0.3f), 0.3f).SetEase(Ease.Linear);
                }
            });
        }

        void OnNextMonsterCallback(MonsterInfo oldMonsterInfo, MonsterInfo monsterInfo)
        {
            MonsterView view = monsters.Find(x => x.monster == oldMonsterInfo);
            if (view != null)
                view.transform.DOMove(GetRandomShift(view.transform.position, 0.3f), 0.3f).SetEase(Ease.Linear);
        }

        void OnMonsterDestroy(MonsterInfo monsterInfo)
        {
            MonsterView view = monsters.Find(x => x.monster == monsterInfo);
            Destroy(view.gameObject);
            monsters.Remove(view);
        }
        #endregion

        Vector3 GetRandomShift(Vector3 origin, float distance)
        {
            return new Vector3(origin.x + Random.Range(-distance, distance), origin.y, origin.z + Random.Range(-distance, distance));
        }
    }
}
