﻿using UnityEngine;
using System.Collections;

namespace Scary
{
    public class Constants
    {
        public static int movesToTurnLight = 2;
        public static int movesToFindItem = 2;
        public static int movesToDarkRoom = 2;
        public static int minPlayersCount = 4;
        public static int maxPlayersCount = 4;
        public static int maxMonstersCount = 6;
        public static int monstersPlayersCountDiff = 2;
        public static int maxItemsCount = 3;
    }
}
