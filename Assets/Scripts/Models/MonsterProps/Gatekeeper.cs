﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Gatekeeper : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            MonsterInfo.OnDieByItem += OnDieByItem;
        }

        void OnDieByItem(MonsterInfo monster, Items.GameItem item)
        {
            MonsterInfo.OnDieByItem -= OnDieByItem;
            if (this.monster == monster && item.GetOwner() != null && monster.canUseAdditionalProperty)
                item.GetOwner().canRessurect = false;
        }
    }
}
