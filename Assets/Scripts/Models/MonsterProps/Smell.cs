﻿using Scary.Models.Items;
using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Smell : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            monster.findNearest = false;
        }

        public override void Deactivate()
        {
            monster.findNearest = true;
            base.Deactivate();
        }

        public override void Reactivate()
        {
            monster.findNearest = true;
            base.Reactivate();
        }
    }
}
