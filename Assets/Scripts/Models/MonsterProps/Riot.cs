﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Riot : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            MonsterInfo.OnStartAttackTarget += OnStartAttackTarget;
        }

        void OnStartAttackTarget(PersonInfo person, MonsterInfo monster)
        {
            if (monster == this.monster && monster.canUseAdditionalProperty)
            {
                if (person.GetItemsCount() == 0)
                    return;
                int index = Random.Range(0, person.GetItemsCount());
                person.RemoveItem(person.GetItem(index));
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            MonsterInfo.OnStartAttackTarget -= OnStartAttackTarget;
        }
    }
}
