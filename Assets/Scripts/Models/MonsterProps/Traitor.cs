﻿using Scary.Models.Items;
using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Traitor : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            CheckRoom();
            GameField.Instance.OnMonsterMove += OnMonsterMove;
        }

        void OnMonsterMove(MonsterInfo monster, bool force)
        {
            if (this.monster != monster)
                return;
            CheckRoom();
        }

        void CheckRoom()
        {
            foreach (var it in GameField.Instance.GetMonstersInRoom(monster.mover.CurrentRoomId))
            {
                if (it != monster)
                    it.RemoveHits(1);
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            GameField.Instance.OnMonsterMove -= OnMonsterMove;
        }
    }
}
