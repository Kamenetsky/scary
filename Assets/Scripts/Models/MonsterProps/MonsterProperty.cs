﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class MonsterPropertyDef : BasePropertyDef
    {
    }

    public abstract class MonsterProperty : System.IDisposable
    {
        protected MonsterInfo monster = null;

        public virtual void Activate(MonsterInfo monster)
        {
            this.monster = monster;
        }

        public virtual void Deactivate()
        {
            GameField.Instance.MonsterRefresh(monster);
        }

        public virtual void Reactivate()
        {
            GameField.Instance.MonsterRefresh(monster);
        }

        public virtual void Dispose() { }
    }
}
