﻿using Scary.Models.Items;
using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Resident : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            CheckRoom();
            GameField.Instance.OnMonsterCompletePath += OnMonsterCompletePath;
        }

        void OnMonsterCompletePath(MonsterInfo monster)
        {
            if (this.monster == monster && monster.canUseAdditionalProperty)
                CheckRoom();
        }

        void CheckRoom()
        {
            if (GameField.Instance.rooms.Find(x => x.id == monster.mover.CurrentRoomId).type != RoomType.Corridor)
            {
                monster.health += 1;
                GameField.Instance.MonsterRefresh(monster);
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            GameField.Instance.OnMonsterCompletePath -= OnMonsterCompletePath;
        }
    }
}
