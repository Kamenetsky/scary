﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Statue : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            monster.damage = 0;
            monster.def.maxMovesCount = 0;
            monster.health = 5;
            monster.def.maxHealth = 5;
            GameField.Instance.MonsterRefresh(monster);
            GameField.Instance.OnHourComplete += OnHourComplete;
        }

        void OnHourComplete()
        {
            monster.ActivateAdditionalProp();
            GameField.Instance.OnHourComplete -= OnHourComplete;
        }

        public override void Dispose()
        {
            base.Dispose();
            monster.def.maxHealth = GameField.defs.monsters.Find(x => x.name == "Statue").maxHealth;
            monster.health = monster.def.maxHealth;
        }
    }
}
