﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Sluggish : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            if (monster.def.maxHealth > 1)
                monster.def.maxHealth -= 1;
            if (monster.health > 1)
                monster.health -= 1;
            GameField.Instance.MonsterRefresh(monster);
        }
    }
}