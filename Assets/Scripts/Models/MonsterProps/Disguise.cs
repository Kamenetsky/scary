﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Disguise : MonsterProperty
    {
        private int health;
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            health = monster.health;
            monster.Die();
            GameField.Instance.OnMonsterSpawn += OnMonsterSpawn;
            GameField.Instance.SpawnMonsterInRoom("", monster.mover.CurrentRoomId);
        }

        void OnMonsterSpawn(MonsterInfo newMonster)
        {
            GameField.Instance.OnMonsterSpawn -= OnMonsterSpawn;
            newMonster.health = health;
            GameField.Instance.MonsterRefresh(newMonster);
        }
    }
}
