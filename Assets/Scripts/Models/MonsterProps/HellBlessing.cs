﻿using System.Collections.Generic;
using Scary.Models.Items;
using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class HellBlessing : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            GameField.Instance.OnMonsterDestroy += OnMonsterDestroy;
            foreach (var it in GameField.Instance.Monsters)
            {
                if (it != monster)
                {
                    it.def.vulnerabilities.Remove(DamageType.Fire);
                    it.def.vulnerabilities.Remove(DamageType.Physics);
                    it.def.vulnerabilities.Remove(DamageType.Sanctification);
                }
            }
        }

        public override void Deactivate()
        {
            foreach (var it in GameField.Instance.Monsters)
            {
                if (it != monster)
                {
                    MonsterDef def = GameField.defs.monsters.Find(x => x.name == it.name);
                    it.def.vulnerabilities = new List<DamageType>(def.vulnerabilities);
                }
            }
            base.Deactivate();
        }

        public override void Reactivate()
        {
            foreach (var it in GameField.Instance.Monsters)
            {
                if (it != monster)
                {
                    it.def.vulnerabilities.Remove(DamageType.Fire);
                    it.def.vulnerabilities.Remove(DamageType.Physics);
                    it.def.vulnerabilities.Remove(DamageType.Sanctification);
                }
            }
            base.Reactivate();
        }

        void OnMonsterDestroy(MonsterInfo info)
        {
            if (monster == info)
            {
                foreach (var it in GameField.Instance.Monsters)
                {
                    if (it != monster)
                    {
                        it.def.vulnerabilities = new List<DamageType>(GameField.defs.monsters.Find(x => x.name == it.name).vulnerabilities);
                    }
                }
            }
        }
    }
}
