﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Lazy : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            if (monster.def.maxMovesCount > 1)
                monster.def.maxMovesCount -= 1;
            GameField.Instance.MonsterRefresh(monster);
        }
    }
}