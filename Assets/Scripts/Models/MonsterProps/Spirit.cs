﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Spirit : MonsterProperty
    {
        private bool hourComplete = false;
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            if (GameField.Instance.currentTime.Hour >= 3)
            {
                monster.Die();
            }
            else
            {
                monster.isImmortal = true;
                GameField.Instance.OnHourComplete += OnHourComplete;
            }
        }

        public override void Deactivate()
        {
            monster.isImmortal = false;
            base.Deactivate();
        }

        public override void Reactivate()
        {
            if (!hourComplete)
                monster.isImmortal = true;
            base.Reactivate();
        }

        void OnHourComplete()
        {
            monster.isImmortal = false;
            hourComplete = true;
            GameField.Instance.OnHourComplete -= OnHourComplete;
        }
    }
}
