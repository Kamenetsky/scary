﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Fury : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            monster.damage += 1;
            GameField.Instance.MonsterRefresh(monster);
        }

        public override void Deactivate()
        {
            monster.damage -= 1;
            base.Deactivate();
        }

        public override void Reactivate()
        {
            monster.damage += 1;
            base.Reactivate();
        }
    }
}
