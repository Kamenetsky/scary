﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Devastate : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            MonsterInfo.OnAttackTarget += OnAttackTarget;
        }

        void OnAttackTarget(PersonInfo person, MonsterInfo monster)
        {
            if (this.monster != monster || !monster.canUseAdditionalProperty)
                return;

            foreach (var it in GameField.Instance.GetPlayersInRoom(person.mover.CurrentRoomId))
            {
                if (it == person)
                    continue;

                monster.target = it;
                monster.AttackTarget();
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            MonsterInfo.OnAttackTarget -= OnAttackTarget;
        }
    }
}
