﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Energy : MonsterProperty
    {      
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            monster.def.maxHealth += 1;
            monster.def.maxMovesCount += 1;
            monster.health += 1;
            GameField.Instance.MonsterRefresh(monster);
        }

        public override void Deactivate()
        {
            monster.def.maxHealth -= 1;
            monster.def.maxMovesCount -= 1;
            monster.health -= 1;
            base.Deactivate();
        }

        public override void Reactivate()
        {
            monster.def.maxHealth += 1;
            monster.def.maxMovesCount += 1;
            monster.health += 1;
            base.Reactivate();
        }
    }
}
