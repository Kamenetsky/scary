﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class DemonShield : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            monster.isImmortal = true;
            GameField.Instance.OnNextMonster += OnNextMonster;
        }

        void OnNextMonster(MonsterInfo monster, MonsterInfo nextMonster)
        {
            if (nextMonster == this.monster)
            {
                monster.isImmortal = false;
                GameField.Instance.OnNextMonster -= OnNextMonster;
            }
        }

        public override void Deactivate()
        {
            monster.isImmortal = false;
            base.Deactivate();
        }
    }
}