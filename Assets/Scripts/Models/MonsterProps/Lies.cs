﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Lies : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            MonsterInfo.OnChangesHits += OnRemoveHits;
        }

        void OnRemoveHits(int damage, CreatureInfo<MonsterDef> info, CreatureInfo<CreatureDef> damager)
        {
            if (info == monster && damage >= monster.health && monster.canUseAdditionalProperty)
            {
                GameField.Instance.SpawnMonsterInRoom("", monster.mover.CurrentRoomId);
                MonsterInfo.OnChangesHits -= OnRemoveHits;
            }
        }
    }
}
