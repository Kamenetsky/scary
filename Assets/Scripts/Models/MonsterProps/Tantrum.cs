﻿using Scary.Models.Items;
using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Tantrum : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            GameField.Instance.OnMonsterDestroy += OnMonsterDestroy;
        }

        void OnMonsterDestroy(MonsterInfo monster)
        {
            if (monster == this.monster)
            {
                GameField.Instance.OnMonsterDestroy -= OnMonsterDestroy;
                if (!monster.canUseAdditionalProperty)
                    return;
                foreach (var it in GameField.Instance.Monsters)
                {
                    it.ActivateAdditionalProp();
                }
            }
        }
    }
}
