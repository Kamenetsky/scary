﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Lord : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            Items.GameItem.OnAttack += OnAttackByItem;
        }

        void OnAttackByItem(Items.GameItem item, MonsterInfo info)
        {
            if (info == monster && monster.canUseAdditionalProperty)
            {
                PersonInfo pi = item.GetOwner();
                pi.RemoveItem(item);
                pi.AddItem(ItemFactory.CreateItem(ItemFactory.GetRandom(), pi));
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            Items.GameItem.OnAttack -= OnAttackByItem;
        }
    }
}
