﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Child : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            GameField.Instance.OnMonsterCompletePath += OnMonsterCompletePath;
        }

        void OnMonsterCompletePath(MonsterInfo monster)
        {
            if (monster == this.monster && monster.canUseAdditionalProperty)
                GameField.Instance.TurnLight(monster.mover.CurrentRoomId, false, null);
        }

        public override void Dispose()
        {
            base.Dispose();
            GameField.Instance.OnMonsterCompletePath -= OnMonsterCompletePath;
        }
    }
}
