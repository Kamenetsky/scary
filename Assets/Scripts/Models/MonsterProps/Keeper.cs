﻿using System.Collections.Generic;
using Scary.Models.Items;
using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Keeper : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            GameItem.OnAttack += OnAttackByItem;
        }

        void OnAttackByItem(GameItem item, MonsterInfo info)
        {
            if (info == monster && item.def.damage >= info.health && info.canUseAdditionalProperty)
            {
                PersonInfo pi = item.GetOwner();
                GameItem.OnAttack -= OnAttackByItem;
                GameItem gi = ItemFactory.CreateItem(ItemFactory.GetRandom(), pi);
                pi.AddItem(gi);
                gi.ActivateAdditionalProp();
            }
        }
    }
}
