﻿using Scary.Models.Items;
using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Incorporeal : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            monster.def.vulnerabilities.Remove(DamageType.Physics);
            monster.def.vulnerabilities.Add(DamageType.Fire);
            monster.SetGhost(true);
        }

        public override void Deactivate()
        {
            monster.def.vulnerabilities.Add(DamageType.Physics);
            monster.def.vulnerabilities.Remove(DamageType.Fire);
            monster.SetGhost(false);
            base.Deactivate();
        }

        public override void Reactivate()
        {
            Activate(monster);
        }
    }
}
