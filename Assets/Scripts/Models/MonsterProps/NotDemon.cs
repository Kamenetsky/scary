﻿using Scary.Models.Items;
using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class NotDemon : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            monster.def.vulnerabilities.Add(DamageType.Fire);
            monster.def.vulnerabilities.Remove(DamageType.Sanctification);
        }

        public override void Deactivate()
        {
            monster.def.vulnerabilities.Remove(DamageType.Fire);
            monster.def.vulnerabilities.Add(DamageType.Sanctification);
            base.Deactivate();
        }

        public override void Reactivate()
        {
            Activate(monster);
        }
    }
}
