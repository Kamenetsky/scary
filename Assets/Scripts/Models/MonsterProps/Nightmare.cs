﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Nightmare : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            MonsterInfo.OnChangesHits += OnRemoveHits;
        }

        void OnRemoveHits(int damage, CreatureInfo<MonsterDef> info, CreatureInfo<CreatureDef> damager)
        {
            if (info == monster && damage >= monster.health)
            {
                if (monster.canUseAdditionalProperty)
                    monster.Reset();
                MonsterInfo.OnChangesHits -= OnRemoveHits;
            }
        }
    }
}
