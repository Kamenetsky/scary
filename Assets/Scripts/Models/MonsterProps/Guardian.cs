﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Guardian : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            if (GameField.Instance.currentTime.Hour >= 3)
            {
                monster.def.maxHealth += 2;
                monster.health += 2;
                GameField.Instance.MonsterRefresh(monster);
                return;
            }
            base.Activate(monster);
            GameField.Instance.OnHourComplete += OnHourComplete;
        }

        void OnHourComplete()
        {
            if (GameField.Instance.currentTime.Hour >= 3)
            {
                GameField.Instance.OnHourComplete -= OnHourComplete;
                monster.def.maxHealth += 2;
                monster.health += 2;
                GameField.Instance.MonsterRefresh(monster);
            }
        }

        public override void Deactivate()
        {
            if (GameField.Instance.currentTime.Hour >= 3)
            {
                monster.def.maxHealth -= 2;
                monster.health -= 2;
            }
            base.Deactivate();
        }

        public override void Reactivate()
        {
            if (GameField.Instance.currentTime.Hour >= 3)
            {
                monster.def.maxHealth += 2;
                monster.health += 2;
            }
            base.Reactivate();
        }
    }
}
