﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Eater : MonsterProperty
    {
        private int initHealth;
        
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            GameField.Instance.OnMonsterMove += OnMonsterMove;
            GameField.Instance.OnNextMonster += OnNextMonster;
            initHealth = monster.health;
        }

        void OnMonsterMove(MonsterInfo info, bool force)
        {
            if (monster == info && monster.canUseAdditionalProperty && 
                GameField.Instance.rooms.Find(x => x.id == info.mover.CurrentRoomId).isLightOn)
            {
                info.health = initHealth + 2;
            }
            else
            {
                info.health = initHealth;
            }
            GameField.Instance.MonsterRefresh(info);
        }

        void OnNextMonster(MonsterInfo oldMonster, MonsterInfo nextMonster)
        {
            if (nextMonster == monster)
            {
                initHealth = monster.health;
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            GameField.Instance.OnMonsterMove -= OnMonsterMove;
        }
    }
}
