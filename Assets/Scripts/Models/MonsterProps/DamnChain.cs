﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class DamnChain : MonsterProperty
    {
        readonly List<PersonInfo> damaged = new List<PersonInfo>();

        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            Items.GameItem.OnAttack += OnAttackByItem;
        }

        void OnAttackByItem(Items.GameItem item, MonsterInfo info)
        {
            if (info != monster || !monster.canUseAdditionalProperty)
                return;

            if (!damaged.Contains(item.GetOwner()))
            {
                item.GetOwner().RemoveHits(1);
                damaged.Add(item.GetOwner());
            }              
        }

        public override void Dispose()
        {
            base.Dispose();
            Items.GameItem.OnAttack -= OnAttackByItem;
        }
    }
}
