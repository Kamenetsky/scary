﻿using Scary.Models.Items;
using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Judge : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            MonsterInfo.OnDieByItem += OnDieByItem;
        }

        void OnDieByItem(MonsterInfo monster, GameItem item)
        {
            MonsterInfo.OnDieByItem -= OnDieByItem;
            if (this.monster == monster && item.GetOwner() != null && monster.canUseAdditionalProperty)
            {
                item.GetOwner().health = 3;
                GameField.Instance.PlayerRefresh(item.GetOwner());
            }
              
        }
    }
}
