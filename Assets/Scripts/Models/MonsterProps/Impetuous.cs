﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Impetuous : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            monster.def.maxMovesCount *= 2;
            GameField.Instance.MonsterRefresh(monster);
        }

        public override void Deactivate()
        {
            monster.def.maxMovesCount /= 2;
            base.Deactivate();
        }

        public override void Reactivate()
        {
            monster.def.maxMovesCount *= 2;
            base.Reactivate();
        }
    }
}