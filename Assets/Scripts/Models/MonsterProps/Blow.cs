﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Blow : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            Items.GameItem.OnAttack += OnAttackByItem;
        }

        void OnAttackByItem(Items.GameItem item, MonsterInfo info)
        {
            if (item.def.damage >= info.health)
            {
                Items.GameItem.OnAttack -= OnAttackByItem;
                if (monster.canUseAdditionalProperty)
                    item.GetOwner().RemoveHits(1);
            }
        }
    }
}
