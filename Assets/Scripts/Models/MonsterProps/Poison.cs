﻿using Scary.Models.Items;
using UnityEngine;
using System.Collections;

namespace Scary.Models.MonsterProps
{
    public class Poison : MonsterProperty
    {
        public override void Activate(MonsterInfo monster)
        {
            base.Activate(monster);
            monster.damageBonus = 1;
        }

        public override void Deactivate()
        {
            monster.damageBonus = 0;
            base.Deactivate();
        }

        public override void Reactivate()
        {
            Activate(monster);
        }
    }
}
