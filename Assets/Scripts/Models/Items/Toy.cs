﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class Toy : GameItemUsable
    {
        public Toy(ItemDef def, PersonInfo person)
            : base(def, person)
        {
        }

        public override void Use()
        {
            owner.isImmortal = true;
            GameField.Instance.OnNextPlayer += OnNextPlayer;
            owner.RemoveItem(this);
        }

        void OnNextPlayer(PersonInfo oldInfo, PersonInfo info)
        {
            if (info == owner)
            {
                owner.isImmortal = false;
                GameField.Instance.OnNextPlayer -= OnNextPlayer;
                Drop();
            }
        }
    }
}
