﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class Revolver : GameItem
    {
        public Revolver(ItemDef def, PersonInfo person)
            : base(def, person)
        {
        }

        public override void Attack(MonsterInfo monster)
        {
            base.Attack(monster);
            GameField.Instance.preparePoints++;
        }
    }
}