﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class Boomerang : GameItem
    {
        public Boomerang(ItemDef def, PersonInfo person)
            : base(def, person)
        {
        }

        public override void Drop()
        {
        }
    }
}
