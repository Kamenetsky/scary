﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Scary.Models.ItemProps;

namespace Scary.Models.Items
{
    public enum DamageType
    {
        None,
        Fire,
        Physics,
        Sanctification
    }

    public enum ItemType
    {
        Default,
        Weapon,
        Relic
    }
    public class ItemDef
    {
        public string name;
        public ItemType itemType;
        public DamageType damageType;
        public int damage = 1;
        public string monsterName;
        public int amountInDeck;
        public int initAmountInDeck;

        public ItemDef()
        {
            initAmountInDeck = amountInDeck;
        }

        public ItemDef(ItemDef def)
        {
            name = def.name;
            itemType = def.itemType;
            damageType = def.damageType;
            damage = def.damage;
            monsterName = def.monsterName;
            initAmountInDeck = def.amountInDeck;
        }
    }

    public abstract class GameItem : IDisposable
    {
        public static event Action<GameItem, MonsterInfo> OnAttack = delegate { };
        public static event Action<GameItem> OnActivateProperty = delegate { }; 
        
        public ItemDef def;
        protected PersonInfo owner = null;
        protected bool isPlush = false;
        protected bool isShovel = false;
        protected bool isLight = false;

        public bool canUseAdditionalProperty = true;
        public string additionalPropertyName;
        protected ItemProperty additionalProperty = null;

        public int useMovesCount = 1;

        public GameItem(ItemDef def, PersonInfo person)
        {
            this.def = def;
            owner = person;
        }

        public PersonInfo GetOwner()
        {
            return owner;
        }

        public virtual void SetOwner(PersonInfo info)
        {
            owner = info;
        }

        public virtual bool CanUse()
        {
            return def.itemType == ItemType.Default;
        }

        public virtual bool IsAttackable()
        {
            return (def.itemType == ItemType.Weapon) && !IsPlush();
        }

        public virtual bool CanAttack(MonsterInfo monster)
        {
            return monster.def.vulnerabilities.Contains(def.damageType);
        }

        public virtual bool AllowToDig()
        {
            return isShovel;
        }

        public virtual void SetShovel()
        {
            isShovel = true;
            def = new ItemDef();
        }

        public virtual bool IsLight()
        {
            return isLight;
        }

        public virtual void SetLight(bool val)
        {
            isLight = val;
        }

        public virtual int GetSlotsCount()
        {
            return 1;
        }

        public virtual void SetPlush()
        {
            isPlush = true;
        }

        public virtual bool IsPlush()
        {
            return isPlush;
        }

        public virtual void Attack(MonsterInfo monster)
        {
            if (!IsAttackable() || !CanAttack(monster))
                return;

            Debug.Log(string.Format("Player {0} attacks monster {1} by item {2}", GetOwner().def.type.ToString(), 
                monster.def.name, def.name));
            ActivateAdditionalProp();
            if (canUseAdditionalProperty)
                additionalProperty.Activate(monster);
            OnAttack(this, monster);
            monster.RemoveHits(def.damage, this);
            if (!canUseAdditionalProperty)
                canUseAdditionalProperty = true;
        }

        public virtual void KillMonster(MonsterInfo monster)
        {
            if (additionalProperty != null)
                additionalProperty.KillMonster(monster);
        }

        public virtual void Drop()
        {
            if (additionalProperty != null)
                additionalProperty.Dispose();

            owner.RemoveItem(this);
        }

        public void ActivateAdditionalProp()
        {
            if (additionalProperty != null)
                return;

            if (!GameField.defs.itemProps.Exists(x => x.amountInDeck > 0))
                throw new Exception("Item props defs are empty!");

            List<ItemPropertyDef> itemPropDefs = GameField.defs.itemProps.FindAll(x => x.amountInDeck > 0);
            ItemPropertyDef itemPropDef = itemPropDefs[UnityEngine.Random.Range(0, itemPropDefs.Count)];
            itemPropDef.amountInDeck--;

            //for tests
            //itemPropDef = GameField.defs.itemProps.Find(x => x.name == "Hellish");
            Debug.Log(string.Format("Item {0} activates property {1}", def.name, itemPropDef.name));
            additionalPropertyName = itemPropDef.name;
            additionalProperty = (ItemProperty)Activator.CreateInstance(Type.GetType("Scary.Models.ItemProps."
                + itemPropDef.name), new ItemPropertyDef(itemPropDef), this);
            OnActivateProperty(this);
        }

        public void KillAdditionalProp()
        {
            if (additionalProperty != null)
            {
                additionalProperty.Dispose();
                additionalProperty = null;
                additionalPropertyName = string.Empty;
            }
        }

        public virtual void Dispose() { }
    }
}
