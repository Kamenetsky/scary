﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class Sword : GameItem
    {
        public Sword(ItemDef def, PersonInfo person)
            : base(def, person)
        {
        }

        public override void Attack(MonsterInfo monster)
        {
            if (owner.currentMovesCount >= 5)
            {
                def.damage = 2;
            }
            else
            {
                def.damage = 1;
            }
            base.Attack(monster);
        }
    }
}
