﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class DiamondsAce : GameItemUsable
    {
        public DiamondsAce(ItemDef def, PersonInfo person)
            : base(def, person)
        {
        }

        public override void Use()
        {
            owner.movesCount += Random.Range(1, 7);
            GameField.Instance.PlayerRefresh(owner);
            Drop();
        }
    }
}
