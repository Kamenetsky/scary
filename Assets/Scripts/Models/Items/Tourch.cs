﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class Tourch : GameItem
    {
        public Tourch(ItemDef def, PersonInfo person)
            : base(def, person)
        {
            GameField.Instance.OnHourComplete += OnHourTick;
        }

        public override bool IsLight()
        {
            return true;
        }

        void OnHourTick()
        {
            GameField.Instance.OnHourComplete -= OnHourTick;
            Drop();
        }
    }
}
