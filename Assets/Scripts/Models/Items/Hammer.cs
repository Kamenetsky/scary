﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class Hammer : GameItem
    {
        public Hammer(ItemDef def, PersonInfo person)
            : base(def, person)
        {
            this.def.damage = 2;
        }
       
        public override int GetSlotsCount()
        {
            return 2;
        }
    }
}
