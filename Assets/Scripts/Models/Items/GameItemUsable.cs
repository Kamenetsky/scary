﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public abstract class GameItemUsable : GameItem
    {
        public GameItemUsable(ItemDef def, PersonInfo person)
            : base(def, person)
        {
        }

        public override bool CanUse()
        {
            return true;
        }

        public abstract void Use();
    }
}
