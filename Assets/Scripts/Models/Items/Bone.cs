﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class Bone : GameItemUsable
    {
        public Bone(ItemDef def, PersonInfo person)
            : base(def, person)
        {
        }

        public override void Use()
        {
            GameField.Instance.MustSelectPlayer();
            GameField.Instance.OnSelectedPlayer += OnSelectedPlayer;
        }

        void OnSelectedPlayer(PersonInfo player)
        {
            GameField.Instance.OnSelectedPlayer -= OnSelectedPlayer;
            if (player.health < player.def.maxHealth)
            {
                player.health += 4;
                if (player.health > player.def.maxHealth)
                    player.health = player.def.maxHealth;
            }
            Drop();
        }
    }
}
