﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class Bible : GameItem
    {
        public Bible(ItemDef def, PersonInfo person)
            : base(def, person)
        {
        }

        public override void Attack(MonsterInfo monster)
        {
            bool hasCrosifix = false;

            for (int i = 0; i < owner.GetItemsCount(); ++i)
            {
                if (owner.GetItem(i) is Crucifix)
                {
                    hasCrosifix = true;
                    break;
                }
            }

            if (hasCrosifix)
            {
                def.damage = 2;
            }
            else
            {
                def.damage = 1;
            }
            base.Attack(monster);
        }
    }
}
