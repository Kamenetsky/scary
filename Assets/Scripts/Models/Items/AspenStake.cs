﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class AspenStake : GameItem
    {
        public AspenStake(ItemDef def, PersonInfo person)
            : base(def, person)
        {
        }

        public override bool CanAttack(MonsterInfo monster)
        {
            return base.CanAttack(monster) || monster.IsVampire();
        }
    }
}
