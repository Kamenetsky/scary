﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class Crossbow : GameItem
    {
        public Crossbow(ItemDef def, PersonInfo person)
            : base(def, person)
        {
        }

        public override void Attack(MonsterInfo monster)
        {
            base.Attack(monster);
            monster.RemoveHits(1000);//HACK
            owner.RemoveItem(this);
        }
    }
}
