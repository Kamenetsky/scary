﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class Jerrycan : GameItem
    {
        private int roomId = -1;
        public Jerrycan(ItemDef def, PersonInfo person)
            : base(def, person)
        {
        }

        public override void Attack(MonsterInfo monster)
        {
            roomId = monster.mover.CurrentRoomId;
            GameField.Instance.OnMonsterMove += OnMonsterMove;
            GameField.Instance.OnPlayerMove += OnPlayerMove;
        }

        void OnMonsterMove(MonsterInfo monster, bool force)
        {
            if (monster.mover.CurrentRoomId == roomId && monster.def.vulnerabilities.Contains(DamageType.Fire))
            {
                monster.RemoveHits(10);
            }
        }

        void OnPlayerMove(PersonInfo player, bool force)
        {
            player.RemoveHits(2);
        }

        public override void Dispose()
        {
            base.Dispose();
            if (roomId >= 0)
            {
                GameField.Instance.OnMonsterMove -= OnMonsterMove;
                GameField.Instance.OnPlayerMove -= OnPlayerMove;
            }
        }
    }
}
