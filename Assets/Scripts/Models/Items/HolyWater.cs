﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class HolyWater : GameItem
    {
        public HolyWater(ItemDef def, PersonInfo person)
            : base(def, person)
        {
            GameField.Instance.OnHourComplete += OnHourTick;
        }

        void OnHourTick()
        {
            GameField.Instance.OnHourComplete -= OnHourTick;
            if (owner.health < owner.def.maxHealth)
            {
                owner.health++;
                GameField.Instance.PlayerRefresh(owner);
            }
        }
    }
}
