﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class Crucifix : GameItem
    {
        public Crucifix(ItemDef def, PersonInfo person)
            : base(def, person)
        {
            MonsterInfo.OnAttackTarget += OnAttackTarget;
        }

        void OnAttackTarget(PersonInfo person, MonsterInfo monster)
        {
            if (person == owner && monster.def.vulnerabilities.Contains(DamageType.Sanctification) 
                && person.health < person.def.maxHealth)
            {
                person.health++;
            }
        }
    }
}
