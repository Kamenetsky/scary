﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class Shotgun : GameItem
    {
        public Shotgun(ItemDef def, PersonInfo person)
            : base(def, person)
        {
        }

        public override void Attack(MonsterInfo monster)
        {
            base.Attack(monster);
            foreach (var it in GameField.Instance.GetMonstersInRoom(owner.mover.CurrentRoomId))
            {
                if (it.def.vulnerabilities.Contains(def.damageType) && it != monster)
                    it.RemoveHits(def.damage);
            }
        }
    }
}
