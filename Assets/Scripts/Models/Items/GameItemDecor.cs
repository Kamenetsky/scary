﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class GameItemDecor : GameItemUsable
    {
        public GameItemDecor(ItemDef def, PersonInfo person)
            : base(def, person)
        {
        }

        public override void Use()
        {
            if (canUseAdditionalProperty && additionalProperty != null)
                additionalProperty.Activate(null);
            else
                ActivateAdditionalProp();
        }
    }
}
