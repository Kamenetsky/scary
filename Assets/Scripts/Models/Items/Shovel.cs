﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class Shovel : GameItem
    {
        public Shovel(ItemDef def, PersonInfo person)
            : base(def, person)
        {
        }
        
        public override bool AllowToDig()
        {
            return true;
        }
    }
}

