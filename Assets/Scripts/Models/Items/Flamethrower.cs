﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class Flamethrower : GameItem
    {
        public Flamethrower(ItemDef def, PersonInfo person)
            : base(def, person)
        {
        }

        public override void Attack(MonsterInfo monster)
        {
            base.Attack(monster);

            if (monster.def.vulnerabilities.Contains(DamageType.Physics))
                monster.RemoveHits(def.damage);
        }

        public override int GetSlotsCount()
        {
            return 2;
        }
    }
}
