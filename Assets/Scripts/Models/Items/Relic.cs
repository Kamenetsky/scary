﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class Relic : GameItem
    {
        public Relic(ItemDef def, PersonInfo person)
            : base(def, person)
        {
        }
    }
}
