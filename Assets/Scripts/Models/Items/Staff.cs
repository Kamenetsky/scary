﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class Staff : GameItem
    {
        public Staff(ItemDef def, PersonInfo person)
            : base(def, person)
        {
        }

        public override void Attack(MonsterInfo monster)
        {
            if (owner.currentMovesCount >= 5 && monster.def.vulnerabilities.Contains(DamageType.Physics))
            {
                def.damage = 2;
            }
            else
            {
                def.damage = 1;
            }
            base.Attack(monster);
        }
    }
}
