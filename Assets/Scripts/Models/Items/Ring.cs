﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class Ring : GameItemUsable
    {
        public Ring(ItemDef def, PersonInfo person)
            : base(def, person)
        {
        }

        public override void Use()
        {
            GameField.Instance.MustSelectMonster();
            GameField.Instance.OnSelectedMonster += OnSelectedMonster;
        }

        void OnSelectedMonster(MonsterInfo monster)
        {
            GameField.Instance.OnSelectedMonster -= OnSelectedMonster;
            if (monster.def.monsterType != MonsterType.Hellish)
            {
                monster.Die();
            }
            Drop();
        }
    }
}
