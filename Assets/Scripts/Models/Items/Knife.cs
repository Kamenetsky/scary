﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class Knife : GameItemUsable
    {
        public Knife(ItemDef def, PersonInfo person)
            : base(def, person)
        {
        }

        public override void Use()
        {
            owner.AddItem(ItemFactory.CreateItem("AspenStake", owner));
            owner.movesCount = 0;
            GameField.Instance.SelectNextPlayer();
        }
    }
}
