﻿using Scary.Models.Effects;
using UnityEngine;
using System.Collections;

namespace Scary.Models.Items
{
    public class GasBurner : GameItem
    {
        public GasBurner(ItemDef def, PersonInfo person)
            : base(def, person)
        {
        }

        public override void Attack(MonsterInfo monster)
        {
            base.Attack(monster);
            Effect effect = new EffectDOT() {damage = 1, turnsCount = 1};
            monster.AddEffect(effect);
        }
    }
}
