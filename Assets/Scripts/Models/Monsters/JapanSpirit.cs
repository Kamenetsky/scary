﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    public class JapanSpirit : MonsterInfo
    {
        public JapanSpirit(MonsterDef def)
            : base(def)
        {
        }

        public override void AttackTarget(bool comleteTurn = true)
        {
            base.AttackTarget(comleteTurn);
            Die();
        }
    }
}
