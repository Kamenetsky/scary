﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    public class StepMother : MonsterInfo
    {
        public StepMother(MonsterDef def)
            : base(def)
        {
            foreach (var it in GameField.Instance.Monsters)
            {
                it.damageBonus += 1;
                it.healthBonus += 1;
                it.movesCountBonus += 1;
                it.damage += 1;
                it.health += 1;
                it.movesCount += 1;
                GameField.Instance.MonsterRefresh(it);
            }

            foreach (var it in GameField.Instance.Players)
            {
                if (it.GetItemsCount() > 0)
                {
                    it.GetItem(0).Drop();
                    GameField.Instance.PlayerRefresh(it);
                }
            }
        }

        public override void Die()
        {
            base.Die();
            foreach (var it in GameField.Instance.Monsters)
            {
                if (it.damageBonus > 0)
                    it.damageBonus -= 1;
                if (it.healthBonus > 0)
                    it.healthBonus -= 1;
                if (it.movesCountBonus > 0)
                    it.movesCountBonus -= 1;
                it.damage -= 1;
                it.health -= 1;
                it.movesCount -= 1;
                GameField.Instance.MonsterRefresh(it);
            }
        }

        public override bool IsStationary()
        {
            return true;
        }
    }
}
