﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    public class BloodyClown : MonsterInfo
    {
        public BloodyClown(MonsterDef def)
            : base(def)
        {
        }

        public override void AttackTarget(bool comleteTurn = true)
        {
            base.AttackTarget(comleteTurn);
            target.MoveTo(Random.Range(0, GameField.Instance.rooms.Count), 0);
        }
    }
}
