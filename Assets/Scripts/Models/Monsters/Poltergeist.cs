﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    public class Poltergeist : MonsterInfo
    {
        public Poltergeist(MonsterDef def)
            : base(def)
        {
        }

        public override void MoveTo(int roomId, int moveCost = 1, bool force = false)
        {
            base.MoveTo(roomId, moveCost);
            if (GameField.Instance.GetPlayersInRoom(roomId).Count > 0)
                GameField.Instance.TurnLight(roomId, false, null);
        }

        public override bool IsGhost()
        {
            return true;
        }
    }
}
