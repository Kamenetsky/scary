﻿using Scary.Models.Items;
using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    //TODO need to implement
    public class FireDemon : MonsterInfo
    {
        private bool hasShield = true;

        public FireDemon(MonsterDef def)
            : base(def)
        {
        }

        public override void RemoveHits(int amount, GameItem item)
        {
            if (hasShield && item != null && item.def.damageType == DamageType.Physics)
            {
                hasShield = false;
                if (amount > 0)
                    amount--;
            }
            base.RemoveHits(amount, item);
        }
    }
}
