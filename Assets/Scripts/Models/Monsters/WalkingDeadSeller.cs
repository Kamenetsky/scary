﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    public class WalkingDeadSeller : MonsterInfo
    {
        public WalkingDeadSeller(MonsterDef def)
            : base(def)
        {
            foreach (var it in GameField.Instance.Monsters)
            {
                if (it.IsPlush())
                {
                    it.damage++;
                    it.movesCountBonus++;
                }
            }
        }
    }
}
