﻿using UnityEngine;
using System.Collections;

using Scary.Models.Items;

namespace Scary.Models.Monsters
{
    public class ScaryBrat : MonsterInfo
    {
        public ScaryBrat(MonsterDef def)
            : base(def)
        {
            GameItem.OnAttack += OnAttack;
        }

        void OnAttack(GameItem item, MonsterInfo monster)
        {
            if (monster == this && item.def.damage >= health && item.GetOwner().health < item.GetOwner().def.maxHealth)
                item.GetOwner().health++;
        }

        public override void Die()
        {
            GameItem.OnAttack -= OnAttack;
            base.Die();
        }
    }
}
