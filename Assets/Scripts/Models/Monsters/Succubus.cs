﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    public class Succubus : MonsterInfo
    {
        public Succubus(MonsterDef def)
            : base(def)
        {
        }
    }
}
