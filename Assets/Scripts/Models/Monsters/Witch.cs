﻿using UnityEngine;
using System.Collections;

using Scary.Models.Items;

namespace Scary.Models.Monsters
{
    public class Witch : MonsterInfo
    {
        public Witch(MonsterDef def)
            : base(def)
        {
            GameItem.OnAttack += OnAttack;
        }

        void OnAttack(GameItem item, MonsterInfo monster)
        {
            if (monster == this && item.def.damageType == DamageType.Fire)
            {
                foreach (var pi in GameField.Instance.Players)
                {
                    if (pi.health < pi.def.maxHealth)
                    {
                        pi.RemoveHits(-1);
                    }
                }
            }
        }

        public override void Die()
        {
            GameItem.OnAttack -= OnAttack;
            base.Die();
        }
    }
}
