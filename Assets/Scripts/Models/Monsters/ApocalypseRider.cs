﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    public class ApocalypseRider : MonsterInfo
    {
        public ApocalypseRider(MonsterDef def)
            : base(def)
        {
            foreach (var it in GameField.Instance.Monsters)
            {
                it.def.maxMovesCount = 0;
            }
        }

        void OnMonsterDestroy(MonsterInfo monster)
        {
            GameField.Instance.SpawnMonsterInRoom("", monster.mover.CurrentRoomId);
        }

        void OnPlayerMove(PersonInfo player, bool force)
        {
            if (GameField.Instance.GetMonstersInRoom(player.mover.CurrentRoomId).Count > 0)
            {
                player.movesCount = 0;
                GameField.Instance.PlayerRefresh(player);
            }
        }

        public override void Die()
        {
            base.Die();
            GameField.Instance.OnMonsterDestroy -= OnMonsterDestroy;
            GameField.Instance.OnPlayerMove -= OnPlayerMove;
        }
    }
}
