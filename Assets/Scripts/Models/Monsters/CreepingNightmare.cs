﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    public class CreepingNightmare : MonsterInfo
    {
        public CreepingNightmare(MonsterDef def)
            : base(def)
        {
        }
    }
}
