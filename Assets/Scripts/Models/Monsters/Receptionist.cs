﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    public class Receptionist : MonsterInfo
    {
        public Receptionist(MonsterDef def)
            : base(def)
        {
            GameField.Instance.OnHourComplete += OnHourComplete;
        }

        public override bool IsStationary()
        {
            return true;
        }

        void OnHourComplete()
        {
            health = 0;
            Die();
        }
    }
}
