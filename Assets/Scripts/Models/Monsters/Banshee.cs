﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    public class Banshee : MonsterInfo
    {
        public Banshee(MonsterDef def)
            : base(def)
        {
        }

        public override void AttackTarget(bool completeTurn = true)
        {
            foreach (var it in GameField.Instance.GetPlayersInRoom(mover.CurrentRoomId))
            {
                it.RemoveHits(damage);
            }
            base.AttackTarget(completeTurn);
        }

        public override bool IsGhost()
        {
            return true;
        }
    }
}
