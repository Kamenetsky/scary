﻿using System.Collections.Generic;
using Scary.Models.Items;
using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    public class DeadPlumber : MonsterInfo
    {
        public DeadPlumber(MonsterDef def)
            : base(def)
        {
        }

        public override void AttackTarget(bool completeTurn = true)
        {
            base.AttackTarget(completeTurn);
            List<GameItem> toRemove = new List<GameItem>();
            for (int i = 0; i < target.GetItemsCount(); ++i)
            {
                if (target.GetItem(i).def.damageType == DamageType.Fire)
                    toRemove.Add(target.GetItem(i));
            }
            target.RemoveItems(toRemove);
        }
    }
}
