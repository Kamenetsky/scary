﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    public class Ghost : MonsterInfo
    {
        public Ghost(MonsterDef def)
            : base(def)
        {
        }

        public override bool IsGhost()
        {
            return true;
        }
    }
}
