﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    public class Postman : MonsterInfo
    {
        public Postman(MonsterDef def)
            : base(def)
        {
        }

        public override int DamageOnMeeting()
        {
            return 1;
        }
    }
}
