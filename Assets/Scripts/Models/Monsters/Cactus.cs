﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    public class Cactus : MonsterInfo
    {
        public Cactus(MonsterDef def)
            : base(def)
        {
        }

        public override int DamageOnMeeting()
        {
            return 1;
        }

        public override bool IsStationary()
        {
            return true;
        }
    }
}
