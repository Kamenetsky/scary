﻿using System.Collections.Generic;
using Scary.Utils;

namespace Scary.Models.Monsters
{
    public class Coffin : MonsterInfo
    {
        public Coffin(MonsterDef def)
            : base(def)
        {
        }

        public override void AttackTarget(bool comleteTurn = true)
        {
            base.AttackTarget(comleteTurn);
            List<int> ids = GameField.searchChecker.FindReachable(mover.CurrentRoomId, 1, null, true);
            foreach (var it in ids)
            {
                foreach (var pi in GameField.Instance.Players)
                {
                    if (pi.mover.CurrentRoomId == it && pi != target)
                        pi.RemoveHits(1);
                }
            }
        }
    }
}
