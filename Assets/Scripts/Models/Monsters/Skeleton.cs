﻿using UnityEngine;
using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.Monsters
{
    public class Skeleton : MonsterInfo
    {
        public Skeleton(MonsterDef def)
            : base(def)
        {
        }

        public override void RemoveHits(int amount, GameItem item = null)
        {
            if (health <= 0 && item != null)
            {
                PersonInfo pi = item.GetOwner();
                for (int i = 0; i < pi.GetItemsCount(); ++i)
                {
                    pi.GetItem(i).KillAdditionalProp();
                }
            }
            base.RemoveHits(amount, item);
        }
    }
}
