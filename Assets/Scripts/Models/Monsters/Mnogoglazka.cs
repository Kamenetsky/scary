﻿using UnityEngine;
using System.Collections;

using Scary.Models.Items;

namespace Scary.Models.Monsters
{
    public class Mnogoglazka : MonsterInfo
    {
        public Mnogoglazka(MonsterDef def)
            : base(def)
        {
            GameItem.OnAttack += OnAttack;
        }

        void OnAttack(GameItem item, MonsterInfo monster)
        {
            if (monster == this && item.def.damage >= health)
                item.GetOwner().RemoveHits(1);
        }

        public override void Die()
        {
            GameItem.OnAttack -= OnAttack;
            base.Die();
        }
    }
}
