﻿using UnityEngine;
using System.Collections;

using Scary.Utils;
using Scary.Models.Items;

namespace Scary.Models.Monsters
{
    public class DeadGardener : MonsterInfo
    {
        private bool firstTurn = true;
        public DeadGardener(MonsterDef def)
            : base(def)
        {
        }

        public override void MakeDecision()
        {
            if (!firstTurn && def.vulnerabilities.Contains(DamageType.Fire))
                def.vulnerabilities.Remove(DamageType.Fire);
            firstTurn = false;
            base.MakeDecision();
        }
    }
}
