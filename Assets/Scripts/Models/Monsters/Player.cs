﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    //TODO need to implement
    public class Player : MonsterInfo
    {
        public Player(MonsterDef def)
            : base(def)
        {
        }

        public override bool IsGhost()
        {
            return true;
        }
    }
}
