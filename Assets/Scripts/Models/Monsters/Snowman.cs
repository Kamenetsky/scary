﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    public class Snowman : MonsterInfo
    {
        public Snowman(MonsterDef def)
            : base(def)
        {
            GameField.Instance.OnHourComplete += OnHourComplete;
        }
        
        public override bool IsStationary()
        {
            return true;
        }

        void OnHourComplete()
        {
            GameField.Instance.OnHourComplete -= OnHourComplete;
            health = 0;
            Die();
        }
    }
}
