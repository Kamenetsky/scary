﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    public class BlackBall : MonsterInfo
    {
        public BlackBall(MonsterDef def)
            : base(def)
        {
        }

        public override void Reset()
        {
            base.Reset();
            movesCount = index;
        }
    }
}
