﻿using UnityEngine;
using System.Collections;

using Scary.Models.Items;

namespace Scary.Models.Monsters
{
    public class TeddyFreak : MonsterInfo
    {
        public TeddyFreak(MonsterDef def)
            : base(def)
        {
            GameItem.OnAttack += OnAttack;
        }

        void OnAttack(GameItem item, MonsterInfo monster)
        {
            if (monster == this && item.def.damage >= health)
                item.SetPlush();
        }

        public override void Die()
        {
            GameItem.OnAttack -= OnAttack;
            base.Die();
        }

        public override bool IsPlush()
        {
            return true;
        }
    }
}
