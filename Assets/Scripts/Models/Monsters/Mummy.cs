﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    public class Mummy : MonsterInfo
    {
        public Mummy(MonsterDef def)
            : base(def)
        {
        }
    }
}
