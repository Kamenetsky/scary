﻿using System.Collections.Generic;
using Scary.Models.Items;
using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    public class Demones : MonsterInfo
    {
        public Demones(MonsterDef def)
            : base(def)
        {
        }

        public override void Die()
        {
            foreach (var it in GameField.Instance.GetPlayersInRoom(mover.CurrentRoomId))
            {
                List<GameItem> items = new List<GameItem>();
                for (int i = 0; i < it.GetItemsCount(); ++i)
                {
                    items.Add(it.GetItem(i));
                }
                it.RemoveItems(items);
            }
            
            base.Die();
        }
    }
}
