﻿using UnityEngine;
using System.Collections;

using Scary.Models.Effects;

namespace Scary.Models.Monsters
{
    public class Nurse : MonsterInfo
    {
        public Nurse(MonsterDef def)
            : base(def)
        {
        }

        public override void AttackTarget(bool comleteTurn = true)
        {
            base.AttackTarget(comleteTurn);
            target.AddEffect(new EffectCalm());
        }
    }
}
