﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    public class SilentHorror : MonsterInfo
    {
        private bool summoned = false;
        public SilentHorror(MonsterDef def)
            : base(def)
        {
        }

        public override void MakeDecision()
        {
            base.MakeDecision();
            if (!summoned)
            {
                summoned = true;
                Summon();
            }
        }

        void Summon()
        {
            foreach (var it in GameField.Instance.Players)
            {
                if (GameField.Instance.Monsters.Count >= Constants.maxMonstersCount)
                    break;

                if (!it.IsDead() && GameField.Instance.Monsters.Count < GameField.Instance.Players.Count + Constants.monstersPlayersCountDiff)
                    GameField.Instance.SpawnMonsterInRoom(string.Empty, it.mover.CurrentRoomId);
            }
        }

        public override void Die()
        {
            base.Die();
            foreach (var it in GameField.Instance.Players)
            {
                if (!it.IsDead())
                {
                    if (GameField.Instance.Monsters.Count > 0)
                        GameField.Instance.Monsters[0].Die();
                }                   
            }
        }
    }
}
