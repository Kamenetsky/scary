﻿using UnityEngine;
using System.Collections;

using Scary.Models.Effects;

namespace Scary.Models.Monsters
{
    public class Spider : MonsterInfo
    {
        public Spider(MonsterDef def)
            : base(def)
        {
        }

        public override void AttackTarget(bool comleteTurn = true)
        {
            base.AttackTarget(comleteTurn);
            target.AddEffect(new EffectDOT());
        }
    }
}
