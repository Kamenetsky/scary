﻿using System.Collections.Generic;
using Scary.Utils;

namespace Scary.Models.Monsters
{
    public class DarkSpawn : MonsterInfo
    {
        public DarkSpawn(MonsterDef def)
            : base(def)
        {
            foreach (var it in GameField.Instance.rooms)
            {
                GameField.Instance.TurnLight(it.id, false, null);
            }
        }

        public override void MakeDecision()
        {
            base.MakeDecision();
            GameField.Instance.TurnLight(mover.CurrentRoomId, false, null);
        }

        public override void Die()
        {
            base.Die();
            foreach (var it in GameField.Instance.rooms)
            {
                GameField.Instance.TurnLight(it.id, true, null);
            }
        }
    }
}
