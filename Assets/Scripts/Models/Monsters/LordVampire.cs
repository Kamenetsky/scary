﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    public class LordVampire : MonsterInfo
    {
        public LordVampire(MonsterDef def)
            : base(def)
        {
            PersonInfo.OnTurnComplete += OnPlayerTurnComplete;
        }

        public override bool IsVampire()
        {
            return true;
        }

        public override void  Die()
        {
            PersonInfo.OnTurnComplete -= OnPlayerTurnComplete;
            base.Die();
        }

        void OnPlayerTurnComplete(CreatureInfo<PersonDef> player)
        {
            health = def.maxHealth;
        }
    }
}
