﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    public class Chucha : MonsterInfo
    {
        private bool spawnHand = false;
        
        public Chucha(MonsterDef def)
            : base(def)
        {
        }

        public override void MoveTo(int roomId, int moveCost = 1, bool force = false)
        {
            base.MoveTo(roomId, moveCost);

            if (target.mover.CurrentRoomId != mover.CurrentRoomId && movesCount <= 0
                && GameField.Instance.Monsters.Count < Constants.maxMonstersCount && !spawnHand)
            {
                spawnHand = true;
                GameField.Instance.SpawnConcreteMonster("ChuchasHand");
            }
        }

        public override bool IsPlush()
        {
            return true;
        }
    }
}
