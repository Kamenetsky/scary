﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Monsters
{
    public class Vampire : MonsterInfo
    {
        public Vampire(MonsterDef def)
            : base(def)
        {
        }

        public override void AttackTarget(bool comleteTurn = true)
        {
            base.AttackTarget(comleteTurn);
            AddHits(1);
        }

        public override bool IsVampire()
        {
            return true;
        }
    }
}
