﻿using UnityEngine;

using System.Collections.Generic;

using JsonFx.Json;

using Scary.Models.Items;
using Scary.Models.ItemProps;
using Scary.Models.MonsterProps;


namespace Scary.Models
{
    public class Defs
    {
        public List<PersonDef> characters = new List<PersonDef>();
        public List<RoomDef> rooms = new List<RoomDef>();
        public List<ItemDef> items = new List<ItemDef>();
        public List<ItemPropertyDef> itemProps = new List<ItemPropertyDef>(); 
        public List<MonsterDef> monsters = new List<MonsterDef>();
        public List<MonsterPropertyDef> monsterProps = new List<MonsterPropertyDef>();

        public int GetTotalItemDefsCount()
        {
            int result = 0;
            foreach (var it in items)
                result += it.amountInDeck;
            return result;
        }

        public int GetItemDefsCountByName(string name)
        {
            int result = 0;
            foreach (var it in items)
                if (it.name == name)
                    result += it.amountInDeck;
            return result;
        }

        public ItemDef GetItemDef(string name)
        {
            return items.Find(x => x.name == name);
        }

        public static void GenerateGameDefs()
        {
            Defs defs = new Defs();

            //rooms
            defs.rooms.Add(new RoomDef() { id = 0, type = RoomType.Hall });
            defs.rooms.Add(new RoomDef() { id = 1, type = RoomType.Cellar });
            defs.rooms.Add(new RoomDef() { id = 2, type = RoomType.Default });
            defs.rooms.Add(new RoomDef() { id = 3, type = RoomType.Default });
            defs.rooms.Add(new RoomDef() { id = 4, type = RoomType.Default });
            defs.rooms.Add(new RoomDef() { id = 5, type = RoomType.Corridor });
            defs.rooms.Add(new RoomDef() { id = 6, type = RoomType.Default });
            defs.rooms.Add(new RoomDef() { id = 7, type = RoomType.Corridor });
            defs.rooms.Add(new RoomDef() { id = 8, type = RoomType.Storage });
            defs.rooms.Add(new RoomDef() { id = 9, type = RoomType.Library });
            defs.rooms.Add(new RoomDef() { id = 10, type = RoomType.Corridor });
            defs.rooms.Add(new RoomDef() { id = 11, type = RoomType.Corridor });
            defs.rooms.Add(new RoomDef() { id = 12, type = RoomType.Default });
            defs.rooms.Add(new RoomDef() { id = 13, type = RoomType.Default });
            defs.rooms.Add(new RoomDef() { id = 14, type = RoomType.Default });
            defs.rooms.Add(new RoomDef() { id = 15, type = RoomType.Storeroom });
            defs.rooms.Add(new RoomDef() { id = 16, type = RoomType.Bedroom });
            defs.rooms.Add(new RoomDef() { id = 17, type = RoomType.Default });
            defs.rooms.Add(new RoomDef() { id = 18, type = RoomType.Playroom });
            defs.rooms.Add(new RoomDef() { id = 19, type = RoomType.Hall });
            defs.rooms.Add(new RoomDef() { id = 20, type = RoomType.Kitchen });

            //characters
            defs.characters.Add(new PersonDef() { type = PersonType.Military, maxHealth = 6, initiative = 1 });
            defs.characters.Add(new PersonDef() { type = PersonType.Alchemist, maxHealth = 6, initiative = 2 });
            defs.characters.Add(new PersonDef() { type = PersonType.Scientist, maxHealth = 6, initiative = 3 });
            defs.characters.Add(new PersonDef() { type = PersonType.Historian, maxHealth = 6, initiative = 4 });
            defs.characters.Add(new PersonDef() { type = PersonType.Doctor, maxHealth = 6, initiative = 5 });
            defs.characters.Add(new PersonDef() { type = PersonType.Priest, maxHealth = 6, initiative = 6 });
            defs.characters.Add(new PersonDef() { type = PersonType.Exorcist, maxHealth = 6, initiative = 7 });

            //items
            defs.items.Add(new ItemDef() { name = "Shovel", amountInDeck = 4, itemType = ItemType.Weapon, damageType = DamageType.Physics });
            defs.items.Add(new ItemDef() { name = "AspenStake", amountInDeck = 4, itemType = ItemType.Weapon, damageType = DamageType.Physics });
            defs.items.Add(new ItemDef() { name = "Crucifix", amountInDeck = 2, itemType = ItemType.Weapon, damageType = DamageType.Sanctification });
            defs.items.Add(new ItemDef() { name = "Shotgun", amountInDeck = 3, itemType = ItemType.Weapon, damageType = DamageType.Physics });
            defs.items.Add(new ItemDef() { name = "Revolver", amountInDeck = 2, itemType = ItemType.Weapon, damageType = DamageType.Physics });
            defs.items.Add(new ItemDef() { name = "Hammer", amountInDeck = 2, itemType = ItemType.Weapon, damageType = DamageType.Physics });
            defs.items.Add(new ItemDef() { name = "GasBurner", amountInDeck = 2, itemType = ItemType.Weapon, damageType = DamageType.Fire });
            defs.items.Add(new ItemDef() { name = "Knife", amountInDeck = 3, itemType = ItemType.Weapon, damageType = DamageType.Physics });
            defs.items.Add(new ItemDef() { name = "Crossbow", amountInDeck = 1, itemType = ItemType.Weapon, damageType = DamageType.Physics });
            defs.items.Add(new ItemDef() { name = "Boomerang", amountInDeck = 2, itemType = ItemType.Weapon, damageType = DamageType.Physics });
            defs.items.Add(new ItemDef() { name = "Tourch", amountInDeck = 3, itemType = ItemType.Weapon, damageType = DamageType.Physics });
            defs.items.Add(new ItemDef() { name = "Sword", amountInDeck = 2, itemType = ItemType.Weapon, damageType = DamageType.Physics });
            defs.items.Add(new ItemDef() { name = "Jerrycan", amountInDeck = 1, itemType = ItemType.Weapon, damageType = DamageType.Physics });
            defs.items.Add(new ItemDef() { name = "Flamethrower", amountInDeck = 2, itemType = ItemType.Weapon, damageType = DamageType.Fire });
            defs.items.Add(new ItemDef() { name = "HolyWater", amountInDeck = 2, itemType = ItemType.Weapon, damageType = DamageType.Sanctification });
            defs.items.Add(new ItemDef() { name = "Shield", amountInDeck = 2, itemType = ItemType.Weapon, damageType = DamageType.Physics });
            defs.items.Add(new ItemDef() { name = "Staff", amountInDeck = 1, itemType = ItemType.Weapon, damageType = DamageType.Physics });
            defs.items.Add(new ItemDef() { name = "Relic", amountInDeck = 1, itemType = ItemType.Weapon, damageType = DamageType.Sanctification });
            defs.items.Add(new ItemDef() { name = "Bible", amountInDeck = 2, itemType = ItemType.Weapon, damageType = DamageType.Sanctification });
            //defs.items.Add(new ItemDef() { name = "Lamp", amountInDeck = 2, damageType = DamageType.Fire });
            defs.items.Add(new ItemDef() { name = "Reed", amountInDeck = 2, itemType = ItemType.Default, damageType = DamageType.None });
            defs.items.Add(new ItemDef() { name = "SmokingPipe", amountInDeck = 2, itemType = ItemType.Default, damageType = DamageType.None });
            defs.items.Add(new ItemDef() { name = "Maracas", amountInDeck = 2, itemType = ItemType.Default, damageType = DamageType.None });
            defs.items.Add(new ItemDef() { name = "Toy", amountInDeck = 1, itemType = ItemType.Relic, damageType = DamageType.None, monsterName = "temp" });
            defs.items.Add(new ItemDef() { name = "DiamondsAce", amountInDeck = 1, itemType = ItemType.Relic, damageType = DamageType.None, monsterName = "temp" });
            defs.items.Add(new ItemDef() { name = "Bone", amountInDeck = 1, itemType = ItemType.Relic, damageType = DamageType.None, monsterName = "temp" });
            //defs.items.Add(new ItemDef() { name = "Will", amountInDeck = 1, damageType = DamageType.None, monsterName = "temp" });
            defs.items.Add(new ItemDef() { name = "Ring", amountInDeck = 1, damageType = DamageType.None, monsterName = "temp" });
            //defs.items.Add(new ItemDef() { name = "Contract", amountInDeck = 1, damageType = DamageType.None, monsterName = "temp" });

            //item props
            defs.itemProps.Add(new ItemPropertyDef() { name = "ThisIsShovel", amountInDeck = 2, propertyType = ItemPropertyType.Passive });
            defs.itemProps.Add(new ItemPropertyDef() { name = "BloodMagic", amountInDeck = 2, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "Vampirism", amountInDeck = 2, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "Plush", amountInDeck = 2, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "Evolution", amountInDeck = 1, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "Hellish", amountInDeck = 1, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "HammerSinner", amountInDeck = 2, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "Puppeteer", amountInDeck = 2, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "Stars", amountInDeck = 1, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "Creator", amountInDeck = 2, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "Call", amountInDeck = 1, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "Darkness", amountInDeck = 2, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "Gates", amountInDeck = 2, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "LostRelic", amountInDeck = 1, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "SpiritMadman", amountInDeck = 1, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "Artifact", amountInDeck = 2, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "Treat", amountInDeck = 1, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "BestWeapon", amountInDeck = 1, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "Fire", amountInDeck = 1, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "Science", amountInDeck = 1, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "Chain", amountInDeck = 1, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "Light", amountInDeck = 1, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "DarkFriend", amountInDeck = 2, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "HellWill", amountInDeck = 1, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "Damn", amountInDeck = 1, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "Storm", amountInDeck = 1, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "InvisibleThread", amountInDeck = 2, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "InfernalShield", amountInDeck = 1, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "Deal", amountInDeck = 1, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "Playing", amountInDeck = 2, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "Anomaly", amountInDeck = 1, propertyType = ItemPropertyType.Active });
            defs.itemProps.Add(new ItemPropertyDef() { name = "Memory", amountInDeck = 1, propertyType = ItemPropertyType.Active });

            //monsters
            defs.monsters.Add(new MonsterDef()
            {
                name = "Snowman",
                monsterType = MonsterType.Default,
                maxHealth = 1,    
                damage = 1,
                maxMovesCount = 0,
                amountInDeck = 1,        
                vulnerabilities = new List<DamageType>() { DamageType.Fire }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "Vampire",
                monsterType = MonsterType.Default,
                maxHealth = 3,
                damage = 2,
                maxMovesCount = 2,
                amountInDeck = 3,
                vulnerabilities = new List<DamageType>() { DamageType.Fire }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "Receptionist",
                monsterType = MonsterType.Default,
                maxHealth = 1,
                damage = 1,
                maxMovesCount = 1,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Physics }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "Chucha",
                monsterType = MonsterType.Default,
                maxHealth = 2,
                damage = 1,
                maxMovesCount = 2,
                amountInDeck = 2,
                vulnerabilities = new List<DamageType>() { DamageType.Fire }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "ChuchasHand",
                monsterType = MonsterType.Default,
                maxHealth = 2,
                damage = 1,
                maxMovesCount = 2,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Fire }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "TeddyFreak",
                monsterType = MonsterType.Default,
                maxHealth = 2,
                damage = 2,
                maxMovesCount = 2,
                amountInDeck = 2,
                vulnerabilities = new List<DamageType>() { DamageType.Fire }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "Goblin",
                monsterType = MonsterType.Default,
                maxHealth = 4,
                damage = 1,
                maxMovesCount = 5,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Fire }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "CreepingNightmare",
                monsterType = MonsterType.Default,
                maxHealth = 2,
                damage = 3,
                maxMovesCount = 2,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Fire, DamageType.Physics }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "LordVampire",
                monsterType = MonsterType.Default,
                maxHealth = 4,
                damage = 2,
                maxMovesCount = 3,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Fire }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "DeadGardener",
                monsterType = MonsterType.Default,
                maxHealth = 3,
                damage = 2,
                maxMovesCount = 2,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Fire, DamageType.Physics }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "Poltergeist",
                monsterType = MonsterType.Default,
                maxHealth = 2,
                damage = 1,
                maxMovesCount = 2,
                amountInDeck = 2,
                vulnerabilities = new List<DamageType>() { DamageType.Sanctification }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "Mnogoglazka",
                monsterType = MonsterType.Default,
                maxHealth = 3,
                damage = 1,
                maxMovesCount = 1,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Physics }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "JapanSpirit",
                monsterType = MonsterType.Default,
                maxHealth = 1,
                damage = 3,
                maxMovesCount = 2,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Sanctification }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "Nurse",
                monsterType = MonsterType.Default,
                maxHealth = 3,
                damage = 2,
                maxMovesCount = 3,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Fire, DamageType.Physics }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "WalkingDeadSeller",
                monsterType = MonsterType.Default,
                maxHealth = 2,
                damage = 2,
                maxMovesCount = 2,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Fire }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "DeadPlumber",
                monsterType = MonsterType.Default,
                maxHealth = 3,
                damage = 2,
                maxMovesCount = 3,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Fire }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "Postman",
                monsterType = MonsterType.Default,
                maxHealth = 3,
                damage = 2,
                maxMovesCount = 2,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Fire }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "Player",
                monsterType = MonsterType.Default,
                maxHealth = 3,
                damage = 2,
                maxMovesCount = 2,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Fire, DamageType.Physics }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "Banshee",
                monsterType = MonsterType.Default,
                maxHealth = 3,
                damage = 2,
                maxMovesCount = 2,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Fire, DamageType.Sanctification }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "Spider",
                monsterType = MonsterType.Default,
                maxHealth = 3,
                damage = 2,
                maxMovesCount = 2,
                amountInDeck = 2,
                vulnerabilities = new List<DamageType>() { DamageType.Fire, DamageType.Physics }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "Witch",
                monsterType = MonsterType.Default,
                maxHealth = 3,
                damage = 2,
                maxMovesCount = 4,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Fire, DamageType.Physics }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "Mummy",
                monsterType = MonsterType.Default,
                maxHealth = 5,
                damage = 2,
                maxMovesCount = -1,
                amountInDeck = 2,
                vulnerabilities = new List<DamageType>() { DamageType.Fire }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "Coffin",
                monsterType = MonsterType.Default,
                maxHealth = 4,
                damage = 2,
                maxMovesCount = 4,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Fire, DamageType.Physics }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "BlackBall",
                monsterType = MonsterType.Default,
                maxHealth = 3,
                damage = 2,
                maxMovesCount = 3,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Physics }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "Cactus",
                monsterType = MonsterType.Default,
                maxHealth = 2,
                damage = 1,
                maxMovesCount = 0,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Fire }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "ScaryBrat",
                monsterType = MonsterType.Default,
                maxHealth = 3,
                damage = 3,
                maxMovesCount = 3,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Physics }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "Succubus",
                monsterType = MonsterType.Default,
                maxHealth = 2,
                damage = 1,
                maxMovesCount = 4,
                amountInDeck = 2,
                vulnerabilities = new List<DamageType>() { DamageType.Physics, DamageType.Sanctification }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "LaughingDevil",
                monsterType = MonsterType.Default,
                maxHealth = 1,
                damage = 1,
                maxMovesCount = 1,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Physics, DamageType.Sanctification }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "Ghost",
                monsterType = MonsterType.Default,
                maxHealth = 2,
                damage = 2,
                maxMovesCount = 3,
                amountInDeck = 3,
                vulnerabilities = new List<DamageType>() { DamageType.Sanctification }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "FireDemon",
                monsterType = MonsterType.Default,
                maxHealth = 3,
                damage = 3,
                maxMovesCount = 3,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Physics, DamageType.Sanctification }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "Demones",
                monsterType = MonsterType.Default,
                maxHealth = 3,
                damage = 2,
                maxMovesCount = 3,
                amountInDeck = 2,
                vulnerabilities = new List<DamageType>() { DamageType.Physics, DamageType.Sanctification }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "IceGuardian",
                monsterType = MonsterType.Default,
                maxHealth = 4,
                damage = 2,
                maxMovesCount = 4,
                amountInDeck = 2,
                vulnerabilities = new List<DamageType>() { DamageType.Fire, DamageType.Physics }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "Skeleton",
                monsterType = MonsterType.Default,
                maxHealth = 2,
                damage = 1,
                maxMovesCount = 3,
                amountInDeck = 2,
                vulnerabilities = new List<DamageType>() { DamageType.Physics }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "BloodyClown",
                monsterType = MonsterType.Default,
                maxHealth = 2,
                damage = 2,
                maxMovesCount = 3,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Sanctification }
            });
            //Hellish
            defs.monsters.Add(new MonsterDef()
            {
                name = "SilentHorror",
                monsterType = MonsterType.Hellish,
                maxHealth = 4,
                damage = 3,
                maxMovesCount = 5,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Sanctification }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "DarkSpawn",
                monsterType = MonsterType.Hellish,
                maxHealth = 4,
                damage = 2,
                maxMovesCount = 2,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Fire }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "StepMother",
                monsterType = MonsterType.Hellish,
                maxHealth = 5,
                damage = 3,
                maxMovesCount = 0,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Physics }
            });
            defs.monsters.Add(new MonsterDef()
            {
                name = "ApocalypseRider",
                monsterType = MonsterType.Hellish,
                maxHealth = 6,
                damage = 3,
                maxMovesCount = 4,
                amountInDeck = 1,
                vulnerabilities = new List<DamageType>() { DamageType.Physics }
            });

            //Monster props
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Energy", amountInDeck = 2 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Fury", amountInDeck = 1 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Statue", amountInDeck = 2 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Spirit", amountInDeck = 1 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Nightmare", amountInDeck = 2 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Lies", amountInDeck = 2 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Demonic", amountInDeck = 2 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Incorporeal", amountInDeck = 2 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "DamnChain", amountInDeck = 1 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "HellBlessing", amountInDeck = 1 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Lord", amountInDeck = 2 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Keeper", amountInDeck = 2 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Blow", amountInDeck = 1 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Child", amountInDeck = 2 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Eater", amountInDeck = 1 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Horror", amountInDeck = 1 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Judge", amountInDeck = 2 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Gatekeeper", amountInDeck = 1 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Guardian", amountInDeck = 1 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Devastate", amountInDeck = 2 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Impetuous", amountInDeck = 1 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Lazy", amountInDeck = 1 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Hardy", amountInDeck = 2 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Disguise", amountInDeck = 2 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "DemonShield", amountInDeck = 2 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Sluggish", amountInDeck = 1 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "NotDemon", amountInDeck = 2 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Tantrum", amountInDeck = 2 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Poison", amountInDeck = 2 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Smell", amountInDeck = 2 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Riot", amountInDeck = 2 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Resident", amountInDeck = 2 });
            defs.monsterProps.Add(new MonsterPropertyDef() { name = "Traitor", amountInDeck = 2 });

            string result = JsonWriter.Serialize(defs);
            Debug.Log(result);
        }
    }
}
