﻿using System;
using System.Collections.Generic;

using Scary.Models.Items;
using Scary.Models.MonsterProps;
using Scary.Models.Effects;

namespace Scary.Models
{
    public enum MonsterType
    {
        Default,
        Legendary,
        Hellish
    }
    public class MonsterDef : CreatureDef
    {
        public MonsterDef() : base()
        {           
        }
        public MonsterDef(MonsterDef origin) : base(origin)
        {
            monsterType = origin.monsterType;
            damage = origin.damage;
            amountInDeck = origin.amountInDeck;
            initAmountInDeck = amountInDeck;
            vulnerabilities = new List<DamageType>();
            foreach (var it in origin.vulnerabilities)
            {
                vulnerabilities.Add(it);
            }
        }
        
        public MonsterType monsterType;
        public int damage;
        public int amountInDeck = 1;
        public List<DamageType> vulnerabilities;

        public int initAmountInDeck = 1;
    }

    public class MonsterInfo : CreatureInfo<MonsterDef>, IDisposable
    {
        public static event Action<PersonInfo, MonsterInfo> OnAttackTarget = delegate { };
        public static event Action<PersonInfo, MonsterInfo> OnStartAttackTarget = delegate { };
        public static event Action<int, MonsterInfo, GameItem> OnTakeDamageByItem = delegate { };
        public static event Action<MonsterInfo, GameItem> OnDieByItem = delegate { };
        public static event Action<MonsterInfo> OnActivateProperty = delegate { };
        
        public PersonInfo target = null;
        public bool findNearest = true;

        public int index;
        public int damage;
        public int healthBonus = 0;
        public int damageBonus = 0;

        private MonsterProperty additionalProperty = null;
        public string additionalPropertyName;
        public bool canUseAdditionalProperty = true;
        
        public MonsterInfo(MonsterDef def)
            : base(def)
        {
            damage = def.damage;
        }

        public override void Reset()
        {
            health = def.maxHealth + healthBonus;
            base.Reset();
            if (def.maxMovesCount < 0)
                movesCount = UnityEngine.Random.Range(1, 6);
            if (movesCountBonus > 0 || (movesCountBonus < 0 && movesCountBonus + movesCount > 0))
                movesCount += movesCountBonus;
            target = null;
        }

        public virtual void MakeDecision()
        {
            if (movesCount <= 0 && !IsStationary())
            {
                CompleteTurn();
                return;
            }

            if (findNearest)
            {
                FindNearestTarget();
            }
            else
            {
                FindWeakestTarget();
            }

            if (target.mover.CurrentRoomId == mover.CurrentRoomId)
            {
                UnityEngine.Debug.Log(string.Format("Monster {0} starts attack {1}", def.name, target.def.type));
                OnStartAttackTarget(target, this);
            }
            else
            {
                if (IsStationary())
                {
                    CompleteTurn();
                    return;
                }

                int nextId = mover.GetNextPathPoint();
                if (nextId >= 0)
                {
                    MoveTo(nextId);
                }
            }
        }

        public virtual void AttackTarget(bool completeTurn = true)
        {
            if (target == null)
            {
                UnityEngine.Debug.LogError("Target is not defined");
                return;
            }
            UnityEngine.Debug.Log(string.Format("Monster {0} attacks {1} succes", def.name, target.def.type));
            ActivateAdditionalProp();
            target.RemoveHits(damage);
            OnAttackTarget(target, this);
            if (completeTurn)
                CompleteTurn();
        }

        public virtual void FindNearestTarget()
        {
            PersonInfo aimChar = null;
            int minPath = 1000;
            foreach (var ch in GameField.Instance.Players)
            {
                if (ch.health <= 0)
                    continue;
                mover.CreatePathTo(ch.mover.CurrentRoomId, IsGhost());
                if (minPath > mover.GetPathLength())
                {
                    minPath = mover.GetPathLength();
                    aimChar = ch;
                }
            }
            target = aimChar;
            mover.ClearPath();
            mover.CreatePathTo(aimChar.mover.CurrentRoomId, IsGhost());
        }

        public virtual void FindWeakestTarget()
        {
            int max = 1000;
            int roomId = 0;
            foreach (var it in GameField.Instance.Players)
            {
                if (it.health > 0 && it.health < max)
                {
                    max = it.health;
                    roomId = it.mover.CurrentRoomId;
                    target = it;
                }
            }
            mover.ClearPath();
            mover.CreatePathTo(roomId, IsGhost());
        }

        public override void RemoveHits(int amount, GameItem item = null)
        {
            ActivateAdditionalProp();
            if (item != null)
                OnTakeDamageByItem(amount, this, item);
            base.RemoveHits(amount, item);
            if (health <= 0 && item != null)
            {
                OnDieByItem(this, item);
                item.KillMonster(this);
            }
        }

        public MonsterProperty GetAdditionalProp()
        {
            return additionalProperty;
        }

        public void SetAdditionalProp(MonsterProperty prop)
        {
            if (def.monsterType == MonsterType.Hellish)
                return;
            
            if (additionalProperty != null)
            {
                additionalProperty.Dispose();
                additionalProperty = null;
            }
            additionalProperty = prop;
            additionalProperty.Activate(this);
            GameField.Instance.MonsterRefresh(this);
        }

        public void ChangeAdditionalProp()
        {
            if (additionalProperty != null)
            {
                additionalProperty.Dispose();
                additionalProperty = null;
            }
            ActivateAdditionalProp();
        }

        public void ActivateAdditionalProp()
        {
            if (!canUseAdditionalProperty)
                return;
            
            if (def.monsterType == MonsterType.Hellish)
                return;
            
            if (additionalProperty != null)
                return;

            if(!GameField.defs.monsterProps.Exists(x => x.amountInDeck > 0))
                throw new Exception("Monster props defs are empty!");

            List<MonsterPropertyDef> propDefs = GameField.defs.monsterProps.FindAll(x => x.amountInDeck > 0);
            MonsterPropertyDef propDef = propDefs[UnityEngine.Random.Range(0, propDefs.Count)];
            propDef.amountInDeck--;
            additionalPropertyName = propDef.name;
            additionalProperty = (MonsterProperty)Activator.CreateInstance(Type.GetType("Scary.Models.MonsterProps." + propDef.name));
            UnityEngine.Debug.Log(string.Format("Monster {0} activates property {1}", def.name, propDef.name));

            //for tests
            //additionalPropertyName = "HellBlessing";
            //additionalProperty = (MonsterProperty)Activator.CreateInstance(Type.GetType("Scary.Models.MonsterProps." + additionalPropertyName));


            additionalProperty.Activate(this);
            OnActivateProperty(this);
        }

        #region For overload
        public virtual bool IsVampire()
        {
            return false;
        }

        public virtual int DamageOnMeeting()
        {
            return 0;
        }

        public virtual bool IsStationary()
        {
            return def.maxMovesCount == 0;
        }

        public virtual bool IsPlush()
        {
            return false;
        }
        #endregion

        public virtual void Dispose()
        {
            if (additionalProperty != null)
                additionalProperty.Dispose();
        }
    }
}
