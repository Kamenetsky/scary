﻿using System;
using System.Collections.Generic;

using Scary.Models.Monsters;

namespace Scary.Models
{
    public static class MonsterFactory
    {
        private static Dictionary<string, Type> defMonsterMap = new Dictionary<string, Type>
        {
            {"Banshee", typeof(Banshee)},
            {"BlackBall", typeof(BlackBall)},
            {"BloodyClown", typeof(BloodyClown)},
            {"Cactus", typeof(Cactus)},
            {"Chucha", typeof(Chucha)},
            {"ChuchasHand", typeof(ChuchasHand)},
            {"Coffin", typeof(Coffin)},
            {"CreepingNightmare", typeof(CreepingNightmare)},
            {"DeadGardener", typeof(DeadGardener)},
            {"DeadPlumber", typeof(DeadPlumber)},
            {"Demones", typeof(Demones)},
            {"FireDemon", typeof(FireDemon)},
            {"Ghost", typeof(Ghost)},
            {"Goblin", typeof(Goblin)},
            {"IceGuardian", typeof(IceGuardian)},
            {"JapanSpirit", typeof(JapanSpirit)},
            {"LaughingDevil", typeof(LaughingDevil)},
            {"LordVampire", typeof(LordVampire)},
            {"Mnogoglazka", typeof(Mnogoglazka)},
            {"Mummy", typeof(Mummy)},
            {"Nurse", typeof(Nurse)},
            {"Player", typeof(Player)},
            {"Poltergeist", typeof(Poltergeist)},
            {"Postman", typeof(Postman)},
            {"Receptionist", typeof(Receptionist)},
            {"ScaryBrat", typeof(ScaryBrat)},
            {"Skeleton", typeof(Skeleton)},
            {"Snowman", typeof(Snowman)},
            {"Spider", typeof(Spider)},
            {"Succubus", typeof(Succubus)},
            {"TeddyFreak", typeof(TeddyFreak)},
            {"Vampire", typeof(Vampire)},
            {"WalkingDeadSeller", typeof(WalkingDeadSeller)},
            {"Witch", typeof(Witch)},

            {"SilentHorror", typeof(SilentHorror)},
            {"DarkSpawn", typeof(DarkSpawn)},
            {"StepMother", typeof(StepMother)},
            {"ApocalypseRider", typeof(ApocalypseRider)}
        };

        public static bool canSpawnHellish = false;

        public static MonsterInfo CreateMonster(MonsterDef def)
        {
            if (def == null)
                throw new Exception("Can not create monster");
            
            if (!defMonsterMap.ContainsKey(def.name))
                throw new Exception("Can not find item: " + def.name);

            def.amountInDeck--;
            MonsterInfo monster = (MonsterInfo)Activator.CreateInstance(defMonsterMap[def.name], new MonsterDef(def));
            return monster;
        }

        public static MonsterDef GetRandomDef()
        {
            List<MonsterDef> defs = GameField.defs.monsters.FindAll(x => x.monsterType != MonsterType.Hellish
                                    && x.amountInDeck > 0 && x.name != "ChuchasHand" && x.name != "Player"
                                    && x.name != "Receptionist" && x.name != "LaughingDevil");
            if (defs.Count == 0)
                throw new Exception("Can not find monster def");

            MonsterDef def = defs[UnityEngine.Random.Range(0, defs.Count)];
            return def;
        }

        public static MonsterDef GetRandomHellishDef()
        {
            List<MonsterDef> defs =
                GameField.defs.monsters.FindAll(x => x.amountInDeck > 0 && x.monsterType == MonsterType.Hellish);
            if (defs.Count == 0)
                throw new Exception("Can not find hellish monster def");

            return defs[UnityEngine.Random.Range(0, defs.Count)];
        }

        public static MonsterDef GetDefByName(string name)
        {
            MonsterDef def = GameField.defs.monsters.Find(x => x.name == name);
            if (def.amountInDeck > 0)
                return def;

            //TODO remove later
            return def; //for tests
            //return null;
        }
    }
}

