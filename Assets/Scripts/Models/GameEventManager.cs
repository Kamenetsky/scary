﻿using System.Collections.Generic;

using UnityEngine;

namespace Scary.Models.GameEvents
{
    public class GameEventManager
    {       
        List<GameEventBase> events = new List<GameEventBase>();
        List<GameEventBase> hellishEvents = new List<GameEventBase>();

        public GameEventManager()
        {
            events.Add(new CofeePoisoned());
            events.Add(new LivingHouse());
            events.Add(new WallsChanges());
            events.Add(new WantSleep());
            events.Add(new WeAreListerning());
            events.Add(new YourFault());

            hellishEvents.Add(new HellishDarkness());
            hellishEvents.Add(new HellishSilence());
            hellishEvents.Add(new HellishStepmother());
        }

        public void SetRandomEvent()
        {
            GameEventBase currentEvent = events[Random.Range(0, events.Count)];
            currentEvent.Activate();
            GameField.Instance.SetGameEvent(currentEvent);
        }

        public void SetRandomHellishEvent()
        {
            GameEventBase currentEvent = hellishEvents[Random.Range(0, events.Count)];
            currentEvent.Activate();
            GameField.Instance.SetGameEvent(currentEvent);
        }
    }
}
