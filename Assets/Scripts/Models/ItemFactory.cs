﻿using System;
using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models
{
    public class ItemFactory
    {
        private static Dictionary<string, System.Type> defItemsMap = new Dictionary<string, System.Type>
        {
            {"Shovel", typeof(Shovel)},
            {"AspenStake", typeof(AspenStake)},
            {"Crucifix", typeof(Crucifix)},
            {"Shotgun", typeof(Shotgun)},
            {"Revolver", typeof(Revolver)},
            {"Hammer", typeof(Hammer)},
            {"GasBurner", typeof(GasBurner)},
            {"Knife", typeof(Knife)},
            {"Crossbow", typeof(Crossbow)},
            {"Boomerang", typeof(Boomerang)},
            {"Tourch", typeof(Tourch)},
            {"Sword", typeof(Sword)},
            {"Jerrycan", typeof(Jerrycan)},
            {"Flamethrower", typeof(Flamethrower)},
            {"HolyWater", typeof(HolyWater)},
            {"Shield", typeof(Shield)},
            {"Staff", typeof(Staff)},
            {"Relic", typeof(Relic)},
            {"Bible", typeof(Bible)},
            {"Reed", typeof(GameItemDecor)},
            {"SmokingPipe", typeof(GameItemDecor)},
            {"Maracas", typeof(GameItemDecor)},
            {"Toy", typeof(Toy)},
            {"DiamondsAce", typeof(DiamondsAce)},
            {"Bone", typeof(Bone)},
            {"Ring", typeof(Ring)}
        };

        static List<string> relicItems = new List<string>() { "Toy", "DiamondsAce", "Bone", "Ring" }; 
        
        public static GameItem CreateItem(string name, PersonInfo person)
        {
            if (!defItemsMap.ContainsKey(name))
                throw new Exception("Can not find item: " + name);

            if (GameField.defs.GetItemDefsCountByName(name) > 0)
            {
                ItemDef def = GameField.defs.GetItemDef(name);
                def.amountInDeck--;
                GameItem item = (GameItem)System.Activator.CreateInstance(defItemsMap[name], def, person);
                return item;
            }
            return null;
        }

        public static string GetRandom()
        {
            List<ItemDef> defs = GameField.defs.items.FindAll(x => x.amountInDeck > 0);
            if (defs.Count == 0)
                throw new Exception("Not enouth item defs");
            return defs[UnityEngine.Random.Range(0, defs.Count)].name;
        }

        public static string GetRandomRelic()
        {
            return relicItems[UnityEngine.Random.Range(0, relicItems.Count)];
        }
    }
}

