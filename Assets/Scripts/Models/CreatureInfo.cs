﻿using Scary.Models.Items;
using UnityEngine;

using System;
using System.Collections.Generic;

using Scary.Controllers;
using Scary.Utils;
using Scary.Models.Effects;

namespace Scary.Models
{
    public class MovableObject
    {
        List<Relation> currentPath = new List<Relation>();
        List<int> totalPath = new List<int>();
        public int PrevRoomId {
            get
            {
                if (totalPath.Count < 2)
                    return -1;
                return totalPath[totalPath.Count - 2];
            }
        }
        int currentRoomId;
        public int CurrentRoomId {
            get { return currentRoomId; }
            set
            {
                currentRoomId = value; 
                totalPath.Add(currentRoomId);
            }
        }

        public void CreatePathTo(int destRoomId, bool isFree = false)
        {
            currentPath.Clear();
            if (isFree)
                currentPath = DFS.GetRelations(GameField.searchExt.DijkstraSearch(CurrentRoomId, destRoomId));
            else
                currentPath = DFS.GetRelations(GameField.search.DijkstraSearch(CurrentRoomId, destRoomId));
        }

        public void ClearPath()
        {
            currentPath.Clear();
        }

        public int GetNextPathPoint()
        {
            if (currentPath.Count == 0)
                return -1;
            Relation rel = currentPath.Find(x => x.roomId1 == CurrentRoomId);

            if (rel == null)
                return -1;

            return rel.roomId2;
        }

        public int GetPrevPathPoint()
        {
            if (totalPath.Count < 3)
                return -1;
            return totalPath[totalPath.Count - 3];
        }

        public int GetEndPathPoint()
        {
            if (currentPath.Count == 0)
                return -1;

            return currentPath[currentPath.Count - 1].roomId2;
        }

        public int GetPathLength()
        {
            return currentPath.Count;
        }
    }
    
    public class CreatureDef
    {
        public CreatureDef()
        {            
        }
        public CreatureDef(CreatureDef origin)
        {
            name = origin.name;
            maxHealth = origin.maxHealth;
            maxMovesCount = origin.maxMovesCount;
        }
        
        public string name;
        public int maxHealth;
        public int maxMovesCount;
    }

    public interface ICreatureInfo { }

    public class CreatureInfo<T> : ICreatureInfo where T : CreatureDef
    {
        public static event Action<int, CreatureInfo<T>, CreatureInfo<CreatureDef>> OnChangesHits = delegate { };
        public static event Action<CreatureInfo<T>, bool> OnMove = delegate { };
        public static event Action<CreatureInfo<T>> OnDie = delegate { };
        public static event Action<CreatureInfo<T>> OnTurnComplete = delegate { };
        
        public T def;
        public string name;
        public int health;
        public int movesCount;
        public int movesCountBonus = 0;
        public bool isImmortal = false;
        public MovableObject mover = new MovableObject();

        protected bool isGhost = false;

        protected List<Effect> effects = new List<Effect>();

        public CreatureInfo(T def)
        {
            this.def = def;
            name = def.name;
            Reset();
        }

        public virtual void Reset()
        {
            movesCount = def.maxMovesCount + movesCountBonus;
            if (movesCountBonus < 0 && def.maxMovesCount > 0
                && def.maxMovesCount + movesCountBonus <= 0)
                movesCount = 1;
            ApplyEffects();
        }

        public virtual void ApplyEffects()
        {
            for (int i = 0; i < effects.Count; ++i)
            {
                if (effects[i].mustRemove)
                {
                    effects[i].Dispose();
                    effects.RemoveAt(i);
                }
            }
            
            foreach (var it in effects)
            {
                it.Activate(this);
            }
        }

        public virtual void AddEffect(Effect effect)
        {
            effects.Add(effect);
            if (effect.activateImmidiate)
                effect.Activate(this);
        }

        public virtual void RemoveHits(int amount, GameItem item = null)
        {
            if (!isImmortal && amount > 0)
                health -= amount;
            health = Mathf.Clamp(health, 0, health);
            OnChangesHits(amount, this, null);
            if (health <= 0)
                Die();
        }

        public virtual void AddHits(int amount)
        {
            if (amount <= 0)
                return;
            health += amount;
            OnChangesHits(-amount, this, null);
        }

        public virtual void Die()
        {
            Debug.Log("Die: " + this.def.name);
            OnDie(this);
        }

        public virtual void MoveTo(int roomId, int moveCost = 1, bool force = false)
        {
            if (roomId == mover.CurrentRoomId)
            {
                return;
            }
            movesCount -= moveCost;
            mover.CurrentRoomId = roomId;
            OnMove(this, force);
        }

        public virtual void CompleteTurn()
        {
            mover.ClearPath();
            OnTurnComplete(this);
        }

        public virtual bool IsGhost()
        {
            return isGhost;
        }

        public virtual void SetGhost(bool value)
        {
            isGhost = value;
        }
    }
}
