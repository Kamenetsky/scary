﻿using UnityEngine;

using System;
using System.Collections.Generic;

using Scary.Utils;
using Scary.Models.Items;

using Random = UnityEngine.Random;

namespace Scary.Models
{
    public class PlayerManager : IDisposable
    {
        private List<PersonInfo> players = new List<PersonInfo>();
        private PersonInfo currentPlayer = null;
        private int currentPlayerIndex = 0;

        public PlayerManager()
        {
            PersonInfo.OnMove += OnPlayerMove;
            PersonInfo.OnTurnComplete += OnPlayerTurnComplete;
            PersonInfo.OnChangesHits += OnPlayerChangesHits;
            PersonInfo.OnDie += OnPlayerDie;
            PersonInfo.OnCantMove += OnPlayerCantMove;
            PersonInfo.OnChangeItems += OnPlayerChangeItems;
            PersonInfo.OnUseAbilty += OnPlayerUseAbilty;

            GameItem.OnActivateProperty += OnActivateItemProperty;
        }

        public void Dispose()
        {
            PersonInfo.OnMove -= OnPlayerMove;
            PersonInfo.OnTurnComplete -= OnPlayerTurnComplete;
            PersonInfo.OnChangesHits -= OnPlayerChangesHits;
            PersonInfo.OnDie -= OnPlayerDie;
            PersonInfo.OnCantMove -= OnPlayerCantMove;
            PersonInfo.OnChangeItems -= OnPlayerChangeItems;
            PersonInfo.OnUseAbilty -= OnPlayerUseAbilty;

            GameItem.OnActivateProperty -= OnActivateItemProperty;
        }

        public List<PersonInfo> GetPlayers()
        {
            return players;
        }

        public PersonInfo GetCurrent()
        {
            return currentPlayer;
        }

        public List<PersonInfo> GetPlayersInRoom(int roomId)
        {
            List<PersonInfo> result = new List<PersonInfo>();
            foreach (var it in players)
            {
                if (it.mover.CurrentRoomId == roomId)
                    result.Add(it);
            }
            return result;
        }

        public void Init(List<PersonType> types = null)
        {
            if (Constants.maxPlayersCount > GameField.defs.characters.Count
                || (types != null && types.Count > GameField.defs.characters.Count))
            {
                throw new System.Exception("Too many players!");
            }
            
            players.Clear();
            currentPlayerIndex = 0;
            int currCount = 0;
            if (types != null)
            {
                foreach (var it in types)
                {
                    PersonDef def = GameField.defs.characters.Find(x => x.type == it);
                    def.maxMovesCount = 6;
                    PersonInfo pi = new PersonInfo(def);
                    players.Add(pi);
                    currCount++;
                }
            }

            while (currCount < Constants.minPlayersCount)
            {
                PersonDef def = GameField.defs.characters[UnityEngine.Random.Range(0, GameField.defs.characters.Count)];
                if (!players.Exists(a => a.def.type == def.type))
                {
                    def.maxMovesCount = 6;
                    players.Add(new PersonInfo(def));
                    currCount++;
                }
            }
            players.Sort((x, y) => { return x.def.initiative.CompareTo(y.def.initiative); });

            currentPlayer = players[currentPlayerIndex];
        }

        public void StartTurn()
        {
            currentPlayerIndex = 0;
            currentPlayer = players[currentPlayerIndex];
            currentPlayer.Reset();
            GameField.Instance.PlayerNext(players[players.Count - 1], currentPlayer);
        }

        public void MoveCurrentTo(int roomId)
        {
            currentPlayer.BuildPathTo(roomId, currentPlayer.IsGhost());
            currentPlayer.MoveToNextRoom();
        }

        public void MoveComplete(PersonInfo info)
        {
            info.MoveToNextRoom();
        }

        public void TurnLight()
        {
            PersonInfo pi = currentPlayer;
            RoomInfo ri = GameField.Instance.GetRoomInfo(pi.mover.CurrentRoomId);
            if (ri.isLightOn)
            {
                Debug.LogWarning("Light is already on!");
                return;
            }

            if (pi.movesCount < Constants.movesToTurnLight)
            {
                Debug.LogWarning("Too low turns!");
                return;
            }
            pi.movesCount -= Constants.movesToTurnLight;
            GameField.Instance.PlayerRefresh(pi);
            if (pi.movesCount <= 0)
                GetNextPlayer();
        }

        #region Callbacks
        void OnActivateItemProperty(GameItem item)
        {
            GameField.Instance.ItemActivateProperty(item);
        }

        void OnPlayerMove(CreatureInfo<PersonDef> info, bool force)
        {
            GameField.Instance.PlayerMove(info as PersonInfo, force);
        }

        void OnPlayerTurnComplete(CreatureInfo<PersonDef> info)
        {
            GetNextPlayer();
        }

        void OnPlayerDie(CreatureInfo<PersonDef> info)
        {
            GameField.Instance.PlayerDie(info as PersonInfo);
        }

        void OnPlayerChangesHits(int amount, CreatureInfo<PersonDef> info, CreatureInfo<CreatureDef> damager)
        {
            GameField.Instance.PlayerChangesHits(amount, info as PersonInfo);
        }

        void OnPlayerCantMove(PersonInfo info)
        {
            
        }

        void OnPlayerChangeItems(PersonInfo info)
        {
            GameField.Instance.PlayerChangeItems(info);
        }

        void OnPlayerUseAbilty(PersonInfo info)
        {
            GameField.Instance.PlayerUseAbility(info);
        }

        public void GetNextPlayer()
        {
            if (!players.Exists(x => !x.IsDead()))
            {
                //TODO make game over
                Debug.Log("Game over!!!");
                return;
            }
            
            PersonInfo oldPlayer = currentPlayer;
            ++currentPlayerIndex;
            
            if (currentPlayerIndex < players.Count)
            {
                currentPlayer = players[currentPlayerIndex];
                currentPlayer.Reset();
                GameField.Instance.PlayerNext(oldPlayer, currentPlayer);
            }
            else
            {
                GameField.Instance.PlayersCompleteTurn(oldPlayer);
            }
        }
        #endregion
    }
}
