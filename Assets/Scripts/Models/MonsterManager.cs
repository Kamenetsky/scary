﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Scary.Utils;

using Random = UnityEngine.Random;

namespace Scary.Models
{
    public class MonsterManager : IDisposable
    {
        private List<MonsterInfo> monsters = new List<MonsterInfo>();
        private MonsterInfo currentMonster = null;
        private int currentMonsterIndex = 0;

        public MonsterManager()
        {
            MonsterInfo.OnMove += OnMonsterMove;
            MonsterInfo.OnTurnComplete += OnMonsterTurnComplete;
            MonsterInfo.OnChangesHits += OnRemoveHits;
            MonsterInfo.OnDie += OnMonsterDie;
            MonsterInfo.OnAttackTarget += OnAttackTarget;
            MonsterInfo.OnStartAttackTarget += OnStartAttackTarget;
            MonsterInfo.OnActivateProperty += OnActivateProperty;
        }

        public void Dispose()
        {
            MonsterInfo.OnMove -= OnMonsterMove;
            MonsterInfo.OnTurnComplete -= OnMonsterTurnComplete;
            MonsterInfo.OnChangesHits -= OnRemoveHits;
            MonsterInfo.OnDie -= OnMonsterDie;
            MonsterInfo.OnAttackTarget -= OnAttackTarget;
            MonsterInfo.OnStartAttackTarget -= OnStartAttackTarget;
            MonsterInfo.OnActivateProperty -= OnActivateProperty;
        }

        public List<MonsterInfo> GetMonsters()
        {
            return monsters;
        }

        public List<MonsterInfo> GetMonstersInRoom(int roomId)
        {
            List<MonsterInfo> result = new List<MonsterInfo>();
            foreach (var it in monsters)
            {
                if (it.mover.CurrentRoomId == roomId)
                    result.Add(it);
            }
            return result;
        }
        
        public int GetCount()
        {
            return monsters.Count;
        }

        public void SpawnRandomHellish()
        {
            if (GameField.defs.monsters.Count(x => (x.amountInDeck > 0) && x.monsterType == MonsterType.Hellish) == 0)
                throw new Exception("Monster hellish deck is empty");

            MonsterDef def = MonsterFactory.GetRandomHellishDef(); ;
            Spawn(def);
        }
        
        public void SpawnRandom()
        {
            if (GameField.defs.monsters.Count(x => x.amountInDeck > 0) == 0)
                throw new Exception("Monster deck is empty");

            Spawn(MonsterFactory.GetRandomDef());
            //Spawn(MonsterFactory.GetDefByName("Nurse"));
        }

        public void Spawn(MonsterDef def, int aimRoomId = -1)
        {
            List<int> emptyRooms = new List<int>();
            foreach (var room in GameField.Instance.rooms)
            {
                if (!GameField.Instance.Players.Exists(x => x.mover.CurrentRoomId == room.id) && room.id != 0)
                    emptyRooms.Add(room.id);
            }
            int index = Random.Range(0, emptyRooms.Count);
            int roomId = aimRoomId >= 0 ? aimRoomId : emptyRooms[index];
            MonsterInfo info = MonsterFactory.CreateMonster(def);    
            info.mover.CurrentRoomId = roomId;
            info.index = monsters.Count + 1;
            monsters.Add(info);
            GameField.Instance.TurnLight(roomId, false, null);
            GameField.Instance.MonsterSpawn(info);
            UnityEngine.Debug.Log("Spawn monster: " + def.name);
        }

        public void Destroy(MonsterInfo info)
        {
            GameField.Instance.MonsterDestroy(info);
            if (currentMonster == info)
                currentMonster = null;
            info.Dispose();
            UnityEngine.Debug.Log("Monster removed " + info.name);
            monsters.Remove(info);
            for (int i = 0; i < monsters.Count; ++i)
                monsters[i].index = i + 1;
        }

        public void StartTurn()
        {
            currentMonsterIndex = 0;
            if (monsters.Count == 0)
                GetNextMonster();

            currentMonster = monsters[currentMonsterIndex];
            currentMonster.Reset();
            currentMonster.MakeDecision();
        }

        public void MoveComplete(MonsterInfo info)
        {
            info.MakeDecision();
        }

        void OnMonsterMove(CreatureInfo<MonsterDef> info, bool force)
        {
            GameField.Instance.MonsterMove(info as MonsterInfo, force);
        }

        void OnMonsterTurnComplete(CreatureInfo<MonsterDef> info)
        {
            GetNextMonster();
        }

        void OnMonsterDie(CreatureInfo<MonsterDef> info)
        {
            Destroy(info as MonsterInfo);
            if (!GameField.isPlayersTurn)
                GetNextMonster();
        }

        void OnRemoveHits(int amount, CreatureInfo<MonsterDef> info, CreatureInfo<CreatureDef> damager = null)
        {
            GameField.Instance.MonsterChangesHits(amount, info as MonsterInfo, damager);
        }

        void GetNextMonster()
        {
            MonsterInfo oldMonster = currentMonster;
            ++currentMonsterIndex;
            if (currentMonsterIndex < monsters.Count)
            {
                currentMonster = monsters[currentMonsterIndex];
                currentMonster.Reset();
                currentMonster.MakeDecision();
                GameField.Instance.MonsterNext(oldMonster, currentMonster);
            }
            else
            {
                GameField.Instance.MonstersCompleteTurn(oldMonster);
            }
        }

        void OnStartAttackTarget(PersonInfo person, MonsterInfo monster)
        {
            GameField.Instance.MonsterStartAttackPlayer(person, monster);
        }

        void OnAttackTarget(PersonInfo person, MonsterInfo monster)
        {
            GameField.Instance.MonsterAttackPlayer(person, monster);
        }

        void OnActivateProperty(MonsterInfo monster)
        {
            GameField.Instance.MonsterActivateProperty(monster);
        }
    }
}
