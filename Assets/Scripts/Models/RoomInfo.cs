﻿using UnityEngine;
using System.Collections;

namespace Scary.Models
{
    public enum RoomType
    {
        Default,
        Hall,
        Storage,
        Cellar,
        Library,
        Storeroom,
        Playroom,
        Kitchen,
        Bedroom,
        Corridor
    }

    public class RoomDef
    {
        public int id;
        public RoomType type;
        public int actionPoints = 1;
        public int preparePoints = 3;
        public string actionId = string.Empty;
    }

    public class RoomInfo : RoomDef
    {
        public RoomInfo(RoomDef def)
        {
            id = def.id;
            type = def.type;
            actionPoints = def.actionPoints;
            preparePoints = def.preparePoints;
            actionId = def.actionId;
        }
        
        public bool isLightOn = true;

        public bool CanSearch()
        {
            return !(type == RoomType.Corridor || type == RoomType.Hall);
        }
    }
}
