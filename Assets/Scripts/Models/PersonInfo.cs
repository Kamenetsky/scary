﻿using Scary.Models.Items;
using UnityEngine;

using System;
using System.Collections.Generic;
using Scary.Models.Effects;

namespace Scary.Models
{
    public enum PersonType
    {
        Priest,
        Exorcist,
        Military,
        Doctor,
        Scientist,
        Historian,
        Alchemist
    }

    public class PersonDef : CreatureDef
    {
        public PersonType type;
        public int initiative;
    }

    public class PersonInfo : CreatureInfo<PersonDef>
    {
        public static event Action<PersonInfo> OnCantMove = delegate { };
        public static event Action<PersonInfo> OnChangeItems = delegate { };
        public static event Action<PersonInfo> OnUseAbilty = delegate { };

        public int currentMovesCount;
        public int movesCountMultiplier = 1;
        public int healBonus = 0;
        public bool isBot = false;
        public bool canRessurect = true;
        public bool isAbilityUsed = false;
        public bool canUseAbility = true;

        private List<GameItem> items = new List<GameItem>();

        public MonsterInfo target = null;

        public PersonInfo(PersonDef def)
            : base(def)
        {
            health = def.maxHealth;
            mover.CurrentRoomId = 0;
            name = def.type.ToString();
        }

        public bool IsDead()
        {
            return health <= 0;
        }

        public override void Die()
        {
            if (UnityEngine.Random.Range(1, 7) > 4)
            {
                health = 0;
                AddHits(1);
            }
            else
                base.Die();
        }

        public void BuildPathTo(int roomId, bool isGhost = false)
        {
            mover.CreatePathTo(roomId, isGhost);
        }

        public override void Reset()
        {
            movesCount = 0;
            ApplyEffects();
        }

        public override void AddHits(int amount)
        {
            amount += healBonus;
            base.AddHits(amount);
        }

        public void UseAbility()
        {
            if (def.type == PersonType.Military)
            {
                GameField.Instance.MustSelectPlayer();
                GameField.Instance.OnSelectedPlayer += OnSelectedPlayer;
            }
            else if (def.type == PersonType.Alchemist)
            {
                List<MonsterInfo> monsters = GameField.Instance.GetMonstersInRoom(mover.CurrentRoomId);
                if (monsters.Count == 0)
                    return;
                GameField.Instance.MustSelectMonster(monsters);
                GameField.Instance.OnSelectedMonster += OnSelectedMonster;
            }
            else if (def.type == PersonType.Scientist)
            {
                List<GameItem> res = new List<GameItem>(items);
                foreach (var it in GameField.Instance.GetPlayersInRoom(mover.CurrentRoomId))
                {
                    for(int i = 0; i < it.GetItemsCount(); ++i)
                        res.Add(it.GetItem(i));
                }
                if (res.Count == 0)
                    return;
                GameField.Instance.MustSelectItems(res, true);
                GameField.Instance.OnSelectedGameItems += OnSelectedGameItems;
            }
            else if (def.type == PersonType.Historian && movesCount > 0)
            {
                List<GameItem> res = new List<GameItem>();
                for (int i = 0; i < items.Count; ++i)
                {
                    if (string.IsNullOrEmpty(items[i].additionalPropertyName))
                        res.Add(items[i]);
                }
                    
                foreach (var it in GameField.Instance.GetPlayersInRoom(mover.CurrentRoomId))
                {
                    for (int i = 0; i < it.GetItemsCount(); ++i)
                    {
                        if (string.IsNullOrEmpty(it.GetItem(i).additionalPropertyName))
                            res.Add(it.GetItem(i));
                    }
                }
                if (res.Count == 0)
                    return;

                GameField.Instance.MustSelectItems(res, true);
                GameField.Instance.OnSelectedGameItems += OnSelectedGameItems;
                movesCount--;
            }
            else if (def.type == PersonType.Doctor && movesCount > 0)
            {
                List<PersonInfo> res = new List<PersonInfo>();
                res.Add(this);
                res.AddRange(GameField.Instance.GetPlayersInRoom(mover.CurrentRoomId));
                GameField.Instance.MustSelectPlayer(res);
                GameField.Instance.OnSelectedPlayer += OnSelectedPlayer;
                movesCount--;
            }
            else if (def.type == PersonType.Priest && movesCount > 0)
            {
                GameField.Instance.MustSelectMonster();
                GameField.Instance.OnSelectedMonster += OnSelectedMonster;
                movesCount--;
            }
            else if (def.type == PersonType.Exorcist && movesCount > 0)
            {
                GameField.Instance.MustSelectMonster();
                GameField.Instance.OnSelectedMonster += OnSelectedMonster;
                movesCount--;
            }
            else
            {
                return;
            }
            isAbilityUsed = true;
            OnUseAbilty(this);
        }

        void OnSelectedPlayer(PersonInfo player)
        {
            GameField.Instance.OnSelectedPlayer -= OnSelectedPlayer;
            if (def.type == PersonType.Military)
            {
                player.AddEffect(new EffectMoveBonus() { activateImmidiate = true/*player == this*/ });
                if (movesCount > 0)
                    OnUseAbilty(this);
            }
            else if (def.type == PersonType.Doctor)
            {
                if (player.health < player.def.maxHealth)
                    player.AddHits(1);
                OnUseAbilty(this);
            }
        }

        void OnSelectedMonster(MonsterInfo monster)
        {
            GameField.Instance.OnSelectedMonster -= OnSelectedMonster;
            if (def.type == PersonType.Alchemist)
            {
                monster.AddEffect(new EffectBlockAbility(){activateImmidiate = true, turnsCount = 2});
            }
            else if (def.type == PersonType.Priest)
            {
                monster.RemoveHits(GameField.Instance.Players.Exists(x => x.IsDead()) ? 2 : 1);
            }
            else if (def.type == PersonType.Exorcist)
            {
                int id = monster.mover.CurrentRoomId;
                monster.Die();
                GameField.Instance.SpawnMonsterInRoom("", id);
            }
            OnUseAbilty(this);
        }

        void OnSelectedGameItems(List<GameItem> selected)
        {
            GameField.Instance.OnSelectedGameItems -= OnSelectedGameItems;
            if (def.type == PersonType.Scientist)
            {
                if (selected.Count > 0)
                    selected[0].canUseAdditionalProperty = false;
            }
            else if (def.type == PersonType.Historian)
            {
                if (selected.Count > 0)
                    selected[0].ActivateAdditionalProp();
            }
            OnUseAbilty(this);
        }

        public override void MoveTo(int roomId, int moveCost = 1, bool force = false)
        {
            foreach (var it in GameField.Instance.GetMonstersInRoom(roomId))
            {
                if (it.DamageOnMeeting() > 0)
                    RemoveHits(it.DamageOnMeeting());
            }
            base.MoveTo(roomId, moveCost);
        }

        public void MoveToNextRoom()
        {
            if (movesCount <= 0)
            {
                CompleteTurn();
                return;
            }

            int nextId = mover.GetNextPathPoint();
            if (nextId >= 0)
            {
                RoomInfo ri = GameField.Instance.GetRoomInfo(nextId);
                bool hasNightmare = false;
                foreach (var it in GameField.Instance.GetMonstersInRoom(nextId))
                {
                    if (it is Scary.Models.Monsters.CreepingNightmare)
                    {
                        hasNightmare = true;
                        break;
                    }
                }

                if (ri.isLightOn && !hasNightmare)
                {
                    MoveTo(nextId);
                }
                else
                {
                    if (movesCount >= Constants.movesToDarkRoom)
                    {
                        MoveTo(nextId, Constants.movesToDarkRoom);
                    }
                    else
                    {
                        OnCantMove(this);
                    }
                }
            }
        }

        public int GetItemsSlotCount()
        {
            int result = 0;
            foreach (var it in items)
            {
                result += it.GetSlotsCount();
            }
            return result;
        }

        public void AddItem(GameItem item)
        {
            if (GetItemsSlotCount() >= Constants.maxItemsCount)
                return;
            items.Add(item);
            OnChangeItems(this);
        }

        public void RefreshItems()
        {
            OnChangeItems(this);
        }

        public void RemoveItem(GameItem item)
        {
            items.Remove(item);
            OnChangeItems(this);
        }

        public void RemoveItems(List<GameItem> removed)
        {
            items.RemoveAll(x => removed.Contains(x));
            OnChangeItems(this);
        }

        public int GetItemsCount()
        {
            return items.Count;
        }

        public GameItem GetItem(int index)
        {
            return items[index];
        }

        public List<GameItem> GetItems()
        {
            return items;
        }

        public bool HasShovel()
        {
            return items.Exists(x => x.AllowToDig());
        }
    }
}
