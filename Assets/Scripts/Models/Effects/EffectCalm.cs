﻿using UnityEngine;

namespace Scary.Models.Effects
{
    public class EffectCalm : Effect
    {
        private MonsterInfo monster = null;
        private PersonInfo person = null;

        private int initMovesCount;

        public override void Activate<T>(CreatureInfo<T> creature)
        {
            base.Activate(creature);
            if (creature is MonsterInfo)
            {
                monster = creature as MonsterInfo;
            }
            else if (creature is PersonInfo)
            {
                person = creature as PersonInfo;
            }

            initMovesCount = creature.def.maxMovesCount;
            creature.def.maxMovesCount = 1;
        }

        public override void Dispose()
        {
            base.Dispose();
            if (monster != null)
                monster.def.maxMovesCount = initMovesCount;
            else if (person != null)
                person.def.maxMovesCount = initMovesCount;
        }
    }
}
