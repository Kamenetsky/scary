﻿using System;
using UnityEngine;
using System.Collections;
using Scary.Models;

namespace Scary.Models.Effects
{
    public abstract class Effect : IDisposable
    {
        public bool mustRemove = false;
        public int turnsCount = 1;
        protected int currTurn = 0;
        public bool isActivated = false;
        public bool activateOnce = true;
        public bool activateImmidiate = false;

        public virtual void Activate<T>(CreatureInfo<T> creature) where T : CreatureDef
        {
            currTurn++;
            if (currTurn >= turnsCount)
                mustRemove = true;

            if (activateOnce && isActivated)
                return;

            isActivated = true;
        }

        public virtual void Dispose()
        {          
        }
    }
}
