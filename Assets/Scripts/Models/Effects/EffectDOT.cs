﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Effects
{
    public class EffectDOT : Effect
    {
        public int damage = 1;

        public override void Activate<T>(CreatureInfo<T> creature)
        {
            base.Activate(creature);
            if (creature != null)
                creature.RemoveHits(damage, null);
        }
    }
}
