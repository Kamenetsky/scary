﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Effects
{
    public class EffectHealthBonus : Effect
    {
        public int value = 1;
        
        private MonsterInfo monster = null;
        private PersonInfo person = null;

        public override void Activate<T>(CreatureInfo<T> creature)
        {
            base.Activate(creature);
            if (creature is MonsterInfo)
            {
                monster = creature as MonsterInfo;
            }
            else if (creature is PersonInfo)
            {
                person = creature as PersonInfo;
            }
            if (value > 0)
                creature.AddHits(value); ;
        }

        public override void Dispose()
        {
            base.Dispose();
            if (monster != null)
            {
                if (monster.health > value)
                    monster.RemoveHits(value);
                else
                {
                    monster.RemoveHits(monster.health - 1);
                }
            }
            else if (person != null)
            {
                if (person.health > value)
                    person.RemoveHits(value);
                else
                {
                    person.RemoveHits(person.health - 1);
                }
            }
        }
    }
}
