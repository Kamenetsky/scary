﻿using System;
using UnityEngine;
using System.Collections;

namespace Scary.Models.Effects
{
    public class EffectBlockAbility : Effect
    {
        private MonsterInfo monster = null;

        public override void Activate<T>(CreatureInfo<T> creature)
        {
            base.Activate(creature);
            if (creature is MonsterInfo)
            {
                monster = creature as MonsterInfo;
            }
            else
            {
                throw new Exception("EffectBlockAbility: Can not find MonsterInfo entity!");
            }

            monster.canUseAdditionalProperty = false;
            if (monster.GetAdditionalProp() != null)
                monster.GetAdditionalProp().Deactivate();
        }

        public override void Dispose()
        {
            base.Dispose();
            monster.canUseAdditionalProperty = true;
            if (monster.GetAdditionalProp() != null)
                monster.GetAdditionalProp().Reactivate();
        }
    }
}
