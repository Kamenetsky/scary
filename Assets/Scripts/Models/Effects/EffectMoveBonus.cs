﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.Effects
{
    public class EffectMoveBonus : Effect
    {
        public int value = 1;

        private MonsterInfo monster = null;
        private PersonInfo person = null;

        public override void Activate<T>(CreatureInfo<T> creature)
        {
            base.Activate(creature);
            if (creature is MonsterInfo)
            {
                monster = creature as MonsterInfo;
            }
            else if (creature is PersonInfo)
            {
                person = creature as PersonInfo;
            }
            if (value > 0)
            {
                creature.movesCountBonus += value;
                GameField.Instance.SetPlayerMovesCount(creature.movesCount);
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            if (monster != null)
            {
                monster.movesCountBonus -= value;
            }
            else if (person != null)
            {
                person.movesCountBonus -= value;
            }
        }
    }
}
