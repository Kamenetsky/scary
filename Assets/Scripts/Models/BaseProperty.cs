﻿using UnityEngine;
using System.Collections;

namespace Scary.Models
{
    public class BasePropertyDef
    {
        public string name;
        public int amountInDeck;

        public BasePropertyDef()
        {
        }

        public BasePropertyDef(BasePropertyDef def)
        {
            name = def.name;
            amountInDeck = def.amountInDeck;
        }
    }
}
