﻿
namespace Scary.Models.GameEvents
{
    public class NeedGoodPlan : GameEventBase
    {
        public override void Activate()
        {
            base.Activate();
            GameField.Instance.preparePointsBonus = 1;
        }

        public override void Dispose()
        {
            base.Dispose();
            GameField.Instance.preparePointsBonus = 0;
        }
    }
}
