﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.GameEvents
{
    public class FearIsIllusion : GameEventBase
    {
        public override void Activate()
        {
            base.Activate();
            foreach (var it in GameField.Instance.Players)
            {
                it.movesCountBonus += 1;
            }
            GameField.allowFindItems = false;
            GameField.allowDefense = false;
        }

        public override void Dispose()
        {
            base.Dispose();
            foreach (var it in GameField.Instance.Players)
            {
                it.movesCountBonus -= 1;
            }
            GameField.allowFindItems = true;
            GameField.allowDefense = true;
        }
    }
}
