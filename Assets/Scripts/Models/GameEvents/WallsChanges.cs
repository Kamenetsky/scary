﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.GameEvents
{
    public class WallsChanges : GameEventBase
    {
        public override void Activate()
        {
            base.Activate();
            foreach (var it in GameField.Instance.Players)
            {
                it.SetGhost(true);
            }

            foreach (var it in GameField.Instance.Monsters)
            {
                it.SetGhost(true);
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            foreach (var it in GameField.Instance.Players)
            {
                it.SetGhost(false);
            }

            foreach (var it in GameField.Instance.Monsters)
            {
                it.SetGhost(false);
            }
        }
    }
}
