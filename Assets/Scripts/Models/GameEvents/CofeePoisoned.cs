﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.GameEvents
{
    public class CofeePoisoned : GameEventBase
    {
        public override void Activate()
        {
            base.Activate();
            foreach (var pi in GameField.Instance.Players)
            {
                pi.movesCountBonus -= 1;
                pi.def.maxHealth += 1;
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            foreach (var pi in GameField.Instance.Players)
            {
                pi.movesCountBonus = 0;
                pi.def.maxHealth -= 1;
            }
        }
    }
}
