﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.GameEvents
{
    public class Darkness : GameEventBase
    {
        public override void Activate()
        {
            base.Activate();
            foreach (var pi in GameField.Instance.Players)
            {
                if (pi.IsDead())
                    continue;
                GameField.Instance.TurnLight(pi.mover.CurrentRoomId, false, null);
            }
        }
    }
}
