﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.GameEvents
{
    public class WantSleep : GameEventBase
    {
        public override void Activate()
        {
            base.Activate();
            foreach (var it in GameField.Instance.Players)
            {
                it.canUseAbility = false;
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            foreach (var it in GameField.Instance.Players)
            {
                it.canUseAbility = true;
            }
        }
    }
}
