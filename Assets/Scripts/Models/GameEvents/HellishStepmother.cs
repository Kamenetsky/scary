﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.GameEvents
{
    public class HellishStepmother : GameEventBase
    {
        public override void Activate()
        {
            base.Activate();
            GameField.Instance.SpawnConcreteMonster("StepMother");
        }
    }
}
