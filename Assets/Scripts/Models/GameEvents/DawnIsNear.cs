﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.GameEvents
{
    public class DawnIsNear : GameEventBase
    {
        public override void Activate()
        {
            base.Activate();
            if (GameField.Instance.currentTime.Hour >= 3)
            {
                foreach (var it in GameField.Instance.Monsters)
                {
                    it.healthBonus -= 1;
                    it.damageBonus += 1;
                }
            }
            else
            {
                foreach (var it in GameField.Instance.Monsters)
                {
                    it.healthBonus += 1;
                    it.damageBonus -= 1;
                }
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            if (GameField.Instance.currentTime.Hour >= 4)
            {
                foreach (var it in GameField.Instance.Monsters)
                {
                    it.healthBonus += 1;
                    it.damageBonus -= 1;
                }
            }
            else
            {
                foreach (var it in GameField.Instance.Monsters)
                {
                    it.healthBonus -= 1;
                    it.damageBonus += 1;
                }
            }
        }
    }
}
