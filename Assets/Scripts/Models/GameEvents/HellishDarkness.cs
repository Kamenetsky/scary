﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.GameEvents
{
    public class HellishDarkness : GameEventBase
    {
        public override void Activate()
        {
            base.Activate();
            for (int i = 0; i < GameField.Instance.Players.Count + Constants.monstersPlayersCountDiff; ++i)
            {
                if (GameField.Instance.Monsters.Count <= Constants.maxMonstersCount)
                    GameField.Instance.SpawnMonsterInRoom();
            }
            GameField.Instance.SpawnConcreteMonster("DarkSpawn");
        }
    }
}
