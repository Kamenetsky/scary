﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.GameEvents
{
    public class IAmScary : GameEventBase
    {
        public override void Activate()
        {
            base.Activate();
            GameField.Instance.OnPlayersCompleteTurn += OnPlayersCompleteTurn;
        }

        void OnPlayersCompleteTurn(PersonInfo player)
        {
            foreach (var it in GameField.Instance.Players)
            {
                if (GameField.Instance.GetPlayersInRoom(it.mover.CurrentRoomId).Count < 2)
                    it.RemoveHits(1);
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            GameField.Instance.OnPlayersCompleteTurn -= OnPlayersCompleteTurn;
        }
    }
}
