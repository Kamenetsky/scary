﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.GameEvents
{
    public class YourFault : GameEventBase
    {
        public override void Activate()
        {
            GameField.allowExchange = false;
            base.Activate();
        }

        public override void Dispose()
        {
            GameField.allowExchange = true;
            base.Dispose();
        }
    }
}
