﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.GameEvents
{
    public class RunAway : GameEventBase
    {
        public override void Activate()
        {
            base.Activate();
            GameField.Instance.OnMonsterChangesHits += OnMonsterChangesHits;
        }

        void OnMonsterChangesHits(int damage, MonsterInfo monster)
        {
            if (damage > monster.health)
            {
                monster.Reset();
                GameField.Instance.MonsterRefresh(monster);
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            GameField.Instance.OnMonsterChangesHits -= OnMonsterChangesHits;
        }
    }
}
