﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.GameEvents
{
    public class WeAreListerning : GameEventBase
    {
        public override void Activate()
        {
            base.Activate();
            foreach (var it in GameField.Instance.Players)
            {
                it.healBonus = 1;
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            foreach (var it in GameField.Instance.Players)
            {
                it.healBonus = 0;
            }
        }
    }
}
