﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.GameEvents
{
    public class MosterHealth : GameEventBase
    {
        public override void Activate()
        {
            base.Activate();
            foreach (var it in GameField.Instance.Monsters)
            {
                it.movesCountBonus += 1;
                it.damageBonus -= 1;
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            foreach (var it in GameField.Instance.Monsters)
            {
                if (it.movesCountBonus > 0)
                    it.movesCountBonus -= 1;
                it.damageBonus += 1;
            }
        }
    }
}
