﻿using UnityEngine;
using System;

namespace Scary.Models.GameEvents
{
    public abstract class GameEventBase : IDisposable
    {
        public bool IsActivated { get; private set; }

        public virtual void Activate()
        {
            IsActivated = true;
            GameField.Instance.OnHourComplete += OnHourComplete;
        }

        public virtual void Dispose() { }

        void OnHourComplete()
        {
            GameField.Instance.OnHourComplete -= OnHourComplete;
            Dispose();
        }
    }
}
