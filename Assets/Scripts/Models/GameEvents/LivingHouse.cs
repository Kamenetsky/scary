﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.GameEvents
{
    public class LivingHouse : GameEventBase
    {
        private PersonInfo prevPlayer = null;
        private int prevRoomId = -1;
        
        public override void Activate()
        {
            base.Activate();
            GameField.Instance.OnNextPlayer += OnNextPlayer;
        }

        void OnNextPlayer(PersonInfo oldPlayer, PersonInfo player)
        {
            if (oldPlayer != null && prevRoomId >= 0 && prevPlayer == oldPlayer && prevRoomId == oldPlayer.mover.CurrentRoomId)
                oldPlayer.RemoveHits(1);
            prevRoomId = player.mover.CurrentRoomId;
            prevPlayer = player;
        }

        public override void Dispose()
        {
            base.Dispose();
            GameField.Instance.OnNextPlayer -= OnNextPlayer;
        }
    }
}
