﻿using UnityEngine;
using System.Collections;

namespace Scary.Models.GameEvents
{
    public class HellishSilence : GameEventBase
    {
        public override void Activate()
        {
            base.Activate();
            foreach (var it in GameField.Instance.Monsters)
            {
                it.Die();
            }
            foreach (var it in GameField.Instance.Players)
            {
                if (it.GetItemsCount() > 0)
                    it.RemoveItem(it.GetItem(0));
            }
            GameField.Instance.SpawnConcreteMonster("SilentHorror");
        }
    }
}
