﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class Memory : ItemProperty
    {
        public Memory(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
        }

        public override void Activate(MonsterInfo monster)
        {
            List<ItemDef> defs = GameField.defs.items.FindAll(x => x.amountInDeck < x.initAmountInDeck);
            if (defs.Count > 0)
                defs[Random.Range(0, defs.Count)].amountInDeck++;
        }
    }
}
