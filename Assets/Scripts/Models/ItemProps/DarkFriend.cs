﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class DarkFriend : ItemProperty
    {
        public DarkFriend(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
        }

        public override void Activate(MonsterInfo monster)
        {
            List<RoomInfo> rooms = GameField.Instance.rooms.FindAll(x => x.isLightOn);
            if (rooms.Count > 0)
                GameField.Instance.TurnLight(rooms[Random.Range(0, rooms.Count)].id, false, null);
        }
    }
}
