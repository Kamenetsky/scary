﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class LostRelic : ItemProperty
    {
        private int initDamage = 0;
        public LostRelic(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
            initDamage = item.def.damage;
        }

        public override void Activate(MonsterInfo monster)
        {
            if (item.def.damageType != DamageType.Sanctification && item.GetOwner().def.type == PersonType.Priest &&
                monster.def.vulnerabilities.Contains(DamageType.Sanctification))
            {
                item.def.damage = initDamage + 2;
            }
            else
            {
                item.def.damage = initDamage;
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            item.def.damage = initDamage;
        }
    }
}
