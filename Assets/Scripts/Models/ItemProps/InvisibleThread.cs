﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class InvisibleThread : ItemProperty
    {
        PersonInfo player = null;

        public InvisibleThread(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
        }

        public override void Activate(MonsterInfo monster)
        {
            GameField.Instance.MustSelectPlayer();
            GameField.Instance.OnSelectedPlayer += OnSelectedPlayer;
        }

        void OnSelectedPlayer(PersonInfo player)
        {
            this.player = player;
            GameField.Instance.OnSelectedPlayer -= OnSelectedPlayer;
            GameField.Instance.MustSelectRoom();
            GameField.Instance.OnSelectedRoom += OnSelectedRoom;
        }

        void OnSelectedRoom(RoomInfo room)
        {
            List<int> ids = new List<int>();
            ids = GameField.neightborsById[player.mover.CurrentRoomId];

            if (ids.Contains(room.id))
            {
                GameField.Instance.OnSelectedRoom -= OnSelectedRoom;
                player.MoveTo(room.id, 0, true);
            }
            else
            {
                GameField.Instance.MustSelectRoom();
            }
        }
    }
}
