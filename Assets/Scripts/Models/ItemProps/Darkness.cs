﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class Darkness : ItemProperty
    {
        public Darkness(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
        }

        public override void Activate(MonsterInfo monster)
        {
           GameField.Instance.TurnLight(item.GetOwner().mover.CurrentRoomId, false, null);
        }
    }
}
