﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class Playing : ItemProperty
    {
        bool goodResult;
        public Playing(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
        }

        public override void Activate(MonsterInfo monster)
        {
            goodResult = Random.Range(1, 7) > 3;
            GameField.Instance.OnSelectedPlayer += OnSelectedPlayer;
            GameField.Instance.MustSelectPlayer();
        }

        void OnSelectedPlayer(PersonInfo player)
        {
            GameField.Instance.OnSelectedPlayer -= OnSelectedPlayer;
            if (goodResult)
                player.AddHits(1);
            else
            {
                player.RemoveHits(1);
            }
        }
    }
}
