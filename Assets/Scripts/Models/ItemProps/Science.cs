﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class Science : ItemProperty
    {
        public Science(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
        }

        public override void Activate(MonsterInfo monster)
        {
            if (item.GetOwner().def.type != PersonType.Scientist)
                return;

            foreach (var it in GameField.Instance.GetMonstersInRoom(item.GetOwner().mover.CurrentRoomId))
            {
                if (it.health > 1)
                    it.RemoveHits(1);
            }
        }
    }
}
