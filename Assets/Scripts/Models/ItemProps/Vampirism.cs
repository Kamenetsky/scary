﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class Vampirism : ItemProperty
    {
        public Vampirism(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
        }

        public override void Activate(MonsterInfo monster)
        {
        }

        public override void KillMonster(MonsterInfo monster)
        {
            if (item.GetOwner().health < item.GetOwner().def.maxHealth)
                item.GetOwner().AddHits(1);
        }
    }
}
