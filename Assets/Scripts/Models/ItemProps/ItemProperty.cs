﻿using System;
using Scary.Models.Items;


namespace Scary.Models.ItemProps
{
    public enum ItemPropertyType
    {
        Active,
        Passive
    }

    public class ItemPropertyDef : BasePropertyDef
    {
        public ItemPropertyType propertyType;

        public ItemPropertyDef()
        {
        }

        public ItemPropertyDef(ItemPropertyDef def) : base(def)
        {
            propertyType = def.propertyType;
        }
    }

    public abstract class ItemProperty : IDisposable
    {
        protected ItemPropertyDef def = null;
        protected GameItem item = null;

        public ItemProperty(ItemPropertyDef def, GameItem item)
        {
            this.def = def;
            this.item = item;
        }

        public abstract void Activate(MonsterInfo monster);

        public virtual void KillMonster(MonsterInfo monster)
        {
        }

        public virtual void Dispose()
        {           
        }
    }
}
