﻿using Scary.Models.MonsterProps;
using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class Anomaly : ItemProperty
    {
        private MonsterInfo monster = null;
        public Anomaly(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
        }

        public override void Activate(MonsterInfo monster)
        {
            monster = null;
            if (GameField.Instance.Monsters.Count < 2)
                return;
            GameField.Instance.MustSelectMonster();
            GameField.Instance.OnSelectedMonster += OnSelectedMonster;
        }

        void OnSelectedMonster(MonsterInfo monster)
        {
            if (this.monster == null)
            {
                this.monster = monster;
                GameField.Instance.MustSelectMonster();
            }
            else if (this.monster == monster)
            {
                GameField.Instance.MustSelectMonster();
            }
            else
            {
                if (this.monster.GetAdditionalProp() == null && monster.GetAdditionalProp() != null)
                {
                    monster.ChangeAdditionalProp();
                }
                else if (this.monster.GetAdditionalProp() != null && monster.GetAdditionalProp() == null)
                {
                    this.monster.ChangeAdditionalProp();
                }
                else if (this.monster.GetAdditionalProp() != null && monster.GetAdditionalProp() != null)
                {
                    MonsterProperty prop = this.monster.GetAdditionalProp();
                    this.monster.SetAdditionalProp(monster.GetAdditionalProp());
                    monster.SetAdditionalProp(prop);
                }
            }
        }
    }
}
