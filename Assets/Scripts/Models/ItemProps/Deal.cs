﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class Deal : ItemProperty
    {
        public Deal(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
        }

        public override void Activate(MonsterInfo monster)
        {
            if (item.GetOwner().GetItemsCount() == 1)
                return;
            foreach (var it in item.GetOwner().GetItems())
            {
                if (item != it)
                {
                    it.Drop();
                    GameField.Instance.OnSelectedPlayer += OnSelectedPlayer;
                    GameField.Instance.MustSelectPlayer();
                    break;
                }
            }
        }

        void OnSelectedPlayer(PersonInfo player)
        {
            if (player.health < player.def.maxHealth)
                player.AddHits(1);
        }
    }
}
