﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class Storm : ItemProperty
    {
        public Storm(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
            item.GetOwner().movesCountBonus += 1;
        }

        public override void Activate(MonsterInfo monster)
        {
        }

        public override void Dispose()
        {
            base.Dispose();
            if (item.GetOwner().movesCountBonus > 0)
                item.GetOwner().movesCountBonus -= 1;
        }
    }
}
