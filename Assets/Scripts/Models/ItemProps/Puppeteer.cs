﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class Puppeteer : ItemProperty
    {
        MonsterInfo monster = null;
        
        public Puppeteer(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
        }

        public override void Activate(MonsterInfo monster)
        {
            GameField.Instance.MustSelectMonster();
            GameField.Instance.OnSelectedMonster += OnSelectedMonster;
        }

        void OnSelectedMonster(MonsterInfo monster)
        {
            this.monster = monster;
            GameField.Instance.OnSelectedMonster -= OnSelectedMonster;
            GameField.Instance.MustSelectRoom();
            GameField.Instance.OnSelectedRoom += OnSelectedRoom;
        }

        void OnSelectedRoom(RoomInfo room)
        {
            List<int> ids = new List<int>();
            if (monster.IsGhost())
                ids = GameField.neightborsByIdFree[monster.mover.CurrentRoomId];
            else
                ids = GameField.neightborsById[monster.mover.CurrentRoomId];

            if (ids.Contains(room.id))
            {
                GameField.Instance.OnSelectedRoom -= OnSelectedRoom;
                monster.MoveTo(room.id, 0, true);
            }
            else
            {
                GameField.Instance.MustSelectRoom();
            }
        }
    }
}
