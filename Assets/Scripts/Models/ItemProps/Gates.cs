﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class Gates : ItemProperty
    {
        public Gates(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
        }

        public override void Activate(MonsterInfo monster)
        {
            GameField.Instance.SpawnMonsterInRoom();
        }
    }
}
