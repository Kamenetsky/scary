﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class Light : ItemProperty
    {
        public Light(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
            item.SetLight(true);
        }

        public override void Activate(MonsterInfo monster)
        {
        }

        public override void Dispose()
        {
            base.Dispose();
            item.SetLight(false);
        }
    }
}
