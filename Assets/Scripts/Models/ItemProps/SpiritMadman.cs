﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class SpiritMadman : ItemProperty
    {
        private int addPoints = 0;
        public SpiritMadman(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
            GameField.Instance.OnPlayerSetMovesCount += OnPlayerSetMovesCount;
            GameField.Instance.OnNextPlayer += OnNextPlayer;
        }

        public override void Activate(MonsterInfo monster)
        {
            item.GetOwner().movesCountMultiplier = 2;
        }

        void OnPlayerSetMovesCount(int factValue, PersonInfo person)
        {
            addPoints = (person.movesCount - factValue)/2;
        }

        void OnNextPlayer(PersonInfo oldPerson, PersonInfo person)
        {
            if (oldPerson == item.GetOwner())
            {
                oldPerson.RemoveHits(addPoints);
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            item.GetOwner().movesCountMultiplier = 1;
        }
    }
}
