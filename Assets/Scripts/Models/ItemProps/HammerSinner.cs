﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class HammerSinner : ItemProperty
    {
        public HammerSinner(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
            item.useMovesCount = 2;
        }

        public override void Activate(MonsterInfo monster)
        {
        }

        public override void Dispose()
        {
            item.useMovesCount = 1;
            base.Dispose();
        }
    }
}
