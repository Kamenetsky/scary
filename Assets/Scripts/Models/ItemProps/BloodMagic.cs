﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class BloodMagic : ItemProperty
    {
        public BloodMagic(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
        }

        public override void Activate(MonsterInfo monster)
        {
            GameField.Instance.MustSelectMonster();
            GameField.Instance.OnSelectedMonster += OnSelectedMonster;
        }

        void OnSelectedMonster(MonsterInfo monster)
        {
            GameField.Instance.OnSelectedMonster -= OnSelectedMonster;
            monster.RemoveHits(1, item);
            item.GetOwner().RemoveHits(1, item);
        }
    }
}
