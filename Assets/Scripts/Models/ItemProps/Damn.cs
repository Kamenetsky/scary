﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class Damn : ItemProperty
    {
        private MonsterInfo monster = null;
        public Damn(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
            GameField.Instance.OnMonsterChangesHits += OnMonsterChangesHits;
        }

        public override void Activate(MonsterInfo monster)
        {
            this.monster = monster;
        }

        void OnMonsterChangesHits(int damage, MonsterInfo monster)
        {
            if (monster == this.monster && monster.health < monster.def.maxHealth)
            {
                monster.health++;
                GameField.Instance.MonsterRefresh(monster);
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            GameField.Instance.OnMonsterChangesHits -= OnMonsterChangesHits;
        }
    }
}
