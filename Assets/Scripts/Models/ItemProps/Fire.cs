﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class Fire : ItemProperty
    {
        private int initDamage = 0;
        public Fire(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
        }

        public override void Activate(MonsterInfo monster)
        {
            if (item.GetOwner().def.type == PersonType.Alchemist &&
                monster.def.vulnerabilities.Contains(DamageType.Fire))
            {
                item.def.damage = initDamage + 1;
            }
            else
            {
                item.def.damage = initDamage;
            }
        }
        public override void Dispose()
        {
            base.Dispose();
            item.def.damage = initDamage;
        }
    }
}
