﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class ThisIsShovel : ItemProperty
    {
        public ThisIsShovel(ItemPropertyDef def, GameItem item) : base(def, item)
        {
        }

        public override void Activate(MonsterInfo monster)
        {
            item.SetShovel();
            item.def = new ItemDef(GameField.defs.items.Find(x => x.name == "Shovel"));
            item.GetOwner().RefreshItems();
        }
    }
}
