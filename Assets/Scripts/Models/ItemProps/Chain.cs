﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class Chain : ItemProperty
    {
        public Chain(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
        }

        public override void Activate(MonsterInfo monster)
        {
            List<MonsterDef> defs = GameField.defs.monsters.FindAll(x => x.amountInDeck < x.initAmountInDeck);
            if (defs.Count > 0)
                defs[Random.Range(0, defs.Count)].amountInDeck++;
        }
    }
}
