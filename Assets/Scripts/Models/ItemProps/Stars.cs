﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class Stars : ItemProperty
    {
        public Stars(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
        }

        public override void Activate(MonsterInfo monster)
        {
            if (GameField.Instance.rooms.FindAll(x => !x.isLightOn).Count == 0)
                return;
            GameField.Instance.MustSelectRoom();
            GameField.Instance.OnSelectedRoom += OnSelectedRoom;
        }

        void OnSelectedRoom(RoomInfo room)
        {
            if (room.isLightOn)
            {
                GameField.Instance.MustSelectRoom();
            }
            else
            {
                GameField.Instance.TurnLight(room.id, true, null);
                GameField.Instance.OnSelectedRoom -= OnSelectedRoom;
            }
        }
    }
}
