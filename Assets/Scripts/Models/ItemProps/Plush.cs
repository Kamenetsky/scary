﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class Plush : ItemProperty
    {
        public Plush(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
            if (item.def.damageType != DamageType.Sanctification)
                item.def.damageType = DamageType.None;
        }

        public override void Activate(MonsterInfo monster)
        {
        }
    }
}
