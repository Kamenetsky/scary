﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class Call : ItemProperty
    {
        public Call(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
        }

        public override void Activate(MonsterInfo monster)
        {
            int roomId = item.GetOwner().mover.CurrentRoomId;
            MonsterInfo movedMonster = null;
            int maxPathLenght = 10000;
            foreach (var it in GameField.Instance.Monsters)
            {
                if (it.mover.CurrentRoomId == roomId)
                    continue;
                LinkedList<int> path = GameField.searchExt.DijkstraSearch(roomId, it.mover.CurrentRoomId);
                if (path.Count < maxPathLenght)
                {
                    movedMonster = it;
                }
            }

            if (movedMonster != null)
            {
                movedMonster.MoveTo(roomId, 0, true);
            }
        }
    }
}
