﻿using UnityEngine;

using System.Collections.Generic;

using Scary.Models.Items;

namespace Scary.Models.ItemProps
{
    public class BestWeapon : ItemProperty
    {
        private int initDamage = 0;
        public BestWeapon(ItemPropertyDef def, GameItem item)
            : base(def, item)
        {
        }

        public override void Activate(MonsterInfo monster)
        {
            if (item.GetOwner().def.type == PersonType.Military &&
                monster.def.vulnerabilities.Contains(DamageType.Physics))
            {
                item.def.damage = initDamage + 2;
            }
            else
            {
                item.def.damage = initDamage;
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            item.def.damage = initDamage;
        }
    }
}
