﻿using JsonFx.Json;

using Scary.Models.GameEvents;
using Scary.Models.Items;
using Scary.Utils;

using UnityEngine;

using System;
using System.Collections.Generic;

namespace Scary.Models
{
    public enum GameObjectType
    {
        None,
        Player,
        Monster,
        Room,
        Item
    }
    
    public class GameField 
    {
        #region singlton
        static GameField instance;
        public static GameField Instance
        {
            get 
            { 
                if (instance == null)
                {
                    instance = new GameField();
                    return instance;
                }
                else
                    return instance; 
            }
        }

        protected GameField() { }
        #endregion

        public event Action<MonsterInfo> OnMonsterSpawn = delegate { };
        public event Action<MonsterInfo> OnMonsterDestroy = delegate { };
        public event Action<int, MonsterInfo> OnMonsterChangesHits = delegate { }; 
        public event Action<MonsterInfo, bool> OnMonsterMove = delegate { };
        public event Action<MonsterInfo> OnMonsterRefresh = delegate { };
        public event Action<MonsterInfo, MonsterInfo> OnNextMonster = delegate { };
        public event Action<MonsterInfo> OnMonstersCompleteTurn = delegate { };
        public event Action<PersonInfo, MonsterInfo> OnMonsterAttackPlayer = delegate { };
        public event Action<PersonInfo, MonsterInfo> OnStartMonsterAttackPlayer = delegate { };
        public event Action<MonsterInfo> OnMonsterCompletePath = delegate { };
        public event Action<MonsterInfo> OnMonsterActivateProperty = delegate { };

        public event Action<PersonInfo, bool> OnPlayerMove = delegate { };
        public event Action<PersonInfo> OnPlayerRefresh = delegate { };
        public event Action<PersonInfo, PersonInfo> OnNextPlayer = delegate { };
        public event Action<int, PersonInfo> OnPlayerChangesHits = delegate { };
        public event Action<int, PersonInfo> OnPlayerSetMovesCount = delegate { }; 
        public event Action<PersonInfo> OnPlayerDie = delegate { };
        public event Action<PersonInfo> OnPlayersCompleteTurn = delegate { };
        public event Action<PersonInfo> OnPlayerChangeItems = delegate { };
        public event Action<PersonInfo> OnPlayerCompletePath = delegate { };
        public event Action<GameItem> OnItemActivateProperty = delegate { };
        public event Action<PersonInfo> OnPlayerUseAbility = delegate { };

        public event Action<List<PersonInfo>> OnMustSelectPlayer = delegate { };
        public event Action<List<MonsterInfo>> OnMustSelectMonster = delegate { };
        public event Action<List<RoomInfo>> OnMustSelectRoom = delegate { };
        public event Action<List<GameItem>, bool> OnMustSelectGameItems = delegate { }; 
        public event Action<PersonInfo> OnSelectedPlayer = delegate { };
        public event Action<MonsterInfo> OnSelectedMonster = delegate { };
        public event Action<RoomInfo> OnSelectedRoom = delegate { };
        public event Action<List<GameItem>> OnSelectedGameItems = delegate { };
        public event Action<GameEventBase> OnSetGameEvent = delegate { };
        public event Action OnOpenStorage = delegate { };

        public event Action<bool, int, PersonInfo> OnSwitchLight = delegate { };
        public event Action OnHourComplete = delegate { };
        public event Action<int, int> OnChangePreparePoints = delegate { };

        public static Defs defs = null;

        public List<RoomInfo> rooms = new List<RoomInfo>();
        private RoomType currentlyUsingRoom = RoomType.Default;
        private MonsterInfo currentlySelectedMonster;
        private PersonInfo currentlySelectedPlayer;

        public static bool mustSelectGameobject = false;

        public DateTime currentTime;
        public int preparePoints = 0;
        public int preparePointsBonus = 0;

        private MonsterManager monsterManager = new MonsterManager();
        private PlayerManager playerManager = new PlayerManager();
        private GameEventManager eventManager = new GameEventManager();

        public static List<GameItem> itemStorage = new List<GameItem>(); 

        public static Dictionary<int, List<int>> neightborsById = new Dictionary<int, List<int>>();
        public static Dictionary<int, List<int>> neightborsByIdFree = new Dictionary<int, List<int>>();
        public static DFS searchChecker;
        public static DAS search;
        public static DAS searchExt;

        public static bool allowExchange = true;
        public static bool allowFindItems = true;
        public static bool allowDefense = true;

        public static bool isPlayersTurn = true;
        private bool isfirstTurn = true;

        static GameField()
        {
            defs = new Defs();
            TextAsset asset = Resources.Load<TextAsset>("Defs/Defs");
            defs = JsonReader.Deserialize<Defs>(asset.text);
            TextAsset paths = Resources.Load<TextAsset>("Defs/Paths");
            var tmp = JsonReader.Deserialize<Dictionary<string, int[]>>(paths.text);
            foreach (var pair in tmp)
            {
                neightborsById.Add(Convert.ToInt32(pair.Key), new List<int>(pair.Value));
            }
            TextAsset pathsFree = Resources.Load<TextAsset>("Defs/PathsFree");
            tmp = JsonReader.Deserialize<Dictionary<string, int[]>>(pathsFree.text);
            foreach (var pair in tmp)
            {
                neightborsByIdFree.Add(Convert.ToInt32(pair.Key), new List<int>(pair.Value));
            }
            search = new DAS(neightborsById);
            searchExt = new DAS(neightborsByIdFree);
            searchChecker = new DFS(neightborsById);
        }

        public void Init(List<PersonType> types = null)
        {
            currentTime = new DateTime(1999, 1, 1, 0, 0, 0);

            rooms = new List<RoomInfo>();
            foreach(var it in defs.rooms)
                rooms.Add(new RoomInfo(it));

            playerManager.Init(types);
            itemStorage.Clear();
            isfirstTurn = true;
            //Test
            //Players[0].SetGhost(true);
            //Players[0].AddItem(ItemFactory.CreateItem("Bible", Players[0]));
            //Players[0].AddItem(ItemFactory.CreateItem("Ring", Players[0]));
            //Players[1].AddItem(ItemFactory.CreateItem("Bible", Players[1]));
            //Players[2].AddItem(ItemFactory.CreateItem("Bible", Players[2]));
            /*Players[1].AddItem(ItemFactory.CreateItem(ItemFactory.GetRandom(), Players[1]));
            Players[1].AddItem(ItemFactory.CreateItem(ItemFactory.GetRandom(), Players[1]));
            Players[2].AddItem(ItemFactory.CreateItem(ItemFactory.GetRandom(), Players[3]));
            Players[2].AddItem(ItemFactory.CreateItem(ItemFactory.GetRandom(), Players[3]));*/
        }

        public void SetRandomEvent(bool lastHour = false)
        {
            if (lastHour)
                eventManager.SetRandomHellishEvent();
            else
                eventManager.SetRandomEvent();
        }

        public RoomInfo GetRoomInfo(int roomId)
        {
            return rooms.Find(x => x.id == roomId);
        }

        public bool CanTurnLight()
        {
            PersonInfo pi = GetCurrentPlayer();
            RoomInfo ri = GetRoomInfo(pi.mover.CurrentRoomId);

            if (ri.isLightOn)
                return false;

            if (pi.movesCount < Constants.movesToTurnLight)
                return false;

            return true;
        }

        public void TurnLight(int roomId, bool isOn, PersonInfo pi)
        {
            RoomInfo ri = rooms.Find(x => x.id == roomId);
            ri.isLightOn = isOn;
            if (pi != null)
            {
                pi.movesCount -= Constants.movesToTurnLight;
                foreach (var it in monsterManager.GetMonstersInRoom(roomId))
                {
                    it.target = pi;
                    it.AttackTarget(false);
                }
            }
            OnSwitchLight(ri.isLightOn, roomId, pi);
            if (pi != null && pi.IsDead())
                playerManager.GetNextPlayer();
        }

        public bool CanFindItem()
        {
            if (!allowFindItems)
                return false;
            
            PersonInfo pi = GetCurrentPlayer();
            RoomInfo ri = GetRoomInfo(pi.mover.CurrentRoomId);

            if (!ri.CanSearch())
                return false;

            if (GetMonstersInRoom(ri.id).Count > 0)
                return false;

            if (pi.GetItemsSlotCount() >= Constants.maxItemsCount)
                return false;

            bool playerHasTourch = false;
            for (int i = 0; i < pi.GetItemsCount(); ++i)
            {
                if (pi.GetItem(i).IsLight())
                {
                    playerHasTourch = true;
                    break;
                }
            }

            if (!ri.isLightOn && !playerHasTourch)
                return false;

            if (pi.movesCount < Constants.movesToFindItem)
                return false;

            return true;
        }

        public void FindItem()
        {
            if (!CanFindItem())
                return;

            PersonInfo pi = GetCurrentPlayer();
            pi.AddItem(ItemFactory.CreateItem(ItemFactory.GetRandom(), pi));
            pi.movesCount -= Constants.movesToFindItem;
            OnPlayerRefresh(pi);

            if (pi.movesCount <= 0)
                playerManager.GetNextPlayer();
        }

        public bool CanDrop()
        {
            PersonInfo pi = GetCurrentPlayer();
            return pi.GetItemsCount() > 0;
        }

        public bool CanAttack(MonsterInfo monster, out List<GameItem> items)
        {
            PersonInfo pi = GetCurrentPlayer();
            RoomInfo ri = GetRoomInfo(pi.mover.CurrentRoomId);
            items = null;

            if (monster.additionalPropertyName == "Horror" && monster.canUseAdditionalProperty && pi.GetItemsCount() < 3)//HACK
                return false;

            if (pi.movesCount < 1)
                return false;

            if (!ri.isLightOn)
                return false;

            if (pi.health <= 2 && monster is Monsters.Succubus)
                return false;

            if (pi.GetItemsCount() == 0)
                return false;

            for (int i = 0; i < pi.GetItemsCount(); ++i)
            {
                GameItem it = pi.GetItem(i);
                if (it.IsAttackable() && it.CanAttack(monster) && it.useMovesCount <= pi.movesCount)
                {
                    if (items == null)
                        items = new List<GameItem>();
                    items.Add(it);
                }
            }
            return items != null;
        }

        public void AttackMonster(MonsterInfo monster, GameItem item)
        {
            PersonInfo pi = GetCurrentPlayer();
            pi.movesCount -= item.useMovesCount;
            item.Attack(monster);
            PlayerRefresh(pi);
            if (pi.movesCount <= 0)
            {
                playerManager.GetNextPlayer();
            }
        }

        public bool CanUseRoom()
        {
            PersonInfo pi = GetCurrentPlayer();
            RoomInfo ri = rooms.Find(x => x.id == pi.mover.CurrentRoomId);
            currentlyUsingRoom = ri.type;
            if (ri.type == RoomType.Default || ri.type == RoomType.Corridor || ri.type == RoomType.Hall || !ri.isLightOn)
                return false;

            if (preparePoints < 3 && !(ri.type == RoomType.Storage || ri.type == RoomType.Cellar))
                return false;

            if (pi == null || pi.movesCount <= 0)
                return false;

            if ((ri.type == RoomType.Cellar && !pi.HasShovel()) || pi.GetItemsCount() >= 3)
                return false;

            return true;
        }

        public void UseRoom()
        {
            PersonInfo pi = GetCurrentPlayer();
            RoomInfo ri = rooms.Find(x => x.id == pi.mover.CurrentRoomId);
            RoomType roomType = ri.type;
            if (!(ri.type == RoomType.Storage || ri.type == RoomType.Cellar))
                preparePoints -= 3;
            pi.movesCount -= 1;
            if (roomType == RoomType.Storage && (pi.GetItemsCount() > 0 || itemStorage.Count > 0))
            {
                OnOpenStorage();
            }
            else if (roomType == RoomType.Cellar)
            {
                pi.AddItem(ItemFactory.CreateItem(ItemFactory.GetRandomRelic(), pi));
            }
            else if (roomType == RoomType.Library)
            {
                //TODO make correct
                foreach (var it in pi.GetItems())
                {
                    if (string.IsNullOrEmpty(it.additionalPropertyName))
                    {
                        it.ActivateAdditionalProp();
                        break;
                    }
                }
            }
            else if (roomType == RoomType.Storeroom)
            {
                MustSelectRoom();
            }
            else if (roomType == RoomType.Playroom)
            {
                MustSelectMonster();
            }
            else if (roomType == RoomType.Kitchen)
            {
                MustSelectPlayer();
            }
            else if (roomType == RoomType.Bedroom)
            {
                if (pi.health < pi.def.maxHealth + pi.healBonus)
                    pi.AddHits(1);
            }
            PlayerRefresh(pi);
        }

        #region Monsters
        public List<MonsterInfo> Monsters
        {
            get { return monsterManager.GetMonsters(); }
        }

        public List<MonsterInfo> GetMonstersInRoom(int roomId)
        {
            return monsterManager.GetMonstersInRoom(roomId);
        }

        public void SpawnConcreteMonster(string name, int roomId = -1)
        {
            MonsterDef def = defs.monsters.Find(x => x.name == name);
            if (def != null)
                monsterManager.Spawn(def, roomId);
        }

        public void SpawnMonsterInRoom(string name = "", int roomId = -1, bool hellish = false)
        {
            MonsterDef def;
            if (name != "")
            {
                def = defs.monsters.Find(x => x.name == name);
            }
            else
            {
                if (hellish)
                {
                    def = MonsterFactory.GetRandomHellishDef();
                }
                else
                {
                    def = MonsterFactory.GetRandomDef();
                }
            }
            if (def != null)
                monsterManager.Spawn(def, roomId);
            else
            {
                throw new Exception("Can not find monster!");
            }
        }

        public void MonsterSpawn(MonsterInfo info)
        {
            OnMonsterSpawn(info);
        }

        public void MonsterDestroy(MonsterInfo info)
        {
            OnMonsterDestroy(info);
        }

        public void MonsterMove(MonsterInfo info, bool force)
        {
            OnMonsterMove(info, force);
        }

        public void MonsterRefresh(MonsterInfo info)
        {
            OnMonsterRefresh(info);
        }

        public void MonsterNext(MonsterInfo oldInfo, MonsterInfo info)
        {
            OnNextMonster(oldInfo, info);
        }

        public void MonstersCompleteTurn(MonsterInfo info)
        {
            isPlayersTurn = true;
            int minute = currentTime.Minute;
            currentTime = currentTime.AddMinutes(20f);
            if (currentTime.Minute == 0 && minute > 0)
            {
                foreach (var it in Players)
                {
                    it.isAbilityUsed = false;
                }
                if (currentTime.Hour < 5)
                {
                    for (int i = 0; i < Players.Count + Constants.monstersPlayersCountDiff; ++i)
                    {
                        if (monsterManager.GetCount() <= Constants.maxMonstersCount)
                            monsterManager.SpawnRandom();
                    }
                    SetRandomEvent();
                }
                else
                {
                    SetRandomEvent(true);
                }
                OnHourComplete();
            }
            OnMonstersCompleteTurn(info);
        }

        public void MonsterChangesHits(int amount, MonsterInfo info, CreatureInfo<CreatureDef> damager)
        {
            OnMonsterChangesHits(amount, info);
        }

        public void MonsterMoveComplete(MonsterInfo info)
        {
            monsterManager.MoveComplete(info);
        }

        public void MonsterAttackPlayer(PersonInfo person, MonsterInfo monster)
        {
            OnMonsterAttackPlayer(person, monster);
        }

        public void MonsterStartAttackPlayer(PersonInfo person, MonsterInfo monster)
        {
            OnStartMonsterAttackPlayer(person, monster);
        }

        public void MonsterCompletePath(MonsterInfo monster)
        {
            OnMonsterCompletePath(monster);
        }

        public void MonsterActivateProperty(MonsterInfo monster)
        {
            OnMonsterActivateProperty(monster);
        }
        #endregion

        #region Players
        public List<PersonInfo> Players
        {
            get { return playerManager.GetPlayers(); }
        }

        public PersonInfo GetCurrentPlayer()
        {
            return playerManager.GetCurrent();
        }

        public List<PersonInfo> GetPlayersInRoom(int roomId)
        {
            return playerManager.GetPlayersInRoom(roomId);
        }

        public void StartPlayerTurn()
        {
            playerManager.StartTurn();
        }

        public void SetPlayerMovesCount(int value)
        {
            PersonInfo pi = playerManager.GetCurrent();
            pi.movesCount = Mathf.Clamp(value, 1, pi.def.maxMovesCount) * pi.movesCountMultiplier + pi.movesCountBonus;
            if (pi.movesCount <= 0)
                pi.movesCount = 1;
            if (pi.IsDead())
            {
                SelectNextPlayer();
                return;
            }
            pi.currentMovesCount = pi.movesCount;
            OnPlayerSetMovesCount(value, pi);
            PlayerRefresh(pi);
        }

        public void BuildPathAndMove(int roomId)
        {
            playerManager.MoveCurrentTo(roomId);
        }

        public void SelectNextPlayer()
        {
            if (GetCurrentPlayer().movesCount > 0)
                ChangePreparePoints(GetCurrentPlayer().movesCount);
            GetCurrentPlayer().movesCount = 0;
            playerManager.GetNextPlayer();
        }

        public void ChangePreparePoints(int value)
        {
            int old = preparePoints;
            preparePoints += value + preparePointsBonus;
            OnChangePreparePoints(old, preparePoints);
        }

        public void PlayerTurnLightOn()
        {
            playerManager.TurnLight();
        }

        public void PlayerDie(PersonInfo info)
        {
            OnPlayerDie(info);
        }

        public void PlayerMove(PersonInfo info, bool force)
        {
            OnPlayerMove(info, force);
        }

        public void PlayerCompletePath(PersonInfo info)
        {
            OnPlayerCompletePath(info);
        }

        public void PlayerRefresh(PersonInfo info)
        {
            OnPlayerRefresh(info);
        }

        public void PlayerNext(PersonInfo oldInfo, PersonInfo info)
        {
            OnNextPlayer(oldInfo, info);
        }

        public void PlayersCompleteTurn(PersonInfo info)
        {
            isPlayersTurn = false;
            OnPlayersCompleteTurn(info);
            if (isfirstTurn)
            {
                for (int i = 0; i < Players.Count; ++i)
                {
                    if (monsterManager.GetCount() <= Constants.maxMonstersCount)
                        monsterManager.SpawnRandom();
                }
                isfirstTurn = false;
            }
            
            monsterManager.StartTurn();
        }

        public void PlayerChangesHits(int amount, PersonInfo info)
        {
            OnPlayerChangesHits(amount, info);
        }

        public void PlayerChangeItems(PersonInfo info)
        {
            OnPlayerChangeItems(info);
        }

        public void PlayerMoveComplete(PersonInfo info)
        {
            playerManager.MoveComplete(info);
        }

        public void PlayerUseAbility(PersonInfo player)
        {
            OnPlayerUseAbility(player);
        }

        public void ItemActivateProperty(GameItem item)
        {
            OnItemActivateProperty(item);
        }
        #endregion

        #region GUI calls
        public void MustSelectPlayer(List<PersonInfo> filter = null)
        {
            mustSelectGameobject = true;
            OnMustSelectPlayer(filter);
        }

        public void MustSelectMonster(List<MonsterInfo> filter = null)
        {
            mustSelectGameobject = true;
            OnMustSelectMonster(filter);
        }

        public void MustSelectRoom(List<RoomInfo> filter = null)
        {
            mustSelectGameobject = true;
            OnMustSelectRoom(filter);
        }

        public void MustSelectItems(List<GameItem> items, bool onlyOne)
        {
            if (mustSelectGameobject)
                return;
            OnMustSelectGameItems(items, onlyOne);
        }

        public void SetSelectedPlayer(PersonInfo player)
        {
            mustSelectGameobject = false;
            if (currentlyUsingRoom == RoomType.Kitchen)
            {
                currentlySelectedPlayer = player;
                MustSelectRoom(rooms.FindAll(x => neightborsById[player.mover.CurrentRoomId].Contains(x.id)));
            }
            OnSelectedPlayer(player);
        }

        public void SetSelectedMonster(MonsterInfo monster)
        {
            mustSelectGameobject = false;
            if (currentlyUsingRoom == RoomType.Playroom)
            {
                currentlySelectedMonster = monster;
                if (monster.IsGhost())
                    MustSelectRoom(rooms.FindAll(x => neightborsByIdFree[monster.mover.CurrentRoomId].Contains(x.id)));
                else
                    MustSelectRoom(rooms.FindAll(x => neightborsById[monster.mover.CurrentRoomId].Contains(x.id)));
            }
            OnSelectedMonster(monster);
        }

        public void SetSelectedGameItems(List<GameItem> items)
        {
            OnSelectedGameItems(items);
        }

        public void SetSelectedRoom(RoomInfo room)
        {
            mustSelectGameobject = false;
            if (currentlyUsingRoom == RoomType.Storeroom)
            {
                TurnLight(room.id, true, null);
                currentlyUsingRoom = RoomType.Default;
            }
            else if (currentlyUsingRoom == RoomType.Playroom && currentlySelectedMonster != null)
            {
                currentlySelectedMonster.MoveTo(room.id, 0, true);
                currentlyUsingRoom = RoomType.Default;
                currentlySelectedMonster = null;
            }
            else if (currentlyUsingRoom == RoomType.Kitchen && currentlySelectedPlayer != null)
            {
                currentlySelectedPlayer.MoveTo(room.id, 0, true);
                currentlyUsingRoom = RoomType.Default;
                currentlySelectedPlayer = null;
            }
            OnSelectedRoom(room);
        }

        public void SetGameEvent(GameEventBase ev)
        {
            OnSetGameEvent(ev);
        }
        #endregion
    }
}
