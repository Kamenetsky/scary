﻿using Scary.Models.GameEvents;
using Scary.Models.Items;
using UnityEngine;
using UnityEngine.UI;

using System;
using System.Collections.Generic;

using Scary.Models;
using Scary.Controllers;

namespace Scary.GUI
{
    public class GUIGameWindow : GenericWindow
    {       
        [SerializeField] private Text rollValueText = null;
        [SerializeField] private Text timeValueText = null;
        [SerializeField] private Text prepPointsText = null;
        [SerializeField] private Text eventCaptionText = null;
        [SerializeField] private GameObject timePanel = null;
        [SerializeField] private GameObject charItemPrefab = null;
        [SerializeField] private GameObject monsterItemPrefab = null;
        [SerializeField] private GameObject charactersPanel = null;
        [SerializeField] private GameObject monstersPanel = null;
        [SerializeField] private GameObject actionPanel = null;
        [SerializeField] private Button buttonRoll = null;
        [SerializeField] private Button buttonTurnLight = null;
        [SerializeField] private Button buttonFindItem = null;
        [SerializeField] private Button buttonAttack = null;
        [SerializeField] private Button buttonDrop = null;
        [SerializeField] private Button buttonUseRoom = null;
        [SerializeField] private GUIGameItem[] items = null;
        [SerializeField] private GUIAttackPanel attackPanel = null;
        [SerializeField] private GUIDropPanel dropPanel = null;
        [SerializeField] private GUIStoragePanel storagePanel = null;
        [SerializeField] private Text[] monsterNums = null;

        List<GUICharacterGameItem> charItems = new List<GUICharacterGameItem>();
        List<GUIMonsterItem> monsterItems = new List<GUIMonsterItem>();

        private float timeLineProgress = 0.0f;
        public GameObjectType selectObjectType = GameObjectType.None;

        public override void OnShow()
        {
            base.OnShow();

            foreach (var it in charItems)
            {
                Destroy(it.gameObject);
            }
            charItems.Clear();

            charItemPrefab.SetActive(true);
            var info = GameField.Instance.Players;
            foreach (var it in info)
            {
                var go = Instantiate(charItemPrefab);
                go.transform.SetParent(charactersPanel.transform, false);
                go.transform.localScale = Vector3.one;
                GUICharacterGameItem ci = go.GetComponent<GUICharacterGameItem>();
                ci.Init(it.def.type, gameObject);
                ci.SetImage(it.def.type.ToString() + "_face");
                charItems.Add(ci);
            }
            charItemPrefab.SetActive(false);

            buttonTurnLight.gameObject.SetActive(false);
            buttonFindItem.gameObject.SetActive(false);
            buttonAttack.gameObject.SetActive(false);
            buttonDrop.gameObject.SetActive(false);
            rollValueText.text = string.Empty;

            timeValueText.text = GameField.Instance.currentTime.ToShortTimeString();
            //TODO тут костыль для прогресс бара
            timePanel.GetComponent<Image>().fillAmount = timeLineProgress;

            GameField.Instance.OnMonsterSpawn += OnMonsterSpawnCallback;
            GameField.Instance.OnMonsterMove += OnMonsterMoveCallback;
            GameField.Instance.OnMonsterChangesHits += OnMonsterChangesHitsCallback;
            GameField.Instance.OnMonstersCompleteTurn += OnMonstersCompleteTurnCallback;
            GameField.Instance.OnMonsterDestroy += OnMonsterDestroyCallback;
            GameField.Instance.OnMonsterRefresh += OnMonsterActCallback;
            GameField.Instance.OnStartMonsterAttackPlayer += OnStartMonsterAttackPlayerCallback;
            GameField.Instance.OnMonsterActivateProperty += OnMonsterActivatePropertyCallback;

            GameField.Instance.OnPlayerMove += OnPlayerMoveCallback;
            GameField.Instance.OnNextPlayer += OnNextPlayerCallback;
            GameField.Instance.OnPlayerChangesHits += OnPlayerChangesHitsCallback;
            GameField.Instance.OnPlayersCompleteTurn += OnPlayersCompleteTurnCallback;
            GameField.Instance.OnPlayerChangeItems += OnPlayerChangeItemsCallback;
            GameField.Instance.OnPlayerCompletePath += OnPlayerCompleteActCallback;
            GameField.Instance.OnPlayerRefresh += OnPlayerActCallback;
            GameField.Instance.OnPlayerUseAbility += OnPlayerActCallback;
            GameField.Instance.OnItemActivateProperty += OnItemActivatePropertyCallback;

            GameField.Instance.OnSwitchLight += OnSwitchLightningCallback;
            GameField.Instance.OnChangePreparePoints += OnChangePreparePointsCallback;

            GameField.Instance.OnMustSelectPlayer += OnMustSelectPlayerCallback;
            GameField.Instance.OnMustSelectMonster += OnMustSelectMonsterCallback;
            GameField.Instance.OnMustSelectGameItems += OnMustSelectGameItemsCallback;
            GameField.Instance.OnSelectedRoom += OnSelectedRoomCallback;
            GameField.Instance.OnSetGameEvent += OnSetGameEvent;
            GameField.Instance.OnOpenStorage += OnOpenStorage;

            foreach (var it in items)
                it.gameObject.SetActive(false);
        }

        public override void Close()
        {
            base.Close();

            GameField.Instance.OnMonsterSpawn -= OnMonsterSpawnCallback;
            GameField.Instance.OnMonsterMove -= OnMonsterMoveCallback;
            GameField.Instance.OnMonsterChangesHits -= OnMonsterChangesHitsCallback;
            GameField.Instance.OnMonstersCompleteTurn -= OnMonstersCompleteTurnCallback;
            GameField.Instance.OnMonsterDestroy -= OnMonsterDestroyCallback;
            GameField.Instance.OnMonsterRefresh -= OnMonsterActCallback;
            GameField.Instance.OnStartMonsterAttackPlayer -= OnStartMonsterAttackPlayerCallback;
            GameField.Instance.OnMonsterActivateProperty -= OnMonsterActivatePropertyCallback;

            GameField.Instance.OnPlayerMove -= OnPlayerMoveCallback;
            GameField.Instance.OnNextPlayer -= OnNextPlayerCallback;
            GameField.Instance.OnPlayerChangesHits -= OnPlayerChangesHitsCallback;
            GameField.Instance.OnPlayersCompleteTurn -= OnPlayersCompleteTurnCallback;
            GameField.Instance.OnPlayerChangeItems -= OnPlayerChangeItemsCallback;
            GameField.Instance.OnPlayerCompletePath -= OnPlayerCompleteActCallback;
            GameField.Instance.OnPlayerRefresh -= OnPlayerActCallback;
            GameField.Instance.OnPlayerUseAbility -= OnPlayerActCallback;
            GameField.Instance.OnItemActivateProperty -= OnItemActivatePropertyCallback;

            GameField.Instance.OnSwitchLight -= OnSwitchLightningCallback;
            GameField.Instance.OnChangePreparePoints -= OnChangePreparePointsCallback;

            GameField.Instance.OnMustSelectPlayer -= OnMustSelectPlayerCallback;
            GameField.Instance.OnMustSelectMonster -= OnMustSelectMonsterCallback;
            GameField.Instance.OnMustSelectGameItems -= OnMustSelectGameItemsCallback;
            GameField.Instance.OnSelectedRoom -= OnSelectedRoomCallback;
            GameField.Instance.OnSetGameEvent -= OnSetGameEvent;
            GameField.Instance.OnOpenStorage -= OnOpenStorage;
        }

        public void OnRoll()
        {
            int maxVal = UnityEngine.Random.Range(1, 7);
            buttonRoll.gameObject.SetActive(false);
            for (int i = 0; i <= GameField.Instance.Players.FindAll(x => x.IsDead()).Count; ++i)
            {
                int val = UnityEngine.Random.Range(1, 7);
                if (val > maxVal)
                    maxVal = val;
            }
            GameField.Instance.SetPlayerMovesCount(maxVal);
            MansionController.instance.ShowAvailablePathes();
            OnPlayerCompleteActCallback(GameField.Instance.GetCurrentPlayer());
        }

        public void OnUseRoom()
        {
            GameField.Instance.UseRoom();
        }

        public void OnShowRoomInfo()
        {
            RoomInfo ri = GameField.Instance.GetRoomInfo(GameField.Instance.GetCurrentPlayer().mover.CurrentRoomId);
            var wnd = WindowManager.Instance.Show<GUITextWindow>();
            (wnd as GUITextWindow).Init(ri.type.ToString(), I2.Loc.ScriptLocalization.Get("RoomInfo" + ri.type));
        }

        public void OnSwitchLightning()
        {
            GameField.Instance.TurnLight(GameField.Instance.GetCurrentPlayer().mover.CurrentRoomId, true, GameField.Instance.GetCurrentPlayer());
        }

        public void OnFindItem()
        {
            GameField.Instance.FindItem();
        }

        public void OnNextPlayer()
        {
            GameField.Instance.SelectNextPlayer();
        }

        public void OnAttackMonster()
        {
            int roomId = GameField.Instance.GetCurrentPlayer().mover.CurrentRoomId;
            attackPanel.gameObject.SetActive(true);
            List<GameItem> items = null;
            Dictionary<MonsterInfo, List<GameItem>> monsterToItems = new Dictionary<MonsterInfo, List<GameItem>>();
            foreach (var it in GameField.Instance.GetMonstersInRoom(roomId))
            {
                if (GameField.Instance.CanAttack(it, out items))
                {
                    monsterToItems.Add(it, new List<GameItem>(items));
                }
            }
            actionPanel.SetActive(false);
            attackPanel.Init(monsterToItems, gameObject);
        }

        public void OnAttackMonsterComplete()
        {
            actionPanel.SetActive(true);
        }

        public void OnDrop()
        {
            dropPanel.gameObject.SetActive(true);
            dropPanel.Init(GameField.Instance.GetCurrentPlayer());
        }

        public void OnOpenStorage()
        {
            storagePanel.gameObject.SetActive(true);
            storagePanel.Init(gameObject);
        }

        #region Game callbacks
        void OnSwitchLightningCallback(bool value, int id, PersonInfo pi)
        {
            MansionController.instance.SwitchLight(value, id);
            if (pi != null)
                OnPlayerActCallback(pi);
        }

        void OnChangePreparePointsCallback(int old, int value)
        {
            prepPointsText.text = value.ToString();
        }

        void OnPlayerChangeItemsCallback(PersonInfo info)
        {
            foreach (var it in items)
                it.gameObject.SetActive(false);

            for (int i = 0; i < info.GetItemsCount(); ++i)
            {
                items[i].gameObject.SetActive(true);
                items[i].Init(info.GetItem(i));
            }
            OnPlayerActCallback(info);
        }

        bool CanAttack(RoomInfo room)
        {
            List<GameItem> items = null;
            foreach (var it in GameField.Instance.GetMonstersInRoom(room.id))
            {
                if (GameField.Instance.CanAttack(it, out items))
                {
                    Debug.Log("CanAttack " + it.def.name);
                    return true;
                }
            }
            return false;
        }

        void OnNextPlayerCallback(PersonInfo oldInfo, PersonInfo info)
        {
            MansionController.instance.SetCurrentCharacter(info);
            MansionController.instance.ClearAvailablePathes();
            buttonRoll.gameObject.SetActive(true);
            OnPlayerChangeItemsCallback(info);
            OnPlayerCompleteActCallback(info);
            rollValueText.text = string.Empty;
            foreach (var it in charItems)
            {
                it.Refresh();
            }
        }

        void OnPlayerCompleteActCallback(PersonInfo info)
        {
            RoomInfo ri = GameField.Instance.GetRoomInfo(info.mover.CurrentRoomId);
            buttonTurnLight.gameObject.SetActive(GameField.Instance.CanTurnLight());
            buttonFindItem.gameObject.SetActive(GameField.Instance.CanFindItem());
            buttonAttack.gameObject.SetActive(CanAttack(ri));
            buttonDrop.gameObject.SetActive(GameField.Instance.CanDrop());
            buttonUseRoom.gameObject.SetActive(GameField.Instance.CanUseRoom());
            rollValueText.text = info.movesCount.ToString();
        }

        void OnPlayerActCallback(PersonInfo info)
        {
            OnPlayerCompleteActCallback(info);
            foreach (var it in charItems)
            {
                it.Refresh();
            }
        }

        void OnPlayerMoveCallback(PersonInfo info, bool force)
        {
            OnPlayerActCallback(info);
        }

        void OnPlayersCompleteTurnCallback(PersonInfo info)
        {
            buttonRoll.gameObject.SetActive(false);
            buttonFindItem.gameObject.SetActive(false);
            buttonTurnLight.gameObject.SetActive(false);
            buttonAttack.gameObject.SetActive(false);
            buttonUseRoom.gameObject.SetActive(false);
            MansionController.instance.ClearAvailablePathes();
        }

        void OnPlayerChangesHitsCallback(int damage, PersonInfo info)
        {
            OnPlayerActCallback(info);
        }

        void OnItemActivatePropertyCallback(GameItem item)
        {
            var wnd = WindowManager.Instance.Show<GUITextWindow>();
            (wnd as GUITextWindow).Init(item.def.name + " on player " + item.GetOwner().def.type, "Activate property: " + item.additionalPropertyName);
        }

        void OnMonsterMoveCallback(MonsterInfo info, bool force)
        {
            OnMonsterActCallback(info);
        }

        void OnMonsterActCallback(MonsterInfo info)
        {
            foreach (var it in monsterItems)
            {
                it.Refresh();
            }
        }

        void OnMonsterSpawnCallback(MonsterInfo info)
        {
            var go = Instantiate(monsterItemPrefab);
            go.SetActive(true);
            go.transform.SetParent(monstersPanel.transform, false);
            go.transform.localScale = Vector3.one;
            go.name = info.name;
            GUIMonsterItem mi = go.GetComponent<GUIMonsterItem>();
            monsterItems.Add(mi);
            mi.Init(info, gameObject);
        }

        private void OnMonsterChangesHitsCallback(int damage, MonsterInfo info)
        {
            OnMonsterActCallback(info);
        }

        void OnMonstersCompleteTurnCallback(MonsterInfo monster)
        {
            buttonRoll.gameObject.SetActive(true);
            buttonFindItem.gameObject.SetActive(true);
            buttonTurnLight.gameObject.SetActive(true);
            timeValueText.text = GameField.Instance.currentTime.ToShortTimeString();
            //TODO тут костыль для прогресс бара
            timeLineProgress += 1.0f / 21.0f;
            timePanel.GetComponent<Image>().fillAmount = timeLineProgress;
            GameField.Instance.StartPlayerTurn();
        }

        void OnMonsterDestroyCallback(MonsterInfo monster)
        {
            GUIMonsterItem mi = monsterItems.Find(x => x.info == monster);
            monsterItems.Remove(mi);
            Destroy(mi.gameObject);
            OnPlayerCompleteActCallback(GameField.Instance.GetCurrentPlayer());
        }

        void OnStartMonsterAttackPlayerCallback(PersonInfo person, MonsterInfo monster)
        {
            List<GameItem> items = new List<GameItem>();
            foreach (var it in person.GetItems())
            {
                if (monster.def.vulnerabilities.Contains(it.def.damageType))
                    items.Add(it);
            }
            if (items.Count == 0)
            {
                monster.AttackTarget();
                return;
            }
            if ((monster is Scary.Models.Monsters.IceGuardian) || !GameField.allowDefense ||
                ((monster is Scary.Models.Monsters.Succubus) && person.health < person.def.maxHealth/2))
            {
                monster.AttackTarget();
                return;
            }
            dropPanel.gameObject.SetActive(true);
            dropPanel.Init(items, monster, person);
        }

        void OnMonsterActivatePropertyCallback(MonsterInfo monster)
        {
            var wnd = WindowManager.Instance.Show<GUITextWindow>();
            (wnd as GUITextWindow).Init(monster.name, "Activate property: " + monster.additionalPropertyName);
        }

        void OnMustSelectPlayerCallback(List<PersonInfo> filter = null)
        {
            actionPanel.SetActive(false);
            selectObjectType = GameObjectType.Player;
            if (filter == null)
            {
                charItems.ForEach(a => a.SetFrame(true));
            }
            else
            {
                charItems.FindAll(x => filter.Contains(x.GetPerson())).ForEach(a => a.SetFrame(true));
            }
        }

        void OnMustSelectMonsterCallback(List<MonsterInfo> filter = null)
        {
            actionPanel.SetActive(false);
            selectObjectType = GameObjectType.Monster;
            if (filter == null)
            {
                monsterItems.ForEach(a => a.SetFrame(true));
            }
            else
            {
                monsterItems.FindAll(x => filter.Contains(x.info)).ForEach(a => a.SetFrame(true));
            }
        }

        void OnMustSelectGameItemsCallback(List<GameItem> items, bool onlyOne)
        {
            var wnd = WindowManager.Instance.Show<GUISelectGameItemWindow>();
            (wnd as GUISelectGameItemWindow).Init(items, onlyOne);
        }

        void OnSelectedRoomCallback(RoomInfo room)
        {
            actionPanel.SetActive(true);
            selectObjectType = GameObjectType.None;
        }

        void OnSetGameEvent(GameEventBase ev)
        {
            int ind = ev.GetType().ToString().LastIndexOf('.');
            string caption = ev.GetType().ToString().Substring(ind + 1);
            eventCaptionText.text = caption;
            var wnd = WindowManager.Instance.Show<GUITextWindow>();
            (wnd as GUITextWindow).Init(caption, "SGameEvent" + caption);
        }
        #endregion

        public void OnPlayerItemClicked(GameObject player)
        {
            GUICharacterItem it = player.GetComponent<GUICharacterItem>();
            if (selectObjectType == GameObjectType.None)
            {
                it.ShowInfo();
                return;
            }
            actionPanel.SetActive(true);
            selectObjectType = GameObjectType.None;
            charItems.ForEach(a => a.SetFrame(false));
            GameField.Instance.SetSelectedPlayer(GameField.Instance.Players.Find(x => x.def.type == it.GetPersonType()));
        }

        public void OnMonsterItemClicked(GameObject monster)
        {
            GUIMonsterItem it = monster.GetComponent<GUIMonsterItem>();
            if (selectObjectType == GameObjectType.None)
            {
                it.ShowInfo();
                return;
            }
            actionPanel.SetActive(true);
            selectObjectType = GameObjectType.None;
            monsterItems.ForEach(a => a.SetFrame(false));
            GameField.Instance.SetSelectedMonster(it.info);
        }

        void Update()
        {
            if (MansionController.instance == null)
                return;

            int num = 0;
            foreach (var it in MansionController.instance.GetMonsterViews())
            {
                Vector3 pos = GetScreenPosition(it.transform, WindowManager.Instance.GetBaseLayer(), Camera.main);
                if (num < monsterNums.Length)
                    monsterNums[num].rectTransform.localPosition = pos;
                num++;
            }
            if (num < monsterNums.Length)
            {
                //HACK
                for (int i = num; i < monsterNums.Length; ++i)
                    monsterNums[i].rectTransform.position = new Vector3(-1000, 0, 0);
            }
        }

        public static Vector3 GetScreenPosition(Transform transform, Canvas canvas, Camera cam)
        {
            Vector3 pos;
            float width = canvas.GetComponent<RectTransform>().sizeDelta.x;
            float height = canvas.GetComponent<RectTransform>().sizeDelta.y;
            float x = Camera.main.WorldToScreenPoint(transform.position).x / Screen.width;
            float y = Camera.main.WorldToScreenPoint(transform.position).y / Screen.height;
            pos = new Vector3(width * x - width / 2, y * height - height / 2);
            return pos;
        }
    }
}
