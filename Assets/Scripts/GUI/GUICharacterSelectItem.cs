﻿using UnityEngine;
using UnityEngine.UI;

using Scary.Models;
using Scary.Controllers;

namespace Scary.GUI
{
    public class GUICharacterSelectItem : GUICharacterItem
    {
        [SerializeField] private Toggle selected = null;

        public void OnSelect(bool value)
        {
            parent.SendMessage("OnSelectCharacter", gameObject);
        }

        public void SetSelected(bool value)
        {
            if (selected != null)
                selected.isOn = value;
        }

        public bool IsSelected()
        {
            return selected == null ? false : selected.isOn;
        }
    }
}
