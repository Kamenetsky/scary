﻿using System.Linq;
using UnityEngine;
using System.Collections.Generic;

using Scary.Models;
using Scary.Models.Items;

namespace Scary.GUI
{
    public class GUIStoragePanel : MonoBehaviour
    {
        [SerializeField]
        private GameObject itemPrefab = null;
        [SerializeField]
        private Transform itemPanel = null;

        private List<GameObject> gameItems = new List<GameObject>();

        private bool isStorage = false;

        public void Init(GameObject parent)
        {
            Refresh();
        }

        public void Refresh()
        {
            foreach (var it in gameItems)
            {
                Destroy(it);
            }
            gameItems.Clear();
            PersonInfo pi = GameField.Instance.GetCurrentPlayer();
            List<GameItem> items = isStorage ? GameField.itemStorage : pi.GetItems();
            foreach (var it in items)
            {
                var go = Instantiate(itemPrefab);
                go.SetActive(true);
                go.transform.SetParent(itemPanel, false);
                go.GetComponent<GUIGameItem>().Init(it);
                gameItems.Add(go);
            }
        }

        public void OnSelect(GUIGameItem item)
        {
            if (isStorage)
            {
                GameField.itemStorage.Remove(item.GetItem());
                GameField.Instance.GetCurrentPlayer().AddItem(item.GetItem());
            }
            else
            {
                GameField.Instance.GetCurrentPlayer().RemoveItem(item.GetItem());
                GameField.itemStorage.Add(item.GetItem());
            }
            gameObject.SetActive(false);
        }

        public void OnChangeStorage()
        {
            isStorage = !isStorage;
            Refresh();
        }
    }
}
