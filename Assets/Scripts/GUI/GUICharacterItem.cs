﻿using UnityEngine;
using UnityEngine.UI;

using Scary.Models;

namespace Scary.GUI
{
    public class GUICharacterItem : MonoBehaviour
    {
        [SerializeField]
        private Text lifesText = null;
        [SerializeField]
        private Text classText = null;
        [SerializeField]
        private Image image = null;
        [SerializeField]
        private GameObject isCurrent = null;
        [SerializeField]
        private GameObject isFramed = null;

        protected PersonType personType;
        protected GameObject parent;

        public PersonType GetPersonType()
        {
            return personType;
        }

        public PersonInfo GetPerson()
        {
            return GameField.Instance.Players.Find(x => x.def.type == personType);
        }

        public virtual void Init(PersonType personType, GameObject parent)
        {
            this.parent = parent;
            this.personType = personType;
            image.sprite = Resources.Load<Sprite>("Textures/Characters/" + personType);
            classText.text = personType.ToString();
            Refresh();
            SetFrame(false);
        }

        public virtual void SetImage(string img)
        {
            image.sprite = Resources.Load<Sprite>("Textures/Characters/" + img);
        }

        public virtual void Refresh()
        {
            PersonInfo info = GameField.Instance.Players.Find(a => a.def.type == personType);
            if (info != null && info.health <= 0)
                image.sprite = Resources.Load<Sprite>("Textures/Characters/Dead");
            lifesText.text = info != null ? info.health.ToString() : string.Empty;
            if (isCurrent != null)
                isCurrent.SetActive(GameField.Instance.GetCurrentPlayer().def.type == personType);
        }

        public virtual void OnClick()
        {
            parent.SendMessage("OnPlayerItemClicked", gameObject);
        }

        public virtual void ShowInfo()
        {         
            var wnd = WindowManager.Instance.Show<GUICharacterInfoWindow>();
            PersonInfo info = GameField.Instance.Players.Find(a => a.def.type == personType);
            (wnd as GUICharacterInfoWindow).Init(info);
        }

        public virtual void SetFrame(bool value)
        {
            if (isFramed != null)
                isFramed.SetActive(value);
        }
    }
}
