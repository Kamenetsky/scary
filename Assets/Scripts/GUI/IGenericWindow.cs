﻿using UnityEngine;
using System.Collections;

namespace Scary.GUI
{
    public enum WindowType
    {
        Modal = 0,  // Above HUD, below Dialog
        Dialog,     // Above Modal
        HUD,        // Below all
        Middle
    }

    public enum WindowOrientationMode
    {
        None = 0,
        Scale,
        Switch
    }

    public interface IGenericWindow
    {
        void RegisterWindow(WindowManager manager);
        void Close();
        void Show();
        string WindowName { get; }
        WindowType WindowType { get; }
        WindowOrientationMode WindowOrientationMode { get; }
    }
}