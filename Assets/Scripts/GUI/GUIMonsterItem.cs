﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

using Scary.Models;

namespace Scary.GUI
{
    public class GUIMonsterItem : MonoBehaviour
    {
        [SerializeField]
        private Text lifesText = null;
        [SerializeField]
        private Text damageText = null;
        [SerializeField]
        private Text movesText = null;
        [SerializeField]
        private Text classText = null;
        [SerializeField]
        private Image image = null;
        [SerializeField]
        private GameObject isFramed = null;

        public MonsterInfo info;
        protected GameObject parent;

        public void Init(MonsterInfo info, GameObject parent)
        {
            this.info = info;
            this.parent = parent;
            Sprite spr = Resources.Load<Sprite>("Textures/Monsters/" + info.name);
            if (spr == null)
                spr = Resources.Load<Sprite>("Textures/Monsters/default");
            image.sprite = spr;
            classText.text = info.name;
            Refresh();
            SetFrame(false);
        }

        public void Refresh()
        {
            lifesText.text = info != null ? info.health.ToString() : string.Empty;
            damageText.text = info.damage.ToString();
            movesText.text = info.movesCount + "/" + info.def.maxMovesCount;
            if (info.health <= 0)
                Destroy(gameObject);
        }

        public void OnClick()
        {
            parent.SendMessage("OnMonsterItemClicked", gameObject);
        }

        public void ShowInfo()
        {
            //TODO make correct localization           
            var wnd = WindowManager.Instance.Show<GUITextWindow>();
            string vol = string.Empty;
            foreach (var it in info.def.vulnerabilities)
            {
                vol += it.ToString() + "; ";
            }
            string caption = info.name + (string.IsNullOrEmpty(info.additionalPropertyName)
                ? ""
                : ": " + info.additionalPropertyName);
            string text = I2.Loc.ScriptLocalization.Get("MonsterDef" + info.name);
            if (!string.IsNullOrEmpty(info.additionalPropertyName))
                text = text + "\r\n\r\n" + I2.Loc.ScriptLocalization.Get("MonsterProperty" + info.additionalPropertyName);
            (wnd as GUITextWindow).Init(caption, vol + "   " + text);
        }

        public virtual void SetFrame(bool value)
        {
            if (isFramed != null)
                isFramed.SetActive(value);
        }
    }
}
