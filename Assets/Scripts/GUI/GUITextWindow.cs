﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Scary.GUI
{
    public class GUITextWindow : GenericWindow
    {
        [SerializeField] private Text textInfo = null;
        [SerializeField] private Text textCaption = null;

        public virtual void Init(string caprion, string text)
        {
            textCaption.text = caprion;
            textInfo.text = text;
        }
    }
}
