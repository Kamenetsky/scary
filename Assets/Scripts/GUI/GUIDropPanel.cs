﻿using System.Linq;
using UnityEngine;
using System.Collections.Generic;

using Scary.Models;
using Scary.Models.Items;

namespace Scary.GUI
{
    public class GUIDropPanel : MonoBehaviour
    {
        [SerializeField]
        private GUIGameItemDrop[] items = null;

        private MonsterInfo attacker = null;
        private PersonInfo player = null;
        private List<GameItem> gameItems;

        public void Init(PersonInfo owner)
        {
            attacker = null;
            gameItems = owner.GetItems();
            foreach (var it in items)
                it.gameObject.SetActive(false);

            int i = 0;
            foreach (var item in gameItems)
            {
                items[i].gameObject.SetActive(true);
                items[i].Init(item);
                ++i;
            }
        }

        public void Init(List<GameItem> gameItems, MonsterInfo monster, PersonInfo person)
        {
            attacker = monster;
            player = person;
            this.gameItems = gameItems;
            foreach (var it in items)
                it.gameObject.SetActive(false);

            int i = 0;
            foreach (var item in gameItems)
            {
                items[i].gameObject.SetActive(true);
                items[i].Init(item);
                ++i;
            }
        }

        public void OnClose()
        {
            gameObject.SetActive(false);
            if (attacker != null)
                attacker.AttackTarget();
        }

        public void OnDrop()
        {
            List<GameItem> toRemove = new List<GameItem>();
            foreach (var item in items)
            {
                if (item.gameObject.activeSelf && item.IsSelected())
                {
                    toRemove.Add(item.GetItem());
                    item.GetItem().Drop();
                }
            }
            if (attacker != null)
            {
                int initDamage = attacker.damage;
                attacker.damage -= toRemove.Count;
                if (attacker.damage < 0)
                    attacker.damage = 0;
                Debug.Log("Player defence from attack with items: " + toRemove.Count + " and monster damage: " + attacker.damage);
                if (attacker is Scary.Models.Monsters.Goblin && toRemove.Count >= 2)
                {
                    if (player.health < player.def.maxHealth)
                        player.AddHits(1);
                }
                else
                    attacker.AttackTarget();
                attacker.damage = initDamage;
            }
            gameObject.SetActive(false);
        }
    }
}
