﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections.Generic;

using Scary.Models.Items;
using Scary.Models;

namespace Scary.GUI
{
    public class GUICharacterGameItem : GUICharacterItem
    {
        [SerializeField]
        private Button abilityButton = null;
        [SerializeField]
        private Text abilityText = null;

        public override void Refresh()
        {
            base.Refresh();
            bool isPlayerCurrent = GameField.Instance.GetCurrentPlayer().def.type == personType;
            PersonInfo info = GameField.Instance.Players.Find(a => a.def.type == personType);
            if (abilityButton != null)
            {
                abilityButton.enabled = (isPlayerCurrent || personType == PersonType.Military) && !info.isAbilityUsed && info.canUseAbility;
                if (abilityText != null)
                {
                    //TODO make normal indicator
                    if (info.isAbilityUsed)
                        abilityText.text = "N";
                    else if (!info.canUseAbility)
                        abilityText.text = "X";
                    else
                        abilityText.text = "A";
                }
            }
        }

        public void OnAbilityClick()
        {
            PersonInfo info = GameField.Instance.Players.Find(a => a.def.type == personType);
            info.UseAbility();
        }

        public override void OnClick()
        {
            PersonInfo info = GameField.Instance.Players.Find(a => a.def.type == personType);
            PersonInfo current = GameField.Instance.GetCurrentPlayer();
            if (info != current && info.GetItemsCount() > 0 && GameField.allowExchange && current.movesCount > 0
                && current.mover.CurrentRoomId == info.mover.CurrentRoomId && current.GetItemsCount() < 3 && !GameField.mustSelectGameobject)
            {
                GameField.Instance.MustSelectItems(info.GetItems(), true);
                GameField.Instance.OnSelectedGameItems += OnSelectedGameItems;
                current.movesCount--;
            }
            else
            {
                base.OnClick();
            }
        }

        void OnSelectedGameItems(List<GameItem> items)
        {
            GameField.Instance.OnSelectedGameItems -= OnSelectedGameItems;
            if (items != null && items.Count > 0)
            {
                PersonInfo info = GameField.Instance.Players.Find(a => a.def.type == personType);
                info.RemoveItem(items[0]);
                GameField.Instance.GetCurrentPlayer().AddItem(items[0]);
            }
        }
    }
}
