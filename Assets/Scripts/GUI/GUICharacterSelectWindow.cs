﻿using Scary.Utils;
using UnityEngine;
using UnityEngine.UI;

using System.Linq;
using System.Collections.Generic;

using Scary.Models;

namespace Scary.GUI
{
    public class GUICharacterSelectWindow : GenericWindow
    {
        [SerializeField]
        private GameObject charItemPrefab = null;
        [SerializeField]
        private GameObject charactersPanel = null;
        [SerializeField]
        private GameObject descriptionWindow = null;
        [SerializeField]
        private Text classText = null;
        [SerializeField]
        private Text abilitiesText = null;
        [SerializeField]
        private Text descriptionText = null;
        [SerializeField]
        private Image classImage = null;
        [SerializeField]
        private Toggle selectRandom = null;
        [SerializeField]
        private Toggle multiplePlayers = null;

        List<GUICharacterSelectItem> charItems = new List<GUICharacterSelectItem>();

        void Start()
        {
            charItemPrefab.SetActive(true);
            var info = GameField.defs.characters;
            foreach (var it in info)
            {
                var go = Instantiate(charItemPrefab);
                go.transform.SetParent(charactersPanel.transform, false);
                go.transform.localScale = Vector3.one;
                GUICharacterSelectItem ci = go.GetComponent<GUICharacterSelectItem>();
                ci.Init(it.type, gameObject);
                charItems.Add(ci);
            }
            charItemPrefab.SetActive(false);
            HideDescriptionWindow();
        }
        public void OnSelectCharacter(object sender)
        {
            if (multiplePlayers.isOn)
            {
                if (charItems.Count(x => x.IsSelected()) > Scary.Constants.maxPlayersCount)
                {
                    charItems.Find(x => x.IsSelected() && x.gameObject != sender).SetSelected(false);
                }
            }
            else if (charItems.Count(x => x.IsSelected()) > 1)
            {
                charItems.Find(x => x.IsSelected() && x.gameObject != sender).SetSelected(false);
            }
            ShowDescriptionWindow();
        }

        public void ShowDescriptionWindow()
        {            
            foreach (var it in charItems)
            {
                if (it.IsSelected())
                {
                    string personType = it.GetPersonType().ToString();
                    classImage.sprite = Resources.Load<Sprite>("Textures/Characters/" + personType);
                    classText.text = personType;
                    abilitiesText.text = "Ability: " + I2.Loc.ScriptLocalization.Get("PlayerAbility" + personType);
                }
            }
            descriptionWindow.SetActive(true);
        }

        public void HideDescriptionWindow()
        {
            descriptionWindow.SetActive(false);
        }

        public void OnSelectRandom(bool value)
        {
            foreach (var it in charItems)
            {
                it.SetSelected(false);
            }
        }

        public void OnSelectMultiple(bool value)
        {
            if (!value)
                OnSelectRandom(true);
        }

        public void OnCompelete()
        {   
            if (selectRandom.isOn)
                GameField.Instance.Init();
            else
            {
                List<PersonType> types = new List<PersonType>();
                foreach (var it in charItems)
                {
                    if (it.IsSelected())
                        types.Add(it.GetPersonType());
                }
                GameField.Instance.Init(types);
            }
            WindowManager.Instance.Show<GUIGameWindow>();
            Instantiate(Resources.Load<GameObject>("Prefabs/MansionViewTest"));
            Camera.main.GetComponent<CameraController>().enabled = true;
            Close();
        }

        public void OnPlayerItemClicked(GameObject player)
        {
            player.GetComponent<GUICharacterItem>().ShowInfo();
        }
    }
}
