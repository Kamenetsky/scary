﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using DG.Tweening;
using Object = UnityEngine.Object;

namespace Scary.GUI
{
    public class WindowManager : MonoBehaviour 
	{
        [SerializeField] GameObject BlackScreen = null;
        [SerializeField] Canvas baseLayer = null;

		private static WindowManager instance = null;
        private List<WindowHandle> Handles = new List<WindowHandle>();

		private Dictionary<Type, string> _types = new Dictionary<Type, string>();

        public event Action<GenericWindow> WindowDidShowed;
        public event Action<GenericWindow> WindowDidClosed;

        public Canvas GetBaseLayer()
        {
            return baseLayer;
        }

        public bool IsPortrait()
        {
#if UNITY_EDITOR
            return false;
#else            
            return false;
#endif
        }

		public static WindowManager Instance
		{
			get 
			{
				if(instance == null) 
				{
					throw new UnityException("WindowManager haven't been initialized");
				}
				return instance;
			}
		}

	    void Awake()
		{
			instance = this;
			BlackScreen.SetActive(false);
            //TODO move to entry point
            RegisterWindow<GUIGameWindow>("GUIGameWindow");
            RegisterWindow<GUICharacterSelectWindow>("GUISelectCharactersWindow");
            RegisterWindow<GUITextWindow>("GUITextWindow");
            RegisterWindow<GUICharacterInfoWindow>("GUICharacterInfoWindow");
            RegisterWindow<GUISelectGameItemWindow>("GUISelectGameItemWindow");
		}

        void Start()
        {
#if !UNITY_EDITOR            
            UpdateFittingRule();
            foreach (WindowHandle handle in Handles) {
               handle.Window.FitWindow();
            }
#endif
        }

        private void UpdateFittingRule()
        {
            float k = (Screen.orientation == ScreenOrientation.Landscape || 
                Screen.orientation == ScreenOrientation.LandscapeRight || 
                Screen.orientation == ScreenOrientation.LandscapeLeft) ? 1.0f : .0f;

            SetFittingRule(baseLayer, k);
        }

		private void SetFittingRule(Canvas canvas, float value)
		{
			CanvasScaler scaler = canvas.GetComponent<CanvasScaler>();
			if (scaler != null) {
				scaler.matchWidthOrHeight = value;
			}
		}

		public WindowHandle FindHandle(GenericWindow window)
		{
			for(int i = 0; i < Handles.Count; ++i) 
			{
				if(window == Handles[i].Window) 
				{
					return Handles[i];
				}
			}
			return null;
		}

        public GenericWindow FindWindow(string windowName)
        {
            for (int i = 0; i < Handles.Count; ++i)
            {
                if (windowName == Handles[i].WindowName)
                {
                    return Handles[i].Window;
                }
            }
            return null;
        }

		private void SetWindowLayer(GenericWindow instance)
		{
            instance.transform.SetParent(baseLayer.transform, false);
		}

        public void SetBlackout(bool visible = true)
        {
            if (BlackScreen != null)
            {
				if(!visible) {
					BlackScreen.GetComponent<Image>().DOFade(0.0f, 0.25f).OnComplete(() => {
						BlackScreen.SetActive(false);
					});
				} else {
					if (!BlackScreen.activeSelf) {
						BlackScreen.SetActive(true);
						Image image = BlackScreen.GetComponent<Image>();
						DOTween.Kill(image, false);
						Color color = image.color;
						color.a = 0.0f;
						image.color = color;
						image.DOFade(0.75f, 0.25f);
					} else {
						Image image = BlackScreen.GetComponent<Image>();
						DOTween.Kill(image, false);
						Color color = image.color;
						color.a = 0.75f;
						image.color = color;
					}

				}
            }
        }

		public GenericWindow Show<T>()
		{
			if(_types.ContainsKey(typeof(T))){
                return ShowWindow<T>();
			}
			return null;
		}

        private GenericWindow ShowWindow<T>()
        {
            Type windowType = typeof(T);

            Debug.Log(windowType.ToString());

            var windowPrefab = Resources.Load<GameObject>(string.Format("GUI/{0}", _types[windowType]));
            GameObject windowInstance = Instantiate(windowPrefab);
           
            var window = windowInstance.GetComponent<GenericWindow>();
            if (window != null) {
                var handle = new WindowHandle(window);

                if (window.WindowType == WindowType.Dialog || window.WindowType == WindowType.Modal) {
                    HideModalWindows();
                }

                Handles.Add(handle);
                ShowAndRegister(window);

                return window;
            }

            return null;
        }

        private void HideModalWindows()
        {
            List<WindowHandle> handles = Handles.FindAll(
                (window)=>window.Window.WindowType == WindowType.Dialog || window.Window.WindowType == WindowType.Modal
            );

            foreach (WindowHandle handle in handles) {
                handle.Window.OnHide();
            }
        }    

		public int ModalCount
		{
			get
			{
				int count = 0;
				foreach (WindowHandle handle in Handles) {
					if (handle.Window.WindowType == WindowType.Dialog || handle.Window.WindowType == WindowType.Modal) {
						count++;
					}
				}
				return count;
			}
		}

		private void ShowAndRegister(GenericWindow window)
		{
			if (!window.gameObject.activeSelf) {
				window.gameObject.SetActive(true);
			}
			window.RegisterWindow(this);
			SetWindowLayer(window);
			window.Show();
			if (WindowDidShowed != null && WindowDidShowed.GetInvocationList().Length > 0) {
				WindowDidShowed.Invoke(window);
			}
		}

		private GenericWindow PendWindow(Type windowType)
		{
            var windowPrefab = Resources.Load<GameObject>(string.Format("GUI/{0}", _types[windowType]));
            GameObject windowInstance = Instantiate(windowPrefab);
			var window = windowInstance.GetComponent<GenericWindow>();
			var handle = new WindowHandle(window);
			Handles.Add(handle);

			if (window.WindowType == WindowType.Dialog || window.WindowType == WindowType.Modal) {
				if (ModalCount > 1) {
					window.gameObject.SetActive(false);
				} else {
					if (!window.gameObject.activeSelf)
						window.gameObject.SetActive(true);
					ShowAndRegister(window);
				}
			} else {
				ShowAndRegister(window);
			}

			return window;
		}

		public void RegisterWindow<T>(string windowPrefab,bool preload=false)
		{
			_types[typeof(T)] = windowPrefab;
		}

		public void RemoveAll()
		{
			while(Handles.Count > 0) 
			{
				RemoveWindow(Handles[Handles.Count - 1].Window);
			}
		}
       
        private void PopTopWindow()
        {
            List<WindowHandle> handles = Handles.FindAll(
                (window)=>window.Window.WindowType == WindowType.Dialog || window.Window.WindowType == WindowType.Modal
            );
            if (handles.Count > 0) {
                handles[0].Window.Close();
            }
        }

		private void CheckPending()
		{
			List<WindowHandle> modalHandles = Handles.FindAll((x) => x.Window.WindowType == WindowType.Dialog || x.Window.WindowType == WindowType.Modal);
            if (modalHandles.Count > 0){
                ShowAndRegister(modalHandles[modalHandles.Count - 1].Window);
			}
		}

		public void RemoveWindow(GenericWindow window)
		{
			WindowHandle handle = FindHandle(window);
			if(handle != null) 
			{
				Handles.Remove(handle);
				if (WindowDidClosed != null && WindowDidClosed.GetInvocationList().Length > 0) {
					WindowDidClosed.Invoke(window);
				}
                window.OnRemove();			    
                Destroy(window.gameObject);
			}
			CheckPending();
		}
	}
}