﻿using Scary.Models;
using UnityEngine.UI;
using UnityEngine;

using System.Collections;

using Scary.Models.Items;

namespace Scary.GUI
{
    public class GUIGameItem : MonoBehaviour
    {
        [SerializeField] private Text caption = null;
        [SerializeField] private Image icon = null;

        [SerializeField] private GameObject buttonUse = null;

        private GameItem item;

        public virtual void Init(GameItem item)
        {
            this.item = item;
            var image = Resources.Load<Sprite>("Textures/Items/" + item.def.name);
            if (image == null)
                image = Resources.Load<Sprite>("Textures/Items/Default");
            icon.sprite = image;

            caption.text = item.def.name;

            if (buttonUse != null)
                buttonUse.SetActive(item.CanUse());
        }

        public GameItem GetItem()
        {
            return item;
        }

        public virtual void OnClick()
        {
            //TODO make correct localization

            var wnd = WindowManager.Instance.Show<GUITextWindow>();
            string vol = item.def.damageType.ToString();

            string caption = item.def.name + (string.IsNullOrEmpty(item.additionalPropertyName)
                ? ""
                : ": " + item.additionalPropertyName);
            string text = I2.Loc.ScriptLocalization.Get("GameItemDef" + item.def.name);
            if (!string.IsNullOrEmpty(item.additionalPropertyName))
                text = text + "\r\n\r\n" + I2.Loc.ScriptLocalization.Get("GameItemProperty" + item.additionalPropertyName);
            (wnd as GUITextWindow).Init(caption, vol + "   " + text);
        }

        //TODO make refactoring of item using interface
        public void OnUse()
        {
            PersonInfo player = item.GetOwner();
            if (player.movesCount < 1)
                return;

            if (item.CanUse())
            {
                (item as GameItemUsable).Use();
                player.movesCount--;
                GameField.Instance.PlayerRefresh(player);
            }
        }
    }
}
