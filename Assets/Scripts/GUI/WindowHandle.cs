﻿using System;
using UnityEngine;
using System.Collections;

namespace Scary.GUI
{
	public class WindowHandle
	{
		private GenericWindow window = null;

		public WindowHandle(GenericWindow window)
		{
			this.window = window;
		}

		public GenericWindow Window
		{
			get
			{
				return window;
			}
		}

		public string WindowName
		{
			get
			{
				return window.WindowName;
			}
		}
	}
}