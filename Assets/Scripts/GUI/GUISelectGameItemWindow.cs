﻿using Scary.Models;
using Scary.Models.Items;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Scary.GUI
{
    public class GUISelectGameItemWindow : GenericWindow
    {
        [SerializeField] private GameObject itemPrefab = null;
        [SerializeField] private GameObject itemsPanel = null;
        [SerializeField] private GameObject closeButton = null;
        [SerializeField] private GameObject selectButton = null;

        private List<GUIGameItemSelect> selectedItems = new List<GUIGameItemSelect>();
        private bool onlyOne;
        public void Init(List<GameItem> items, bool onlyOne, bool canClose = false)
        {
            this.onlyOne = onlyOne;
            closeButton.SetActive(canClose);
            selectButton.SetActive(!onlyOne);
            itemPrefab.SetActive(true);
            foreach (var it in items)
            {
                var go = Instantiate(itemPrefab);
                go.transform.SetParent(itemsPanel.transform, false);
                go.transform.localScale = Vector3.one;
                GUIGameItemSelect si = go.GetComponent<GUIGameItemSelect>();
                si.Init(it, onlyOne);
                selectedItems.Add(si);
            }
            itemPrefab.SetActive(false);
        }

        public void OnItemClick(GUIGameItemSelect item)
        {
            if (onlyOne)
            {
                GameField.Instance.SetSelectedGameItems(new List<GameItem>() { item.GetItem() });
                Close();
            }
        }

        public void OnSelect()
        {
            if (!selectedItems.Exists(x => x.IsSelected()))
                return;

            if (onlyOne)
            {
                GameField.Instance.SetSelectedGameItems(new List<GameItem>() { selectedItems.Find(x => x.IsSelected()).GetItem() });
            }
            else
            {
                List<GameItem> res = new List<GameItem>();
                foreach (var it in selectedItems.FindAll(x => x.IsSelected()))
                {
                    res.Add(it.GetItem());
                }
                GameField.Instance.SetSelectedGameItems(res);
            }
            Close();
        }

        public void OnSimpleClose()
        {
            GameField.Instance.SetSelectedGameItems(new List<GameItem>());
            Close();
        }
    }
}
