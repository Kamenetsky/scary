﻿using Scary.Models;
using UnityEngine;
using System.Collections;

namespace Scary.GUI
{
    public class GUICharacterInfoWindow : GUITextWindow
    {
        public PersonInfo player = null;
        [SerializeField] private GameObject abilityButton = null;

        public void Init(PersonInfo player)
        {
            this.player = player;
            string text = I2.Loc.ScriptLocalization.Get("PlayerAbility" + (player.def.type));
            bool canUseAbility = !player.isAbilityUsed && player.canUseAbility &&
                                 (player == GameField.Instance.GetCurrentPlayer());
            abilityButton.SetActive(canUseAbility);
            base.Init(player.def.type.ToString(), text);
        }
        
        public void OnAbilityClick()
        {
            Close();
            player.UseAbility();
        }
    }   
}
