﻿using UnityEngine.UI;
using UnityEngine;

using System.Collections;

using Scary.Models.Items;

namespace Scary.GUI
{
    public class GUIGameItemSelect : GUIGameItem
    {
        [SerializeField]
        private Toggle selected = null;

        public void Init(GameItem item, bool onlyOne)
        {
            base.Init(item);
            selected.gameObject.SetActive(!onlyOne);
        }

        public bool IsSelected()
        {
            return selected.isOn;
        }

        public new void OnClick()
        {
            if (selected.gameObject.activeSelf)
                selected.isOn = !selected.isOn;
        }
    }
}
