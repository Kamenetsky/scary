﻿using UnityEngine.UI;
using UnityEngine;

using System.Collections;

using Scary.Models.Items;

namespace Scary.GUI
{
    public class GUIGameItemDrop : GUIGameItem
    {
        [SerializeField] private Toggle selected = null;

        public bool IsSelected()
        {
            return selected.isOn;
        }
    }
}
