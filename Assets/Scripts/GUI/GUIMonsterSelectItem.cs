﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

using Scary.Models;

namespace Scary.GUI
{
    public class GUIMonsterSelectItem : MonoBehaviour
    {
        [SerializeField]
        private Text lifesText = null;
        [SerializeField]
        private Text classText = null;
        [SerializeField]
        private Image image = null;
        [SerializeField]
        private Toggle selected = null;

        public MonsterInfo info;

        private GUIAttackPanel parent = null;

        public void Init(MonsterInfo info, GUIAttackPanel parent)
        {
            this.info = info;
            this.parent = parent;
            Sprite spr = Resources.Load<Sprite>("Textures/Monsters/" + info.name);
            if (spr == null)
                spr = Resources.Load<Sprite>("Textures/Monsters/default");
            image.sprite = spr;
            classText.text = info.name;
            Refresh();
        }

        public void Refresh()
        {
            lifesText.text = info != null ? info.health.ToString() : string.Empty;
            if (info.health <= 0)
                Destroy(gameObject);
        }

        public void OnSelect()
        {
            if (parent != null)
                parent.SendMessage("OnSelect", gameObject);
        }

        public bool IsSelected()
        {
            return selected.isOn;
        }

        public void SetSelected(bool val)
        {
            selected.isOn = val;
        }
    }
}
