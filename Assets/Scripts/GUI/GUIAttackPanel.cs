﻿using System.Linq;
using UnityEngine;
using System.Collections.Generic;

using Scary.Models;
using Scary.Models.Items;

namespace Scary.GUI
{
    public class GUIAttackPanel : MonoBehaviour
    {
        [SerializeField] private GUIGameItemAttack[] items = null;
        [SerializeField] private GameObject monsterPrefab = null;
        [SerializeField] private Transform monsterPanel = null;

        private List<GUIMonsterSelectItem> monsterItems = new List<GUIMonsterSelectItem>();
        private Dictionary<MonsterInfo, List<GameItem>> monsterToItems;
        private MonsterInfo current = null;
        private GameObject parent = null;

        public void Init(Dictionary<MonsterInfo, List<GameItem>> monsterToItems, GameObject parent)
        {
            this.monsterToItems = monsterToItems;
            this.parent = parent;
            current = monsterToItems.First().Key;
            Refresh();
            monsterItems[0].SetSelected(true);
        }

        public void Refresh()
        {
            foreach (var it in items)
                it.gameObject.SetActive(false);
            
            foreach (var it in monsterItems)
                Destroy(it.gameObject);
            monsterItems.Clear();
            
            foreach (var it in monsterToItems)
            {
                var go = Instantiate(monsterPrefab);
                go.SetActive(true);
                go.transform.SetParent(monsterPanel.transform, false);
                go.transform.localScale = Vector3.one;
                go.name = it.Key.name;
                GUIMonsterSelectItem mi = go.GetComponent<GUIMonsterSelectItem>();
                mi.Init(it.Key, this);
                monsterItems.Add(mi);
                if (it.Key == current)
                {
                    int i = 0;
                    foreach (var item in it.Value)
                    {
                        items[i].gameObject.SetActive(true);
                        items[i].Init(item, this);
                        ++i;
                    }
                }
            }

            foreach (var it in monsterItems)
            {
                if (it.info != current)
                    it.SetSelected(false);
            }
        }

        public void OnSelect(object sender)
        {
            GUIMonsterSelectItem it = (sender as GameObject).GetComponent<GUIMonsterSelectItem>();
            if (it.IsSelected())
            {
                current = it.info;
                Refresh();
            }
        }

        public void OnAttack(GUIGameItem item)
        {
            GameField.Instance.AttackMonster(current, item.GetItem());
            gameObject.SetActive(false);
            parent.SendMessage("OnAttackMonsterComplete");
        }
    }
}
