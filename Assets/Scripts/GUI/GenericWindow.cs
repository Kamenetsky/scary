﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

namespace Scary.GUI
{
    public enum WindowOrientation
    {
        None = 0,
        Landscape,
        Portrait
    }

    public enum WindowState
    {
        Hidden = 0,
        Showing,
        Present,
        Hiding
    }

	public abstract class GenericWindow : MonoBehaviour, IGenericWindow
	{
		[SerializeField] string _WindowName = "";
		[SerializeField] WindowType _WindowType = WindowType.Modal;
        [SerializeField] WindowOrientationMode _WindowOrientationMode = WindowOrientationMode.None;
        [SerializeField] protected GameObject PortraitPrefab = null;
        [SerializeField] protected GameObject LandscapeHolder = null;

        protected WindowManager _windowManager = null;
        private WindowOrientation _windowOrientation = WindowOrientation.None;
        private WindowState _windowState = WindowState.Hidden;

        public WindowState WindowState
        {
            get {
                return _windowState;
            }
            set {
                _windowState = value;
            }
        }

        public WindowOrientation Orientation
        {
            get {
                return _windowOrientation;
            }
        }

        public WindowOrientationMode WindowOrientationMode {
            get
            {
                return _WindowOrientationMode;
            }
        }

		public string WindowName
		{
			get 
			{
				return _WindowName;
			}
		}

		public WindowType WindowType
		{
			get 
			{
				return _WindowType;
			}
		}

        public virtual void OnShow()
        {
            _windowState = WindowState.Present;
        }

        public virtual void OnHide()
        {
            _windowState = WindowState.Hidden;
            gameObject.SetActive(false);
        }

		public virtual void OnRemove() 
		{
		}

        public virtual void RegisterWindow(WindowManager manager)
		{
			_windowManager = manager;
		}

		public virtual void Close()
		{
			_windowManager.RemoveWindow(this);
		}

		public void FitWindow()
		{
            switch (WindowOrientationMode) {
                case WindowOrientationMode.Scale:
                    RectTransform rt = gameObject.GetComponent<RectTransform>();
                    CanvasScaler scaler = gameObject.GetComponentInParent<CanvasScaler>();

                    if (rt == null || scaler == null)
                        return;

                    rt.localScale = Vector3.one;

                    float k = _windowManager.IsPortrait() ? 
                        scaler.referenceResolution.x / rt.rect.width : 
                        scaler.referenceResolution.y / rt.rect.height;

                    rt.localScale = new Vector3(k, k, 1);

                    Debug.LogFormat("Fit Window ({0}x{1}) in screen ({2}x{3} scaler={4}x{5}, k={6})", 
                        rt.rect.width, rt.rect.height, Screen.width, Screen.height, 
                        scaler.referenceResolution.x, scaler.referenceResolution.y, k
                    );
                break;
                
                case WindowOrientationMode.Switch:
                    CheckOrientation();
                break;
            }
		}

        protected virtual void OnOrientationChange()
        {
            
        }

        protected void CheckOrientation()
        {
            if (_windowManager.IsPortrait()) {
                if (PortraitPrefab != null) {
                    LandscapeHolder.SetActive(false);
                    PortraitPrefab.SetActive(true);
                }
                _windowOrientation = WindowOrientation.Portrait;
            } else {
                _windowOrientation = WindowOrientation.Landscape;
                if (PortraitPrefab != null) {
                    LandscapeHolder.SetActive(true);
                    PortraitPrefab.SetActive(false);
                }
            }
#if UNITY_EDITOR
            _windowOrientation = WindowOrientation.Landscape;
#endif
            OnOrientationChange();
        }

		public virtual void Show()
		{
			if(!this.gameObject.activeSelf) 
			{
				this.gameObject.SetActive(true);
			}

			FitWindow();
            OnShow();
		}
    
        public virtual void Reset()
        {
        }
    }
}