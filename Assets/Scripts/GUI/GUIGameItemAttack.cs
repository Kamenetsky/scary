﻿using UnityEngine.UI;
using UnityEngine;

using System.Collections;

using Scary.Models.Items;

namespace Scary.GUI
{
    public class GUIGameItemAttack : GUIGameItem
    {
        private GUIAttackPanel parent = null;

        public void Init(GameItem item, GUIAttackPanel parent)
        {
            this.parent = parent;
            base.Init(item);
        }

        public override void OnClick()
        {
            parent.OnAttack(this);
        }
    }
}
