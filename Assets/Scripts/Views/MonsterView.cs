﻿using UnityEngine;
using System.Collections;

using Scary.Models;

namespace Scary.Views
{
    public class MonsterView : MonoBehaviour
    {
        [HideInInspector]
        public MonsterInfo monster = null;

        public void Init(MonsterInfo monster)
        {
            this.monster = monster;
        }
    }
}
