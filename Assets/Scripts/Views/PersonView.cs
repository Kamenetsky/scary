﻿using UnityEngine;
using System.Collections;

using Scary.Models;

namespace Scary.Views
{
    public class PersonView : MonoBehaviour
    {
        [HideInInspector]
        public PersonInfo person = null;

        [SerializeField] private GameObject selected = null;

        public void Init(PersonInfo person)
        {
            this.person = person;
        }

        public void Select(bool value)
        {
            if (selected)
                selected.SetActive(value);
        }
    }
}
