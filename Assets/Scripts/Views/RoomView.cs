﻿using Scary.GUI;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Scary.Models;

namespace Scary.Views
{
    public class RoomView : MonoBehaviour 
    {
        public int id;
        public Transform center = null;
        [SerializeField] private GameObject illumination = null;

        RoomInfo info;

        void Awake()
        {
            if (center == null)
                center = transform;
        }

        public void Init(RoomInfo info)
        {
            this.info = info;
            if (center == null)
                center = transform;
        }

        public void SetLight(bool val)
        {
            if (illumination != null)
                illumination.SetActive(val);
        }

        public RoomInfo GetInfo()
        {
            return info;
        }
    }
}
